# Проект PiSun

Проект PiSun создан для того, чтобы каждый обладатель продукции компании Vanavara Digital смог разобраться с принципами работы модулей, используемых в них. В данном репозитории содержится исходный код программы PiSun CORS.

## Краткий функционал программы:

1. Подключение к PiGO Lite и его настройка.
2. Получение дифференциальных поправок с PiGO Lite и отправка их на бесплатный кастер сети pidt.net или на локальный кастер.
3. Запись статики в формате CNB.
4. Разделение файлов статики по часам для удобства архивации и анализа.
5. Отображение состояний спутников на столбцовой диаграмме.
6. Визуализация положения спутников на небосводе с помощью диаграммы в полярных координатах.
8. Настройка К803 для получения сигнала синхронизации времени и меток.
9. Выбор формата для передачи дифференциальных поправок.
10. Управление точками.
8. Облачная конвертация CNB файлов в формат RINEX.
9. Авторизация в сервисах PiDT.Net.
10. Интеграция системы координат в поток поправок.
11. Отправка файлов статики на FTP-сервер.
12. Подключение Raspberry к WIFI сети.

Программа писалась и тестировалась на операционной системе Windows 10 с установленным пакетом для разработки Python 3, web версия протестирована на Windows 10, 11, Ubuntu 20, Raspberry Pi OS Lite.

## Компиляция desktop версии программы

Для компиляции программы под операционную систему Windows нужно установить pyinstaller.exe а затем выполнить следующую команду в терминале:

```pyinstaller --name 'PiSun CORS' --noconsole --onefile --add-binary "venv/Lib/site-packages/pyzbar/libiconv.dll;pyzbar" --add-binary "venv/Lib/site-packages/pyzbar/libzbar-64.dll;pyzbar" StartPiSunDesktop.py```

Или воспользоваться вкладкой Releases и скачать последнюю версию.

## Установка и запуск web версии

Для упрощенного использования был собран img образ для Raspberry Pi 3-4. Подробнее по ссылке: https://wiki.pidt.net/books/pisun-otkrytyi-kod-dlya-pigopidata

Для ручной установки на ОС Линукс следует выполнить следующие команды:


```bash
sudo apt install git python3 python3-pip python3-venv
git clone https://gitlab.com/piall1/pisun.git
cd pisun
python3 -m venv venv
venv/bin/pip3 install -r requirements.txt
sudo bash ./scripts/install.sh
```

В момент установки настраивается автозапуск при загрузке. Полезные комманды для управления автозапуском программы:

```bash
sudo systemctl status pisun.service     - посмотреть статус работы программы
sudo systemctl restart pisun.service    - перезапустить программу
sudo systemctl stop pisun.service       - остановить выполнение программы
sudo systemctl disable pisun.service    - выключить автозагрузку программы
sudo systemctl enable pisun.service     - включить автозагрузку программы
```

Для запуска программы вручную из терминала, находясь в корне репозитория, выполнить следующую команду:

```bash
sudo ./venv/bin/python3 StartPiSunRPI.py
```

## Лицензия

Данный программный код не лицензируется и может быть использован любым человеком.

Обсуждение кода в чате телеграм: https://t.me/pigo_official

![Logo](resources/image/Pi_sun_w_2_long.png)