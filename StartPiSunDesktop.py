import threading
import time
from src.connections import SerialPort, TcpClient
from src.converters import Parser, СonverterCNB, RtcmStreamHandler
from src.services import Controller, PointController, CSController, FTPController
from src.structures import SettingsComnav, SettingsJson
from src.output import LocalNtrip, RemoteNtrip, FileRecorder, NtripClient
from src.ui import Components
from src.utils.Console import cons
from src.utils import Buzzer


def get_alive_threads():
    alive_threads = []
    for thread in threading.enumerate():
        if thread.is_alive():
            alive_threads.append(thread)
    return alive_threads


def thread():
    active_threads = ["1"]
    while active_threads.__sizeof__() != 1:
        active_threads = get_alive_threads()
        print("\n")
        for thread in active_threads:
            print(f"Поток {thread.name} жив: {thread.is_alive()}")
        time.sleep(1)


def thread_auto_connect():
    try:
        params_dict = controller.load_params()
        auto_connect_value = params_dict["auto_connect"]
        if auto_connect_value:
            components.on_click_load_button()
            components.set_connect_callback()
    except Exception as e:
        print(f"Ошибка запуска: {e}")


if __name__ == "__main__":
    components = Components.Components()
    cons.configure(components.set_value_console)
    controller = Controller.Controller()
    serial_port = SerialPort.SerialManager()
    parser = Parser.ParserDataASCII()
    binparser = Parser.ParserDataBinary()
    settings_json = SettingsJson.SettingsJson()
    settings = SettingsComnav.SettingsComnav()
    ntrip_remote = RemoteNtrip.RemoteNtrip()
    ntrip_local = LocalNtrip.LocalNtrip()
    tcp_cli = TcpClient.TcpClient()
    frecord = FileRecorder.FileRecorder()
    null_port = SerialPort.SerialManager()
    ntrip_client = NtripClient.NtripClient()
    convert = СonverterCNB.ConverterCNB()
    points = PointController.PointController()
    rtcm_handler = RtcmStreamHandler.RtcmStreamHandler()
    cont_cs = CSController.ControllerCS()
    tcp_remote = TcpClient.TcpClient()
    ftp_cont = FTPController.FTPController()
    buzzer = Buzzer.Buzzer()

    controller.set_class_reference(serial_port, parser, binparser, settings_json,
                                   settings, ntrip_remote, ntrip_local, tcp_cli, frecord, null_port,
                                   ntrip_client, convert, points, rtcm_handler, cont_cs, tcp_remote,
                                   ftp_cont, buzzer)

    threading.Thread(target=thread_auto_connect, name="Autoconnect thread").start()
    components.root.mainloop()
