import threading
import time
import psutil
import os
from src.web            import WebServer
from src.connections    import SerialPort, TcpClient
from src.converters     import Parser, СonverterCNB, RtcmStreamHandler
from src.services       import Controller, PointController, CSController, FTPController, WifiController
from src.structures     import SettingsComnav, SettingsJson
from src.output         import LocalNtrip, RemoteNtrip, FileRecorder, NtripClient
from src.utils.Console  import cons
from src.utils          import Buzzer

port_webserver = 80


def get_process_by_port(port):
    for conn in psutil.net_connections(kind='inet'):
        if conn.laddr.port == port:
            process = psutil.Process(conn.pid)
            return process


def thread_auto_connect():
    try:
        params_dict = controller.load_params()
        if params_dict["auto_connect"]:
            controller.start_controller(params_dict)
    except Exception as e:
        print(f"Ошибка запуска: {e}")


if __name__ == "__main__":
    process = get_process_by_port(port_webserver)
    cons.configure(WebServer.console_handler)

    if process:
        print(f"Процесс '{process.name()}' с PID {process.pid} использует порт {port_webserver}")
        os.system(f"fuser -k {process.pid}/tcp")
    else:
        print(f"Порт {port_webserver} не используется ни одним процессом.")

    controller = Controller.Controller()
    serial_port = SerialPort.SerialManager()
    asciiparser = Parser.ParserDataASCII()
    binparser = Parser.ParserDataBinary()
    webs = WebServer.WebServer(port_webserver)
    settings_json = SettingsJson.SettingsJson()
    settings_comnav = SettingsComnav.SettingsComnav()
    ntrip_remote = RemoteNtrip.RemoteNtrip()
    ntrip_local = LocalNtrip.LocalNtrip()
    tcp_cli = TcpClient.TcpClient()
    file_recorder = FileRecorder.FileRecorder()
    out_com_port = SerialPort.SerialManager()
    ntrip_client = NtripClient.NtripClient()
    convert = СonverterCNB.ConverterCNB()
    points = PointController.PointController()
    rtcm_handler = RtcmStreamHandler.RtcmStreamHandler()
    cont_cs = CSController.ControllerCS()
    tcp_remote = TcpClient.TcpClient()
    ftp_cont = FTPController.FTPController()
    buzzer = Buzzer.Buzzer()
    wifi_cont = WifiController.WifiController()

    controller.set_class_reference(serial_port, asciiparser, binparser, settings_json, settings_comnav,
                                   ntrip_remote, ntrip_local, tcp_cli, file_recorder, out_com_port, ntrip_client,
                                   convert, points, rtcm_handler, cont_cs, tcp_remote, ftp_cont, buzzer)

    threading.Thread(target=thread_auto_connect, name="Autoconnect thread").start()

    for x in range(100):
        try:
            webs.start()
            break
        except Exception as e:
            print(f"Error: {e} Wait...")
            time.sleep(1)

    try:
        while True:
            time.sleep(10)
    except Exception:
        controller.stop_controller()
