document.addEventListener("DOMContentLoaded", _cnb_init);


function _cnb_init() {
    console.log('_cnb_init()');
    if(document.location.hostname != 'pisunrpi.local' ) {
        Settings.url = document.location.origin + '/';
    }
    renderMenu();
    getSettingsServer();
    getFiles();
    setProfile();
}

async function setProfile() {
    let iB = document.getElementById('infoBlock');

    let fetchRes = await fetch(Settings.url + 'api/authorization/get_profile');
    let data = await fetchRes.json();
    let _profile = data.profile;
    console.log('Имя профиля', data.profile);

    // В JavaScript для проверки на null или undefined используется "!=" или "!=="
    if (_profile == null) {
       iB.innerText = "Войдите в свой аккаунт перед использованием конвертора!";
       iB.classList.remove('d-none');
    } else {
       let _ib = document.getElementById('infoBlock');
       _ib.innerText = '';
       _ib.classList.add('d-none');
    }
}

function getFiles() {
    let postdata = { 'scope': 'user' };

    let options = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify(postdata)
    };

    let fetchRes = fetch(Settings.url + 'api/converter/result');
    fetchRes.then(res =>
        res.json()).then(d => {
        clearTable();
        insertTableData(d.responce);
    })
}

function clearTable() {
    console.log('clearTable()');
    let tbl = document.querySelector('#tblList');
    if(!tbl) {
        return;
    }

    tbl.removeChild(tbl.getElementsByTagName("tbody")[0]);
    tbl.appendChild(document.createElement("tbody"));
}


function insertTableData(data) {
    let tbl = document.getElementById('tblList').getElementsByTagName('tbody')[0];
    let tmpDateRow = '';

    let ic = 0;
    for(var i=0; i < data.length; i++) {
        let row = tbl.insertRow(ic);

        let tmpDate = new Date(data[i].time_mark * 1000);

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        //var cell4 = row.insertCell(3);

        cell1.innerHTML = '<small>'+ data[i].file_name + '</small>';
        cell2.innerHTML = '<small>'+ data[i].status +'</small>';
        if(data[i].status == 'ready') {
            cell3.innerHTML = '<small><a href="'+ data[i].link +'" target="_blank">download</a></small>';
        } else {
            cell3.innerHTML = ' ';
        }

        ic++;
    }

}
