document.addEventListener("DOMContentLoaded", _sat_cs);

function _sat_cs() {
    console.log('_sat_cs()');
    if(document.location.hostname != 'pisunrpi.local' ) {
        Settings.url = document.location.origin + '/';
    }
    renderMenu();
    getSettingsServer();
    getCoordsSystem();
    doSetSelectBox("EURA", "StandardSevenParameterStrictFormula", "PhysicalHeightResult_SourceSystem", "UnknownQuality", "UnknownQuality", "TransverseMercator");

}

let selectedUuid = null;

const elements_plate = ['Unknown', 'AFRC', 'ANTA', 'ARAB', 'AUST', 'CARB', 'COCO', 'EURA', 'INDI',
    'NOAM', 'NAZC', 'PCFC', 'SOAM', 'JUFU', 'PHIL', 'RIVR', 'SCOT'
];

const elements_computation = ["StandardSevenParameterApproximation", "StandardSevenParameterStrictFormula",
    "MolodenskiAbridged", "MolodenskiBadekas"
];

const elements_height = ["GeometricHeightResult", "PhysicalHeightResult_HelmertMolodenski",
    "PhysicalHeightResult_SourceSystem", "Reserved"
];

const elements_horizontal = ["UnknownQuality", "QualityBetterThan21mm", "Quality21to50mm", "Quality51to200mm",
    "Quality201to500mm", "Quality501to2000mm", "Quality2001to5000mm", "QualityWorseThan5001mm"
];

const elements_vertical = ["UnknownQuality", "QualityBetterThan21mm", "Quality21to50mm", "Quality51to200mm",
    "Quality201to500mm", "Quality501to2000mm", "Quality2001to5000mm", "QualityWorseThan5001mm"
];

const elements_projection = ["UnknownProjectionType", "TransverseMercator", "TransverseMercatorSouth", "LambertConicConformal1SP",
    "LambertConicConformal2SP", "LambertConicConformalWest", "CassiniSoldner", "ObliqueMercator", "ObliqueStereographic",
    "Mercator", "PolarStereographic", "DoubleStereographic"
];

async function getCoordsSystem() {
    clearTable();
    let fetchRes = await fetch(Settings.url + 'api/coords_system/list');
    let data = await fetchRes.json(); // извлекаем json-данные
    insertTableData(data.system_coord); // вставляем данные в таблицу
    return data.system_coord; // Возвращаем полученные данные
}

function doCreateCS() {

    let _name = document.getElementById('elDatum');
    let _source = document.getElementById('elSource');
    let _as_semi_major = document.getElementById('elAs_semi_major');
    let _s_1_div_f = document.getElementById('elS_1_div_f');

    let _target = document.getElementById('elTarget');
    let _at_semi_major = document.getElementById('elAt_semi_major');
    let _t_1_div_f = document.getElementById('elT_1_div_f');

    let _transx = document.getElementById('elTransX');
    let _transy = document.getElementById('elTransY');
    let _transz = document.getElementById('elTransZ');
    let _rotationX = document.getElementById('elRotationX');
    let _rotationy = document.getElementById('elRotationY');
    let _rotationz = document.getElementById('elRotationZ');
    let _scale_correction = document.getElementById('elScaleCorrection');

    let _latitudeN = document.getElementById('elLatitudeN');
    let _longitudeN = document.getElementById('elLongitudeN');
    let _false_easting = document.getElementById('elFalseEasting');
    let _false_northing = document.getElementById('elFalseNorthing');
    let _scale_factorat = document.getElementById('elScaleFactorat');

    let _select_plate = document.getElementById('elPlate').selectedOptions[0].value;
    let _select_computation = document.getElementById('elComputation').selectedOptions[0].value;
    let _select_height = document.getElementById('elHeight').selectedOptions[0].value;
    let _select_horizontal = document.getElementById('elHorizontal').selectedOptions[0].value;
    let _select_vertical = document.getElementById('elVertical').selectedOptions[0].value;
    let _select_projection = document.getElementById('elProjection').selectedOptions[0].value;

    let iB = document.getElementById('infoBlock');
    let iA = document.getElementById('infoBlockError');

    let error = false;

    if(_name.value == '') {
        _name.classList.add('text-bg-danger');
        error = true;
    }


    if(error == true) {
        return;
    }

    let res = {};
    res.Datum = _name.value;

    res.SourceName = _source.value;
    res.As_semi_major_axis = _as_semi_major.value;
    res.s_1_div_f = _s_1_div_f.value;

    res.TargetName = _target.value;
    res.At_semi_major_axis = _at_semi_major.value;
    res.t_1_div_f = _t_1_div_f.value;

    res.TranslationinX = _transx.value;
    res.TranslationinY = _transy.value;
    res.TranslationinZ = _transz.value;
    res.RotationAroundtheX = _rotationX.value;
    res.RotationAroundtheY = _rotationy.value;
    res.RotationAroundtheZ = _rotationz.value;
    res.ScaleCorrection = _scale_correction.value;

    res.LatitudeNatural = _latitudeN.value;
    res.LongitudeNatural = _longitudeN.value;
    res.FalseEasting = _false_easting.value;
    res.FalseNorthing = _false_northing.value;
    res.ScaleFactorat = _scale_factorat.value;

    res.PlateNumber = _select_plate
    res.ComputationIndicator = _select_computation
    res.HeightIndicator = _select_height
    res.HorizontalHelmert = _select_horizontal
    res.VerticalHelmert = _select_vertical
    res.ProjectionType = _select_projection


    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    let fetchRes = fetch(Settings.url + 'api/coords_system/save', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            clearForm();
            getCoordsSystem();

            iA.innerText = '';
            iA.classList.add('d-none');

            iB.innerText = d.message; // Используем iB для блока успешного редактирования
            iB.classList.remove('d-none');

            return;
        } else {
            iB.innerText = '';
            iB.classList.add('d-none');

            iA.innerText = d.message; // Используем iA для блока ошибок
            iA.classList.remove('d-none');
        }
    })
}

function doEditCS(){
    let _uuid = selectedUuid;

    let _name = document.getElementById('elDatum');

    let _source = document.getElementById('elSource');
    let _as_semi_major = document.getElementById('elAs_semi_major');
    let _s_1_div_f = document.getElementById('elS_1_div_f');

    let _target = document.getElementById('elTarget');
    let _at_semi_major = document.getElementById('elAt_semi_major');
    let _t_1_div_f = document.getElementById('elT_1_div_f');

    let _transx = document.getElementById('elTransX');
    let _transy = document.getElementById('elTransY');
    let _transz = document.getElementById('elTransZ');
    let _rotationX = document.getElementById('elRotationX');
    let _rotationy = document.getElementById('elRotationY');
    let _rotationz = document.getElementById('elRotationZ');
    let _scale_correction = document.getElementById('elScaleCorrection');

    let _latitudeN = document.getElementById('elLatitudeN');
    let _longitudeN = document.getElementById('elLongitudeN');
    let _false_easting = document.getElementById('elFalseEasting');
    let _false_northing = document.getElementById('elFalseNorthing');
    let _scale_factorat = document.getElementById('elScaleFactorat');

    let _select_plate = document.getElementById('elPlate').selectedOptions[0].value;
    let _select_computation = document.getElementById('elComputation').selectedOptions[0].value;
    let _select_height = document.getElementById('elHeight').selectedOptions[0].value;
    let _select_horizontal = document.getElementById('elHorizontal').selectedOptions[0].value;
    let _select_vertical = document.getElementById('elVertical').selectedOptions[0].value;
    let _select_projection = document.getElementById('elProjection').selectedOptions[0].value;

    let iB = document.getElementById('infoBlock');
    let iA = document.getElementById('infoBlockError');

    let error = false;

    if(_name.value == '') {
        _name.classList.add('text-bg-danger');
        error = true;
    }

    if(error == true) {
        return;
    }

    let res = {};
    res.uuid = _uuid

    res.Datum = _name.value;

    res.SourceName = _source.value;
    res.As_semi_major_axis = _as_semi_major.value;
    res.s_1_div_f = _s_1_div_f.value;

    res.TargetName = _target.value;
    res.At_semi_major_axis = _at_semi_major.value;
    res.t_1_div_f = _t_1_div_f.value;

    res.TranslationinX = _transx.value;
    res.TranslationinY = _transy.value;
    res.TranslationinZ = _transz.value;
    res.RotationAroundtheX = _rotationX.value;
    res.RotationAroundtheY = _rotationy.value;
    res.RotationAroundtheZ = _rotationz.value;
    res.ScaleCorrection = _scale_correction.value;

    res.LatitudeNatural = _latitudeN.value;
    res.LongitudeNatural = _longitudeN.value;
    res.FalseEasting = _false_easting.value;
    res.FalseNorthing = _false_northing.value;
    res.ScaleFactorat = _scale_factorat.value;

    res.PlateNumber = _select_plate
    res.ComputationIndicator = _select_computation
    res.HeightIndicator = _select_height
    res.HorizontalHelmert = _select_horizontal
    res.VerticalHelmert = _select_vertical
    res.ProjectionType = _select_projection

    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    let fetchRes = fetch(Settings.url + 'api/coords_system/edit', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            clearForm();
            getCoordsSystem();

            iA.innerText = '';
            iA.classList.add('d-none');

            iB.innerText = d.message; // Используем iB для блока успешного редактирования
            iB.classList.remove('d-none');

            document.getElementById('btnEdit').classList.add('hidden');

            return;
        } else {
            iB.innerText = '';
            iB.classList.add('d-none');

            iA.innerText = d.message; // Используем iA для блока ошибок
            iA.classList.remove('d-none');
        }
    })
}

function deleteCS(key, uuid){
    let iB = document.getElementById('infoBlock');
    let iA = document.getElementById('infoBlockError');

    let res = { "uuid": uuid }
    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    let fetchRes = fetch(Settings.url + 'api/coords_system/delete', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            clearForm();
            getCoordsSystem();

            iA.innerText = '';
            iA.classList.add('d-none');

            iB.innerText = d.message; // Используем iB для блока успешного редактирования
            iB.classList.remove('d-none');
            return;
        } else {
            iB.innerText = '';
            iB.classList.add('d-none');

            iA.innerText = d.message; // Используем iA для блока ошибок
            iA.classList.remove('d-none');
        }
    })
}

function insertTableData(data) {
    let tbl = document.getElementById('tblListCS').getElementsByTagName('tbody')[0];
    let tmpDateRow = '';

    let ic = 0;
    for(var i=0; i < data.length; i++) {
        let row = tbl.insertRow(ic);

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);

        cell1.innerHTML = data[i].Datum;
        cell2.innerHTML = data[i].SourceName
        cell3.innerHTML = data[i].TargetName

        cell4.innerHTML = '<button class="btn btn-sm btn-dark py-0" id="b'+ i +'" onclick="SelectCS('+ i +',\''
            + data[i].uuid +'\')" title="Выбрать CK">&check;</button>'

        cell5.innerHTML = '<button class="btn btn-sm btn-danger py-0" id="b'+ i +'" onclick="deleteCS('+ i +',\''
            + data[i].uuid +'\')" title="Удалить CK">х</button>'

        ic++;
    }
}

function setCoordsSystem(item) {
    document.getElementById('elDatum').value = item.Datum;

    document.getElementById('elSource').value = item.SourceName;
    document.getElementById('elAs_semi_major').value = item.As_semi_major_axis;
    document.getElementById('elS_1_div_f').value = item.s_1_div_f;

    document.getElementById('elTarget').value = item.TargetName;
    document.getElementById('elAt_semi_major').value = item.At_semi_major_axis;
    document.getElementById('elT_1_div_f').value = item.t_1_div_f;

    document.getElementById('elTransX').value = item.TranslationinX;
    document.getElementById('elTransY').value = item.TranslationinY;
    document.getElementById('elTransZ').value = item.TranslationinZ;
    document.getElementById('elRotationX').value = item.RotationAroundtheX;
    document.getElementById('elRotationY').value = item.RotationAroundtheY;
    document.getElementById('elRotationZ').value = item.RotationAroundtheZ;
    document.getElementById('elScaleCorrection').value = item.ScaleCorrection;

    document.getElementById('elLatitudeN').value = item.LatitudeNatural;
    document.getElementById('elLongitudeN').value = item.LongitudeNatural;
    document.getElementById('elFalseEasting').value = item.FalseEasting;
    document.getElementById('elFalseNorthing').value = item.FalseNorthing;
    document.getElementById('elScaleFactorat').value = item.ScaleFactorat;

    let plate = item.PlateNumber;
    let computation = item.ComputationIndicator;
    let height = item.HeightIndicator;
    let horizontal = item.HorizontalHelmert;
    let vertical = item.VerticalHelmert;
    let projection = item.ProjectionType;

    doSetSelectBox(plate, computation, height, horizontal, vertical, projection);
}

async function SelectCS(key, uuid) {
    let list_params = await getCoordsSystem();
    console.log('Полученные параметры:', list_params);

    let selectedItem = list_params.find(item => item["uuid"] === uuid);

    if (selectedItem) {
        setCoordsSystem(selectedItem);
        setUuid(selectedItem)
        document.getElementById('btnEdit').classList.remove('hidden');

    } else {
        console.error('Элемент с uuid', uuid, 'не найден');
    }

}

function setUuid(item) {
    selectedUuid = item.uuid;
}

function doSetSelectBox(select_plate, select_computation, select_height, select_horizontal, select_vertical, select_projection){
    let selectBoxPlate = document.querySelector('#elPlate');
    while (selectBoxPlate.options.length > 1) {
        selectBoxPlate.remove(1);
    }

    elements_plate.forEach((element) => {
    if(select_plate==element) {
        selectBoxPlate.add(
            new Option(element,element, true, true),
            undefined
        );
    } else {
        selectBoxPlate.add(new Option(element,element), undefined);
        }
    });

    let selectBoxComputation = document.querySelector('#elComputation');
    while (selectBoxComputation.options.length > 1) {
        selectBoxComputation.remove(1);
    }

    elements_computation.forEach((element) => {
    if(select_computation==element) {
        selectBoxComputation.add(
            new Option(element,element, true, true),
            undefined
        );
    } else {
        selectBoxComputation.add(new Option(element,element), undefined);
        }
    });

    let selectBoxHeight = document.querySelector('#elHeight');
    while (selectBoxHeight.options.length > 1) {
        selectBoxHeight.remove(1);
    }

    elements_height.forEach((element) => {
    if(select_height==element) {
        selectBoxHeight.add(
            new Option(element,element, true, true),
            undefined
        );
    } else {
        selectBoxHeight.add(new Option(element,element), undefined);
        }
    });

    let selectBoxHorizontal = document.querySelector('#elHorizontal');
    while (selectBoxHorizontal.options.length > 1) {
        selectBoxHorizontal.remove(1);
    }

    elements_horizontal.forEach((element) => {
    if(select_horizontal==element) {
        selectBoxHorizontal.add(
            new Option(element,element, true, true),
            undefined
        );
    } else {
        selectBoxHorizontal.add(new Option(element,element), undefined);
        }
    });

    let selectBoxVertical = document.querySelector('#elVertical');
    while (selectBoxVertical.options.length > 1) {
        selectBoxVertical.remove(1);
    }

    elements_vertical.forEach((element) => {
    if(select_vertical==element) {
        selectBoxVertical.add(
            new Option(element,element, true, true),
            undefined
        );
    } else {
        selectBoxVertical.add(new Option(element,element), undefined);
        }
    });

    let selectBoxProjection = document.querySelector('#elProjection');
    while (selectBoxProjection.options.length > 1) {
        selectBoxProjection.remove(1);
    }

    elements_projection.forEach((element) => {
    if(select_projection==element) {
        selectBoxProjection.add(
            new Option(element,element, true, true),
            undefined
        );
    } else {
        selectBoxProjection.add(new Option(element,element), undefined);
        }
    });
}

function handleFileSelect(event) {
    const fileInput = event.target;
    const file = fileInput.files[0];

    if (file) {
        console.log('Selected file path:', file.name);
        uploadFile(file);
    } else {
        console.log('No file selected');
    }
}

function uploadFile(file) {
    const formData = new FormData();
    formData.append('file', file);

    let fetchRes = fetch(Settings.url + 'api/coords_system/upload_gr_code', {
        method: 'POST',
        body: formData
    })
    fetchRes.then(formData =>
        formData.json()).then(d => {
        if(d.status == 'ok') {
            console.log(d.message);
            setQRParams()
            return;
        } else {
            console.log(d.message);
        }
    })
}

async function setQRParams() {
    let iB = document.getElementById('infoBlock');
    let iA = document.getElementById('infoBlockError');

    try {
        let fetchRes = await fetch(Settings.url + 'api/coords_system/params_qr');
        let params_qr = await fetchRes.json();
        if (!params_qr || Object.keys(params_qr).length === 0) {

            iB.innerText = '';
            iB.classList.add('d-none');

            iA.innerText = "Система координат не найдена"; // Используем iA для блока ошибок
            iA.classList.remove('d-none');
            return;
        }
        clearForm();
        setCoordsSystem(params_qr);

        iA.innerText = '';
        iA.classList.add('d-none');

        iB.innerText = "Импортирована система координат" // Используем iB для блока успешного редактирования
        iB.classList.remove('d-none');

    } catch (error) {
        console.error('Fetch error:', error);
    }
}

function clearTable() {
    console.log('clearTable()');
    let tbl = document.querySelector('#tblListCS');
    if(!tbl) {
        return;
    }

    tbl.removeChild(tbl.getElementsByTagName("tbody")[0]);
    tbl.appendChild(document.createElement("tbody"));
}

function clearForm() {
    let _name = document.getElementById('elDatum');
    let _source = document.getElementById('elSource');
    let _as_semi_major = document.getElementById('elAs_semi_major');
    let _s_1_div_f = document.getElementById('elS_1_div_f');

    let _target = document.getElementById('elTarget');
    let _at_semi_major = document.getElementById('elAt_semi_major');
    let _t_1_div_f = document.getElementById('elT_1_div_f');

    let _transx = document.getElementById('elTransX');
    let _transy = document.getElementById('elTransY');
    let _transz = document.getElementById('elTransZ');
    let _rotationX = document.getElementById('elRotationX');
    let _rotationy = document.getElementById('elRotationY');
    let _rotationz = document.getElementById('elRotationZ');
    let _scale_correction = document.getElementById('elScaleCorrection');

    let _latitudeN = document.getElementById('elLatitudeN');
    let _longitudeN = document.getElementById('elLongitudeN');
    let _false_easting = document.getElementById('elFalseEasting');
    let _false_northing = document.getElementById('elFalseNorthing');
    let _scale_factorat = document.getElementById('elScaleFactorat');

    let _select_plate = document.getElementById('elPlate').selectedOptions[0].value;
    let _select_computation = document.getElementById('elComputation').selectedOptions[0].value;
    let _select_height = document.getElementById('elHeight').selectedOptions[0].value;
    let _select_horizontal = document.getElementById('elHorizontal').selectedOptions[0].value;
    let _select_vertical = document.getElementById('elVertical').selectedOptions[0].value;
    let _select_projection = document.getElementById('elProjection').selectedOptions[0].value;

    _name.value = "";
    _source.value = "";
    _as_semi_major.value = "";
    _s_1_div_f.value = "";
    _target.value = "";
    _at_semi_major.value = "";
    _t_1_div_f.value = "";
    _transx.value = "";
    _transy.value = "";
    _transz.value = "";
    _rotationX.value = "";
    _rotationy.value = "";
    _rotationz.value = "";
    _scale_correction.value = "";
    _latitudeN.value = "";
    _longitudeN.value = "";
    _false_easting.value = "";
    _false_northing.value = "";
    _scale_factorat.value = "";
    _select_plate.value = "";
    _select_computation.value = "";
    _select_height.value = "";
    _select_horizontal.value = "";
    _select_vertical.value = "";
    _select_projection.value = "";

}