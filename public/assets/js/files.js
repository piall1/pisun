document.addEventListener("DOMContentLoaded", _files_init);


function _files_init() {
    console.log('_files_init()');
    if(document.location.hostname != 'pisunrpi.local' ) {
        Settings.url = document.location.origin + '/';
    }
    renderMenu();
    getSettingsServer();
    getFiles();
}

function getFiles() {
    let fetchRes = fetch(Settings.url + 'api/files/list');
    fetchRes.then(res =>
        res.json()).then(d => {
        clearTable();
        insertTableData(d.files);
        console.log('getFiles()->result', d)
    })
}

function convertFile(t, name) {
    if(confirm('Конвертировать файл в rinex?')) {
        let postdata = { filename: name};
        let options = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(postdata)
        };
        let fetchRes = fetch(Settings.url + 'api/converter/convert', options);
        fetchRes.then(res =>
            res.json()).then(d => {
                if(d.status == 'ok') {
                    var tb = document.getElementById('b' + t);
                    tb.style.display = 'none';
                } else {
                    showToast(d.message, d.status);
                }
        })
        return;
    }
}

function deleteFile(name) {
    if(confirm('Delete file?')) {
        console.log('delete file');

        let postdata = { filename: name};

        let options = {
            method: "POST",
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify(postdata)
        };
        let fetchRes = fetch(Settings.url + 'api/files/delete', options);
        fetchRes.then(res =>
            res.json()).then(d => {
            showToast(d.message, d.status);
            getFiles()
        })

        return;
    }
}

function clearTable() {
    console.log('clearTable()');
    let tbl = document.querySelector('#tblList');
    if(!tbl) {
        return;
    }

    tbl.removeChild(tbl.getElementsByTagName("tbody")[0]);
    tbl.appendChild(document.createElement("tbody"));
}


function insertTableData(data) {
    let tbl = document.getElementById('tblList').getElementsByTagName('tbody')[0];
    let tmpDateRow = '';

    let ic = 0;
    for(var i=0; i < data.length; i++) {
        let row = tbl.insertRow(ic);

        let tmpDate = new Date(data[i].time_mark * 1000);
        let dateRow = tmpDate.getFullYear() + '-' + tmpDate.getMonth() + '-' + tmpDate.getDate();
        if(tmpDateRow != dateRow) {
            row.classList.add('table-secondary');
            var cell = row.insertCell(0);
            cell.colSpan = 4;
            cell.classList.add('text-center');
            cell.classList.add('small');
            cell.innerHTML = dateRow;
            tmpDateRow = dateRow;
            ic++;
            row = tbl.insertRow(ic);
        }

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);

        cell1.innerHTML = '<small><a href="/api/files/get/'+ data[i].filename +'" target="_blank">'
            + data[i].filename +'</a></small>';
        cell2.innerHTML = '<small>'+ tmpDate.getHours() + ':'+ tmpDate.getMinutes() +'</small>';
        cell3.innerHTML = '<small>'+ humanFileSize(data[i].size) +'</small>';
        cell4.innerHTML = '<button class="btn btn-sm btn-dark py-0" id="b'+ i +'" onclick="convertFile('+ i +',\''
            + data[i].filename +'\')" title="Конвертировать файл">@</button>&nbsp;'
            + '<button class="btn btn-sm btn-danger py-0" onclick="deleteFile(\''
            + data[i].filename+'\');" title="Удалить файл">х</button>'

        ic++;
    }

}






