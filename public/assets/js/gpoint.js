document.addEventListener("DOMContentLoaded", _gpoint_init);

let selectedUuid = null;

function _gpoint_init() {
    console.log('_gpoint_init()');
    if(document.location.hostname != 'pisunrpi.local' ) {
        Settings.url = document.location.origin + '/';
    }
    renderMenu();
    getSettingsServer();
    getPoints();
}

async function getPoints() {
    clearTable();
    let fetchRes = await fetch(Settings.url + 'api/point/list');
    let data = await fetchRes.json();
    insertTableData(data.points);
    return data.points;
}

function syncPoints() {
    let iB = document.getElementById('infoBlockAddPoint');
    clearTable();
    let fetchRes = fetch(Settings.url + 'api/point/sync');
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            insertTableData(d.points)
            showToast(d.message, d.status);
            return;
        } else {
             showToast(d.message, d.status);
             getPoints();
        }
    })
}

function clearTable() {
    console.log('clearTable()');
    let tbl = document.querySelector('#tblList');
    if(!tbl) {
        return;
    }

    tbl.removeChild(tbl.getElementsByTagName("tbody")[0]);
    tbl.appendChild(document.createElement("tbody"));
}


function insertTableData(data) {
    let tbl = document.getElementById('tblList').getElementsByTagName('tbody')[0];
    let tmpDateRow = '';

    let ic = 0;
    for(var i=0; i < data.length; i++) {
        let row = tbl.insertRow(ic);

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);
        var cell5 = row.insertCell(4);

        cell1.innerHTML = data[i].name;
        cell2.innerHTML = '<small>' + data[i].data.lat + '</small><br>'
                    + '<small>' + floatToString(data[i].data.lon)  + '</small><br>'
                    + '<small>' + floatToString(data[i].data.height)  + '</small>'

        cell4.innerHTML = '<button class="btn btn-sm btn-dark py-0" id="b'+ i +'" onclick="selectPoint('+ i +',\''
            + data[i].uuid +'\')" title="Выбрать точку">&check;</button>'

        cell5.innerHTML = '<button class="btn btn-sm btn-danger py-0" id="b'+ i +'" onclick="deletePoint('+ i +',\''
            + data[i].uuid +'\')" title="Удалить точку">х</button>'

        ic++;
    }

}

function doEdit() {
    let _uuid = selectedUuid;
    let _name = document.getElementById('elName');
    let _x = document.getElementById('elCX');
    let _y = document.getElementById('elCY');
    let _z = document.getElementById('elCZ');
    let iB = document.getElementById('infoBlockSuccess');
    let iA = document.getElementById('infoBlockError');

    let error = false;

    if(_name.value == '') {
        _name.classList.add('text-bg-danger');
        error = true;
    }
    if(_x.value == '') {
        _x.classList.add('text-bg-danger');
        error = true;
    }
    if(_y.value == '') {
        _y.classList.add('text-bg-danger');
        error = true;
    }
    if(_z.value == '') {
        _z.classList.add('text-bg-danger');
        error = true;
    }

    if(error == true) {
        return;
    }

    let res = {};
    res.uuid = _uuid
    res.name = _name.value;
    res.data = {};
    res.data.lat = _x.value
    res.data.lon = _y.value
    res.data.height = _z.value

    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    let fetchRes = fetch(Settings.url + 'api/point/edit', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            clearForm();
            getPoints();

            iA.innerText = '';
            iA.classList.add('d-none');
            iB.innerText = d.message; // Используем iB для блока успешного редактирования
            iB.classList.remove('d-none');

            document.getElementById('btnEdit').classList.add('hidden');

            return;
        } else {
            iB.innerText = '';
            iB.classList.add('d-none');
            iA.innerText = d.message; // Используем iA для блока ошибок
            iA.classList.remove('d-none');
        }
    })
}

function setSelectPoint(item) {
    document.getElementById('elName').value = item.name;
    document.getElementById('elCX').value = item.data.lat;
    document.getElementById('elCY').value = item.data.lon;
    document.getElementById('elCZ').value = item.data.height;
}

async function selectPoint(key, uuid) {
    let list_params = await getPoints();

    let selectedItem = list_params.find(item => item["uuid"] === uuid);

    if (selectedItem) {
        setSelectPoint(selectedItem);
        setUuid(selectedItem)
        document.getElementById('btnEdit').classList.remove('hidden');

    } else {
        console.error('Элемент с uuid', uuid, 'не найден');
    }

}

function setUuid(item) {
    selectedUuid = item.uuid;
}


function deletePoint(key, uuid) {
    let iB = document.getElementById('infoBlockSuccess');
    let iA = document.getElementById('infoBlockError');
    let res = { "uuid": uuid }
    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    let fetchRes = fetch(Settings.url + 'api/point/delete', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            clearForm();
            getPoints();

            iB.innerText = d.message; // Используем iB для блока успешного редактирования
            iB.classList.remove('d-none');
            iA.innerText = '';
            iA.classList.add('d-none');
            return;
        } else {
            iA.innerText = d.message; // Используем iA для блока ошибок
            iA.classList.remove('d-none');
            iB.innerText = '';
            iB.classList.add('d-none');
        }
    })

}

function doCreate() {
    let _name = document.getElementById('elName');
    let _x = document.getElementById('elCX');
    let _y = document.getElementById('elCY');
    let _z = document.getElementById('elCZ');
    let iB = document.getElementById('infoBlockSuccess');
    let iA = document.getElementById('infoBlockError');

    let error = false;

    if(_name.value == '') {
        _name.classList.add('text-bg-danger');
        error = true;
    }
    if(_x.value == '') {
        _x.classList.add('text-bg-danger');
        error = true;
    }
    if(_y.value == '') {
        _y.classList.add('text-bg-danger');
        error = true;
    }
    if(_z.value == '') {
        _z.classList.add('text-bg-danger');
        error = true;
    }

    if(error == true) {
        return;
    }

    let res = {};
    res.name = _name.value;
    res.data = {};
    res.data.lat = _x.value
    res.data.lon = _y.value
    res.data.height = _z.value

    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    let fetchRes = fetch(Settings.url + 'api/point/save', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            clearForm();
            getPoints();

            iB.innerText = d.message; // Используем iB для блока успешного редактирования
            iB.classList.remove('d-none');
            iA.innerText = '';
            iA.classList.add('d-none');
            return;
        } else {
            iA.innerText = d.message; // Используем iA для блока ошибок
            iA.classList.remove('d-none');
            iB.innerText = '';
            iB.classList.add('d-none');
        }
    })

}

function clearTable() {
    console.log('clearTable()');
    let tbl = document.querySelector('#tblList');
    if(!tbl) {
        return;
    }

    tbl.removeChild(tbl.getElementsByTagName("tbody")[0]);
    tbl.appendChild(document.createElement("tbody"));
}

function clearForm() {
    let _name = document.getElementById('elName');
    let _x    = document.getElementById('elCX');
    let _y    = document.getElementById('elCY');
    let _z    = document.getElementById('elCZ');
    _name.value = '';
    _x.value = '';
    _y.value = '';
    _z.value = '';

}
