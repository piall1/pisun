
function getSettingsServer() {
    console.log('getStatus()')
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    let fetchRes = fetch(Settings.url + 'api/status/get' , options);
    fetchRes.then(res =>
        res.json()).then(d => {
        Settings.params = d;
        document.getElementById('firmwareVersion').innerText = Settings.params.firmware_version;
    })
}

function humanFileSize(bytes, si= true, dp=1) {
    const thresh = si ? 1000 : 1024;

    if (Math.abs(bytes) < thresh) {
        return bytes + ' B';
    }

    const units = si
        ? ['kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB']
        : ['KiB', 'MiB', 'GiB', 'TiB', 'PiB', 'EiB', 'ZiB', 'YiB'];
    let u = -1;
    const r = 10**dp;

    do {
        bytes /= thresh;
        ++u;
    } while (Math.round(Math.abs(bytes) * r) / r >= thresh && u < units.length - 1);


    return bytes.toFixed(dp) + ' ' + units[u];
}


function showToast(message, status) {
    let toast = document.getElementById('liveToast');
    toast.querySelector('.toast-body').innerHTML = message;

    if(status == 'ok') {
        toast.classList.remove();
        toast.classList.add('toast');
        toast.classList.add('text-bg-success');
    } else if(status == 'error') {
        toast.classList.remove();
        toast.classList.add('toast');
        toast.classList.add('text-bg-danger');
    }

    let divToast = new bootstrap.Toast(toast);
    divToast.show();
}


function floatToString(num) {
    return num.toString()
    //return num.toFixed(Math.max(1, num.toString().substr(num.toString().indexOf(".")+1).length));
}
