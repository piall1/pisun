document.addEventListener("DOMContentLoaded", _login_init);


function _login_init() {
    if(document.location.hostname != 'pisunrpi.local' ) {
        Settings.url = document.location.origin + '/';
    }

    renderMenu();
    getSettingsServer();
    setProfile();

    if(localStorage.getItem('telegram_id') ) {
        let fL = document.getElementById('elLogin');
        if(fL.value == '') {
            fL.value = localStorage.getItem('telegram_id');
        }
    }
}

let stepLogin = 1;
function doLogin() {
    if(stepLogin === 1) {
        _doLogin1();
    } else {
        _doLogin2();
    }
}

function _doLogin1() {
    let fL = document.getElementById('elLogin');
    let fP = document.getElementById('elPin');
    let fC = document.getElementById('elCode');
    let iB = document.getElementById('infoBlockSuccess');
    let iA = document.getElementById('infoBlockError');


    let error = false;
    if(fL.value == '') {
        fL.classList.add('text-bg-danger')
        error = true;
    } else {
        fL.classList.remove('text-bg-danger')
    }
    if(fP.value == '') {
        fP.classList.add('text-bg-danger')
    } else {
        fP.classList.remove('text-bg-danger')
    }

    if(error) {
       return;
    }

    let res = {};
    res.telegram_id = fL.value;
    res.pwd         = fP.value;

    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    let fetchRes = fetch(Settings.url + 'api/authorization/authorize', options);
    fetchRes.then(res =>
        res.json()).then(d => {
            if(d.status == 'ok') {
                fL.disabled = true;
                fP.disabled = true;
                stepLogin = 2;
                fC.disabled = false;

                iA.innerText = '';
                iA.classList.add('d-none');
                iB.innerText = d.message; // Используем iB для блока успешного редактирования
                iB.classList.remove('d-none');
                return;
            } else {
                iB.innerText = '';
                iB.classList.add('d-none');
                iA.innerText = d.message; // Используем iA для блока ошибок
                iA.classList.remove('d-none');
            }
    })
}


function _doLogin2() {
    let fL = document.getElementById('elLogin');
    let fC = document.getElementById('elCode');
    let iB = document.getElementById('infoBlockSuccess');
    let iA = document.getElementById('infoBlockError');

    let res = {};
    res.pin = fC.value;

    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    let fetchRes = fetch(Settings.url + 'api/authorization/pin', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            localStorage.setItem('telegram_id', fL.value)

            iA.innerText = '';
            iA.classList.add('d-none');
            iB.innerText = d.message; // Используем iB для блока успешного редактирования
            iB.classList.remove('d-none');

            setProfile();

            return;
        } else {
            stepLogin = 2;
            _login_init();

            iB.innerText = '';
            iB.classList.add('d-none');
            iA.innerText = d.message; // Используем iA для блока ошибок
            iA.classList.remove('d-none');
        }
    })
}

async function setProfile() {
    let elProfile = document.getElementById('elProfile');
    let fetchRes = await fetch(Settings.url + 'api/authorization/get_profile');
    let data = await fetchRes.json();
    elProfile.textContent = data.profile;
}

function doExit() {
    let iB = document.getElementById('infoBlockSuccess');
    let iA = document.getElementById('infoBlockError');

    let fetchRes = fetch(Settings.url + 'api/authorization/log_out');

    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            clearForm();

            iA.innerText = '';
            iA.classList.add('d-none');
            iB.innerText = d.message; // Используем iB для блока успешного редактирования
            iB.classList.remove('d-none');

            unBlock();
            stepLogin = 1;
            _login_init();
            return;
        } else {
            iB.innerText = '';
            iB.classList.add('d-none');
            iA.innerText = d.message; // Используем iA для блока ошибок
            iA.classList.remove('d-none');
        }
    })
}

function unBlock(){
    let log = document.getElementById('elLogin');
    let pin = document.getElementById('elPin');
    let code = document.getElementById('elCode');

    log.disabled = false;
    pin.disabled = false;
    code.disabled = true;
}

function clearForm(){
    let _login = document.getElementById('elLogin');
    let _pwd    = document.getElementById('elPin');
    let _code    = document.getElementById('elCode');
    let _profile    = document.getElementById('elProfile');
    _login.value = '';
    _pwd.value = '';
    _code.value = '';
    _profile.textContent = '';

}