// Action click button "Connect"
// send params to server

document.addEventListener("DOMContentLoaded", _init);


function _init() {
    console.log('_init');
    if(document.location.hostname != 'pisunrpi.local' ) {
        Settings.url = document.location.origin + '/';
    }
    getInitStatus();
    renderMenu();
    setInterval(getStatus, 3000);
}

function getStatus() {
    console.log('getStatus()')
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' },
    };
    let fetchRes = fetch(Settings.url + 'api/status/get'+ (Settings.full_console ? '?console=full' : '') , options);
    fetchRes.then(res =>
        res.json()).then(d => {
        Settings.params = d;
//        console.log("Settings", d, Settings)
        //Settings.params.console = ['row!', new Date().getTime()]
        applyParams();
        workStatus();
        writeConsole();
        Settings.full_console = false;
    })
}

function getInitStatus() {
    console.log('getInitStatus()')
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
    let fetchRes = fetch(Settings.url + 'api/status/get?console=full', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        //Settings.params = d;
        Settings.params = d;
        console.log(d, Settings)
        applyParams();
        loadSettings();
        workStatus();
    })
}


function sendConnect() {
    let res = getFormData();
    let error = '';
    console.log('function sendConnect()',res)

    // let elInvalid = document.querySelector('.is-invalid');
    // console.log(elInvalid);

    let selectBox = document.querySelector('#elPorts');
    selectBox.classList.remove('is-invalid');
    let inputTcpPort = document.querySelector('#elNtripTcpPort');
    inputTcpPort.classList.remove('is-invalid');
    let inputCasterHost = document.querySelector('#elCasterHost');
    inputCasterHost.classList.remove('is-invalid');
    let inputCasterPort = document.querySelector('#elCasterPort');
    inputCasterPort.classList.remove('is-invalid');
    let inputCasterMountPoint = document.querySelector('#elCasterMountPoint');
    inputCasterMountPoint.classList.remove('is-invalid');
    let inputCasterPassword = document.querySelector('#elCasterPassword');
    inputCasterPassword.classList.remove('is-invalid');
    let EntryInterval = document.querySelector('#elEntryInterval');
    EntryInterval.classList.remove('is-invalid');

    if(res.port_name == '') {
        error = true;
        selectBox.classList.add('is-invalid');
    }
    if(res.tcp_local && !res.tcp_local_port) {
        error = true;
        inputTcpPort.classList.add('is-invalid');
    }
    if(res.entry_interval_rtcm == '') {
        error = true;
        EntryInterval.classList.add('is-invalid');
    }

    if(error) {
        showToast('Не заполнены необходимые данные', 'error');
        return;
    }

    // send to server
    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };
    let fetchRes = fetch(Settings.url + 'api/pihatel/start', options);
    fetchRes.then(res =>
        res.json()).then(d => {
            if(d.status == 'ok') {
                Settings.params.work_status = 'processing';
                workStatus();
                buttonLock(false);
            }
        console.log('sendConnect()->result', d);
        showToast(d.message, d.status);
    })
}

function sendStop() {
    let res = getFormData();
    console.log('function sendStop()',res)
    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };
    let fetchRes = fetch(Settings.url + 'api/pihatel/stop', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            Settings.params.work_status = 'default';
            workStatus();
            buttonLock(true);
        }
        console.log('sendStop()->result', d);
        showToast(d.message, d.status);
    })
}


function workStatus() {
    console.log('workStatus()', Settings);
    if(Settings.params.work_status == 'default') {
        document.getElementById('btnConnect').classList.remove('visually-hidden');
        document.getElementById('btnProcess').classList.add('visually-hidden');
        document.getElementById('btnError').classList.add('visually-hidden');
    } else if(Settings.params.work_status == 'processing') {
        document.getElementById('btnConnect').classList.add('visually-hidden');
        document.getElementById('btnProcess').classList.remove('visually-hidden');
        document.getElementById('btnError').classList.add('visually-hidden');
    } else if(Settings.params.work_status == 'error') {
        document.getElementById('btnConnect').classList.add('visually-hidden');
        document.getElementById('btnProcess').classList.add('visually-hidden');
        document.getElementById('btnError').classList.remove('visually-hidden');
    }
}

function loadSettings() {
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
    let fetchRes = fetch(Settings.url + 'api/params/get', options);

    fetchRes.then(res =>
        res.json()).then(d => {

            console.log('loadSettings()', d);
            Settings.params.coords_status = d.coords_status;
            Settings.params.coords_input  = d.coords_input;

            Settings.load  = d;
            applyForms();
    })
}


function saveSettings() {
    let res = getFormData();

    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };
    let fetchRes = fetch(Settings.url + 'api/params/set', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        console.log('saveSettings()->result', d);
        showToast(d.message, d.status);
    })
}

var myModalCS = new bootstrap.Modal(document.getElementById('myModalCS'), [])
function dialogCS() {
    var options = [];
    myModalCS.show();
    getCoordsSystem();
}

async function getCoordsSystem() {
    clearTableCS();
    let fetchRes = await fetch(Settings.url + 'api/coords_system/list');
    let data = await fetchRes.json();
    insertTableDataCS(data.system_coord);
    return data.system_coord;
}

function clearTableCS() {
    console.log('clearTableCS()');
    let tbl = document.querySelector('#tblListCS');
    if(!tbl) {
        return;
    }

    tbl.removeChild(tbl.getElementsByTagName("tbody")[0]);
    tbl.appendChild(document.createElement("tbody"));
}

function insertTableDataCS(data) {
    let tbl = document.getElementById('tblListCS').getElementsByTagName('tbody')[0];
    let tmpDateRow = '';

    let ic = 0;
    for(var i=0; i < data.length; i++) {
        let row = tbl.insertRow(ic);

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);

        cell1.innerHTML = data[i].Datum;
        cell2.innerHTML = data[i].SourceName
        cell3.innerHTML = data[i].TargetName

        cell4.innerHTML = '<button class="btn btn-sm btn-dark py-0" id="b'+ i +'" onclick="SelectCS('+ i +',\''
            + data[i].uuid +'\')" title="Выбрать CK">&check;</button>'

        ic++;
    }
}

async function SelectCS(key, uuid) {
    let list_params = await getCoordsSystem();
    console.log('Полученная СК:', list_params);

    let selectedItem = list_params.find(item => item["uuid"] === uuid);

    if (selectedItem) {
        let elDatum = document.getElementById('elNameDatum');
        elDatum.textContent = selectedItem.Datum;
        myModalCS.hide();

        let res = {};
        res.Datum = selectedItem.Datum;

        res.SourceName = selectedItem.SourceName;
        res.As_semi_major_axis = selectedItem.As_semi_major_axis;
        res.s_1_div_f = selectedItem.s_1_div_f;
        res.Bs_semi_minor_axis = selectedItem.Bs_semi_minor_axis;

        res.TargetName = selectedItem.TargetName;
        res.At_semi_major_axis = selectedItem.At_semi_major_axis;
        res.t_1_div_f = selectedItem.t_1_div_f;
        res.Bt_semi_minor_axis = selectedItem.Bt_semi_minor_axis;


        res.TranslationinX = selectedItem.TranslationinX;
        res.TranslationinY = selectedItem.TranslationinY;
        res.TranslationinZ = selectedItem.TranslationinZ;
        res.RotationAroundtheX = selectedItem.RotationAroundtheX;
        res.RotationAroundtheY = selectedItem.RotationAroundtheY;
        res.RotationAroundtheZ = selectedItem.RotationAroundtheZ;
        res.ScaleCorrection = selectedItem.ScaleCorrection;

        res.LatitudeNatural = selectedItem.LatitudeNatural;
        res.LongitudeNatural = selectedItem.LongitudeNatural;
        res.FalseEasting = selectedItem.FalseEasting;
        res.FalseNorthing = selectedItem.FalseNorthing;
        res.ScaleFactorat = selectedItem.ScaleFactorat;

        res.PlateNumber = selectedItem.PlateNumber
        res.ComputationIndicator = selectedItem.ComputationIndicator
        res.HeightIndicator = selectedItem.HeightIndicator
        res.HorizontalHelmert = selectedItem.HorizontalHelmert
        res.VerticalHelmert = selectedItem.VerticalHelmert
        res.ProjectionType = selectedItem.ProjectionType

        let options = {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(res)
        };

        let fetchRes = fetch(Settings.url + 'api/coords_system/select_cs', options);
        fetchRes.then(res =>
            res.json()).then(d => {
            if(d.status == 'ok') {
                return;
            } else {
                alert(d.message)
            }
        })

    } else {
        console.error('Элемент с uuid', uuid, 'не найден');
    }
}

var myModal = new bootstrap.Modal(document.getElementById('myModal'), [])
function dialogPoints() {
    var options = [];
    myModal.show();
    getPoints();
}

function getPoints() {
    clearTable();
    let fetchRes = fetch(Settings.url + 'api/point/list');
    fetchRes.then(res =>
        res.json()).then(d => {
        insertTableData(d.points)
    })
}

function clearTable() {
    console.log('clearTable()');
    let tbl = document.querySelector('#tblList');
    if(!tbl) {
        return;
    }

    tbl.removeChild(tbl.getElementsByTagName("tbody")[0]);
    tbl.appendChild(document.createElement("tbody"));
}

function insertTableData(data) {
    let tbl = document.getElementById('tblList').getElementsByTagName('tbody')[0];
    let tmpDateRow = '';

    let ic = 0;
    for(var i=0; i < data.length; i++) {
        let row = tbl.insertRow(ic);
        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);

        cell1.innerHTML = data[i].name;
        cell2.innerHTML = '<small>' + data[i].data.lat + '</small><br>'
            + '<small>' + data[i].data.lon  + '</small><br>'
            + '<small>' + data[i].data.height  + '</small>'


        cell3.innerHTML = '<button class="btn btn-sm btn-dark py-0" id="b'+ i
            +'" onclick="selectPoint(\''+ data[i].data.lat +'\', \''
            + data[i].data.lon +'\', \''
            + data[i].data.height +'\',)" title="Выбрать точку">&check;</button>'
        ic++;
    }
}


function selectPoint(x,y,z) {
    let cl = document.getElementById('elCoordsLat');
    let cn = document.getElementById('elCoordsLon');
    let ch = document.getElementById('elCoordsHeight');
    cl.value = x;
    cn.value = y;
    ch.value = z;
    myModal.hide();
}


function sendAuto() {

}


function applyPorts() {
    console.log('applyPorts()')
}

function applyForms() {
    console.log('applyForms()', Settings)
    document.getElementById('elCoordsLat').value    = Settings.params.coords_input[0];
    document.getElementById('elCoordsLon').value    = Settings.params.coords_input[1];
    document.getElementById('elCoordsHeight').value = Settings.params.coords_input[2];
    document.getElementById('elCoordsStatus').checked =
        Settings.params.coords_status == 'fix' ? false : true;

    document.getElementById('elAutoConnect').checked = Settings.load.auto_connect
    document.getElementById('elCoordsPPS').checked   = Settings.load.pps_output;
    document.getElementById('elCoordsEvent').checked = Settings.load.event_input;
    document.getElementById('elNtripRTCM').checked   = Settings.load.to_com;
    document.getElementById('elNtripLocal').checked  = Settings.load.ntrip_local;
    document.getElementById('elNtripRemote').checked = Settings.load.ntrip_remote;
    document.getElementById('elNtripTcp').checked = Settings.load.tcp_local;
    document.getElementById('elNtripTcpPort').value = Settings.load.tcp_local_port;
    document.getElementById('elTCPRemote').checked = Settings.load.tcp_remote;

    document.getElementById('elCasterHost').value = Settings.load.caster_host;
    document.getElementById('elCasterPort').value = Settings.load.caster_port;
    document.getElementById('elCasterMountPoint').value = Settings.load.caster_mount_point;
    document.getElementById('elCasterPassword').value = Settings.load.caster_password;

    document.getElementById('elTCPHost').value = Settings.load.tcp_remote_host;
    document.getElementById('elTCPPort').value = Settings.load.tcp_remote_port;

    document.getElementById('elApplyCS').checked = Settings.load.statusCS_application;

    document.getElementById('elReset').checked = Settings.load.reset_status;

    document.getElementById('elCOM1').checked = Settings.load.hardware_checkbox1;
    document.getElementById('elCOM2').checked = Settings.load.hardware_checkbox2;
    document.getElementById('elCOM3').checked = Settings.load.hardware_checkbox3;
    document.getElementById('elMaxFiles').value = Settings.load.max_num_files
    document.getElementById('firmwareVersion').innerText = Settings.params.firmware_version;

    document.getElementById('elSatGPS').checked = Settings.load.hardware_sats_system.indexOf('GPS') != -1 ? true : false;
    document.getElementById('elSatGLO').checked = Settings.load.hardware_sats_system.indexOf('GLO') != -1 ? true : false;
    document.getElementById('elSatGAL').checked = Settings.load.hardware_sats_system.indexOf('GAL') != -1 ? true : false;
    document.getElementById('elSatBDS').checked = Settings.load.hardware_sats_system.indexOf('BDS') != -1 ? true : false;

    document.getElementById('elRTCMinterval').selectedIndex = Settings.load.hardware_interval_rtcm;
    document.getElementById('elEntryInterval').value = Settings.load.entry_interval_rtcm;

    document.getElementById('elStatusFTP').checked   = Settings.load.status_work_ftp;
    document.getElementById('elFTPHost').value = Settings.load.ftp_host;
    document.getElementById('elFTPPort').value = Settings.load.ftp_port;
    document.getElementById('elFTPLogin').value = Settings.load.ftp_login;

    document.getElementById('elBuzzer').checked = Settings.load.work_buzzer;

    let selectPort = document.querySelector('#elPorts');
    while (selectPort.options.length > 1) {
        selectPort.remove(1);
    }

    let selectInterval = document.querySelector('#elInterval');
    while (selectInterval.options.length > 1) {
        selectInterval.remove(1);
    }

    if(Settings.load.rtcm_type == '3.0') {
        document.getElementById('elRtcmType1').checked = true;
        document.getElementById('elRtcmType2').checked = false;
    } else {
        document.getElementById('elRtcmType1').checked = false;
        document.getElementById('elRtcmType2').checked = true;
    }

    let selectBox2 = document.querySelector('#elNtripRTCMPort');
    while (selectBox2.options.length > 1) {
        selectBox2.remove(1);
    }

    let selectBoxCOM1sp = document.querySelector('#elCOM1speed');
    while (selectBoxCOM1sp.options.length > 1) {
        selectBoxCOM1sp.remove(1);
    }
    let selectBoxCOM1fr = document.querySelector('#elCOM1format');
    while (selectBoxCOM1fr.options.length > 1) {
        selectBoxCOM1fr.remove(1);
    }

    let selectBoxCOM2sp = document.querySelector('#elCOM2speed');
    while (selectBoxCOM2sp.options.length > 1) {
        selectBoxCOM2sp.remove(1);
    }
    let selectBoxCOM2fr = document.querySelector('#elCOM2format');
    while (selectBoxCOM2fr.options.length > 1) {
        selectBoxCOM2fr.remove(1);
    }

    let selectBoxCOM3sp = document.querySelector('#elCOM3speed');
    while (selectBoxCOM3sp.options.length > 1) {
        selectBoxCOM3sp.remove(1);
    }
    let selectBoxCOM3fr = document.querySelector('#elCOM3format');
    while (selectBoxCOM3fr.options.length > 1) {
        selectBoxCOM3fr.remove(1);
    }

    ['rtcm3.0', 'rtcm3.2', '3.2.radio', 'none'].forEach((element) => {
        if(Settings.load.hardware_comport1==element) {
            selectBoxCOM1fr.add(
                new Option(element,element, true, true),
                undefined
            );
        } else {
            selectBoxCOM1fr.add(new Option(element,element), undefined);
        }
        if(Settings.load.hardware_comport2==element) {
            selectBoxCOM2fr.add(
                new Option(element,element, true, true),
                undefined
            );
        } else {
            selectBoxCOM2fr.add(new Option(element,element), undefined);
        }
        if(Settings.load.hardware_comport3==element) {
            selectBoxCOM3fr.add(
                new Option(element,element, true, true),
                undefined
            );
        } else {
            selectBoxCOM3fr.add(new Option(element,element), undefined);
        }
    });

    ['115200', '38400'].forEach((element) => {
        if(Settings.load.hardware_speed1==element) {
            selectBoxCOM1sp.add(
                new Option(element,element, true, true),
                undefined
            );
        } else {
            selectBoxCOM1sp.add(new Option(element,element), undefined);
        }
        if(Settings.load.hardware_speed2==element) {
            selectBoxCOM2sp.add(
                new Option(element,element, true, true),
                undefined
            );
        } else {
            selectBoxCOM2sp.add(new Option(element,element), undefined);
        }
        if(Settings.load.hardware_speed3==element) {
            selectBoxCOM3sp.add(
                new Option(element,element, true, true),
                undefined
            );
        } else {
            selectBoxCOM3sp.add(new Option(element,element), undefined);
        }
    });

    [1,2,3,4,5].forEach((element) => {
    if(Settings.load.periodCS_sending==element) {
        selectInterval.add(
            new Option(element,element, true, true),
            undefined
        );
    } else {
        selectInterval.add(new Option(element,element), undefined);
        }
    });

    console.log("List Ports :" , Settings.params.ports)
    Settings.params.ports.forEach((element) => {
    if(Settings.load.port_name == element) {
        selectPort.add(
            new Option(element,element, true, true),
            undefined
        );
    } else {
        selectPort.add(new Option(element,element), undefined)
        }
    });

}

function applyParams() {
    console.log('applyParams()')
    document.getElementById('elSerial').value =
        clearValue(Settings.params.serial_number);
    document.getElementById('elSats').value   =
        clearValue(Settings.params.sats_data);
    document.getElementById('elFix').value    =
        clearValue(Settings.params.fix_status);

    // document.getElementById('paramNtripStatus').innerHTML


    document.getElementById('paramNtripStatus').innerHTML = Settings.params.ntrip_status;
    document.getElementById('paramFileStatus').textContent = Settings.params.file_status;
    document.getElementById('paramLat').textContent = Settings.params.coords[0];
    document.getElementById('paramLon').textContent = Settings.params.coords[1];
    document.getElementById('paramHeight').textContent = Settings.params.coords[2];
}

function clearValue(val) {
    if(val == 'Undefs') {
        return '';
    }
    if(val == 'Undef') {
        return '';
    }
    if(val == 'undef') {
        return '';
    }
    return val;
}


function writeConsole() {
    let elConsole = document.getElementById('elConsole');
    let textConsole = Settings.params.console.join("\n");
    if(textConsole) {
        //elConsole.textContent = elConsole.textContent + textConsole + "\n";
        elConsole.textContent = textConsole + "\n";
        elConsole.scrollTop = elConsole.scrollHeight;
        Settings.params.console = []
    }
}


function doSetConsoleFull() {
    Settings.full_console = true;
}


function buttonLock(val) {
    let loadBtn = document.getElementById("btnLoad");
    let saveBtn = document.getElementById("btnSave");

    loadBtn.disabled = !val;
    saveBtn.disabled = !val;
}


function getFormData() {
    let res = {};
    res.port_name = document.getElementById('elPorts').selectedOptions[0].value;

    //res.ntrip.status = document.querySelector('input[name="elNtripStatus"]:checked').value;
    res.tcp_local = document.getElementById('elNtripTcp').checked ? true : false;
    res.tcp_local_port = document.getElementById('elNtripTcpPort').value;

    res.caster_host = document.getElementById('elCasterHost').value;
    res.caster_port = document.getElementById('elCasterPort').value;
    res.caster_mount_point = document.getElementById('elCasterMountPoint').value;
    res.caster_password = document.getElementById('elCasterPassword').value;

    res.tcp_remote_host = document.getElementById('elTCPHost').value;
    res.tcp_remote_port = document.getElementById('elTCPPort').value;


    res.auto_connect = document.getElementById('elAutoConnect').checked ? true : false;
    res.to_com       = document.getElementById('elNtripRTCM').checked   ? true : false;
    res.to_com_port  = document.getElementById('elNtripRTCMPort').selectedOptions[0].value;

    res.ntrip_local  = document.getElementById('elNtripLocal').checked  ? true : false;
    res.ntrip_remote = document.getElementById('elNtripRemote').checked ? true : false;
    res.tcp_remote = document.getElementById('elTCPRemote').checked ? true : false;

    res.statusCS_application = document.getElementById('elApplyCS').checked ? true : false;
    res.periodCS_sending = parseInt(document.getElementById('elInterval').selectedOptions[0].value);

    res.reset_status = document.getElementById('elReset').checked ? true : false;

    res.pps_output  = document.getElementById('elCoordsPPS').checked   ? true : false;
    res.event_input = document.getElementById('elCoordsEvent').checked ? true : false;

    res.status_work_ftp  = document.getElementById('elStatusFTP').checked   ? true : false;
    res.ftp_host = document.getElementById('elFTPHost').value;
    res.ftp_port = parseInt(document.getElementById('elFTPPort').value);
    res.ftp_login = document.getElementById('elFTPLogin').value;
    res.ftp_password = document.getElementById('elFTPPassword').value;

    res.entry_interval_rtcm = parseInt(document.getElementById('elEntryInterval').value);

    res.work_buzzer = document.getElementById('elBuzzer').checked ? true : false;


    res.hardware_sats_system = [];

    if(document.getElementById('elSatGPS').checked) {
        res.hardware_sats_system.push('GPS');
    }
    if(document.getElementById('elSatGLO').checked) {
        res.hardware_sats_system.push('GLO');
    }
    if(document.getElementById('elSatGAL').checked) {
        res.hardware_sats_system.push('GAL');
    }
    if(document.getElementById('elSatBDS').checked) {
        res.hardware_sats_system.push('BDS');
    }

    res.hardware_interval_rtcm = parseInt(document.getElementById('elRTCMinterval').selectedOptions[0].value);

    res.rtcm_type = document.getElementById('elRtcmType1').checked ? '3.0' : '3.2';

    res.hardware_speed1 = parseInt(document.getElementById('elCOM1speed').selectedOptions[0].value);
    res.hardware_speed2 = parseInt(document.getElementById('elCOM2speed').selectedOptions[0].value);
    res.hardware_speed3 = parseInt(document.getElementById('elCOM3speed').selectedOptions[0].value);

    res.hardware_comport1 = document.getElementById('elCOM1format').selectedOptions[0].value;
    res.hardware_comport2 = document.getElementById('elCOM2format').selectedOptions[0].value;
    res.hardware_comport3 = document.getElementById('elCOM3format').selectedOptions[0].value;

    res.hardware_checkbox1 = document.getElementById('elCOM1').checked ? true : false;
    res.hardware_checkbox2 = document.getElementById('elCOM2').checked ? true : false;
    res.hardware_checkbox3 = document.getElementById('elCOM3').checked ? true : false;

    res.max_num_files = parseInt(document.getElementById('elMaxFiles').value);

    res.coords_input = [];
    res.coords_status = document.getElementById('elCoordsStatus').checked ? 'auto' : 'fix';
    res.coords_input.push(parseFloat(document.getElementById('elCoordsLat').value));
    res.coords_input.push(parseFloat(document.getElementById('elCoordsLon').value));
    res.coords_input.push(parseFloat(document.getElementById('elCoordsHeight').value));

    return res;
}
