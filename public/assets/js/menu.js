/**
 * Render main menu
 */


let menu = [
    { link: '/login.html', text: 'Login' },
    { link: '/satellites.html', text: 'Satellites' },
    { link: '/files.html', text: 'Files' },
    { link: '/cnb.html', text: 'CNB converter' },
    { link: '/gpoints.html', text: 'GPoints' },
    { link: '/wifi.html', text: 'Wifi' },
    { link: '/update.html', text: 'Update' },
    { link: '/coords_system.html', text: 'Coords System' }
];

function renderMenu() {

    let container = document.getElementById('navbarNavDropdown');
    let ul = document.createElement("ul");//
    ul.classList.add('navbar-nav');

    menu.forEach( (k,i)=> {
        var li = document.createElement('li');
        li.classList.add('nav-item');
        var a  = document.createElement('a');
        a.classList.add('nav-link');
        a.href = k.link;
        a.innerText = k.text;
        li.append(a);
        ul.append(li);
    })

    container.append(ul);
}
