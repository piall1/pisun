document.addEventListener("DOMContentLoaded", _sat_init);

Settings.satellites = {}

function _sat_init() {
    console.log('_sat_init()');
    if(document.location.hostname != 'pisunrpi.local' ) {
        Settings.url = document.location.origin + '/';
    }
    renderMenu();
    getSettingsServer();
    resizeSat();
    getDataPlot();

    setInterval(getDataPlot, 1500);
    //setTimeout(writePlot, 1000);
    //setInterval(fakeGetData, 1500)
    //writePlot();

    // writeBallTest();
}

let satBlock = document.getElementById('satellitePlot');

function getDataPlot(){
    let options = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
    let fetchRes = fetch(Settings.url + 'api/plotdata/get', options);
    //let fetchRes = fetch('sat.json', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        transformPlotData(d);
        writePlot();
        //writeBallTest();
        //console.log(d);
    })
}

function writePlot(){
    console.log('writePlot');
    let bc = satBlock.querySelector('.sat-plot-wrap');
    let satBalls = satBlock.querySelectorAll('.sat-plot-wrap .sat-ball');
    if(satBalls.length) {
        satBalls.forEach((element) => {
            element.dataset.process = true;
        });
    }

    let ball;
    Settings.satellites.items.forEach((element) => {
        ball = satBlock.querySelector('#sat_' + element.name);
        if(!ball) {
            ///console.log('not ball');
            let c = document.createElement("div");
            c.classList.add('sat-ball', element.class);
            c.setAttribute('id', 'sat_' + element.name);
            c.innerHTML = '<span>' + element.name + '</span>';
            bc.insertAdjacentElement('beforeend', c);
            ball = satBlock.querySelector('#sat_' + element.name);
        }

        ball.style.left = "calc("+ calculateProcent(element.x, 'x') +"% - 10px)";
        ball.style.top  = "calc("+ calculateProcent(element.y, 'y') +"% - 10px)";
        ball.style.opacity  = element.opacity;
        ball.setAttribute('title',
            element.name + "\n"
                + 'SNR: ' + element.snr + "\n"
                + 'ELEV: ' + element.elevation + "\n"
                + 'AZ: ' + element.azimut
            );
        ball.dataset.process = false;
    });

    satBalls = satBlock.querySelectorAll('.sat-plot-wrap .sat-ball[data-process="true"]');
    if(satBalls.length) {
        satBalls.forEach((element) => {
            element.style.opacity = '0%';
        });
    }
}

function transformPlotData(data){
    Settings.satellites.items = [] //обнуляем данные спутников
    Settings.satellites.x_min = data.x_min;
    Settings.satellites.x_max = data.x_max;
    Settings.satellites.y_min = data.y_min;
    Settings.satellites.y_max = data.y_max;

    let item = {}
    data.satelites.forEach((element) => {
        item = {}
        item.name = element.name;
        item.snr  = element.snr;
        item.opacity = satelliteSnrOpacity(element.snr);
        item.class   = sateliteTypeClass(element.name_color);
        item.x = element.x_skyplot;
        item.y = element.y_skyplot;
        item.azimut = element.azimut;
        item.elevation = element.elevation;

        Settings.satellites.items.push(item);
    });

    console.log(Settings);
}

function sateliteTypeClass(liter) {
    if(liter == 'G') {
        return 'gps';
    }
    if(liter == 'C') {
        return 'bd';
    }
    if(liter == 'I') {
        return 'irn';
    }
    if(liter == 'Q') {
        return 'qzss';
    }
    if(liter == 'S') {
        return 'sbs';
    }
    if(liter == 'R') {
        return 'glo';
    }
    if(liter == 'E') {
        return 'gal';
    }
    return '';
}

function satelliteSnrOpacity(snr) {
    if(snr > 0 && snr < 31) {
        return '30%';
    } else if(snr > 30 && snr < 44) {
        return '55%';
    } else {
        return '100%';
    }
}


function resizeSat() {
    let width = satBlock.clientWidth;
    satBlock.style.height = width + 'px';
    console.log(satBlock.clientHeight, width);
}

function fakeGetData() {
    Settings.satellites.items.forEach((element) => {
        let rnd = getRandomInt(10);
        if(rnd < 2) {
            // change X
            element.x = element.x + Math.random();
            if(element.x > Settings.satellites.x_max) {
                element.x = element.x - Math.random()*2;
            }
        } else if(rnd > 2 && rnd < 4){
            // change Y
            element.y = element.y + Math.random();
            if(element.y > Settings.satellites.y_max) {
                element.y = element.y - Math.random()*2;
            }
        }

        if(getRandomInt(100) < 2) {
            // hidden balls
            element.opacity = '0%';
        } else {
            element.opacity = satelliteSnrOpacity(element.snr);
        }
    });

    writePlot(); //обновляем данные
}

function writeBallTest() {
    let b = [
        [1, 13.891854213354426, 78.78462024097664],
        [2, 5.209445330007909, -29.544232590366242],
        [3, -8.660254037844386, -5.000000000000004],
        [4, -42.42640687119286, 42.42640687119284],
        [5, 0, 30],
        [6, 0, 60],
        [7, 60, 0],
    ];

    let bc = satBlock.querySelector('.sat-plot-wrap');

    b.forEach((element) =>  {
        let c = document.createElement("div");
        c.classList.add('sat-ball', 'glo');
        c.innerHTML = '<span>C' + element[0] + '</span>';
        c.style.left = "calc("+ calculateProcent(element[1], 'x') +"% - 10px)";
        c.style.top  = "calc("+ calculateProcent(element[2], 'y') +"% - 10px)";
        c.style.backgroundColor = "red";

        console.log('C' + element[0] + ' x: '+element[1] + '  -  ' + calculateProcent(element[1], 'x'));

        bc.insertAdjacentElement('beforeend', c);

    })


}

function calculateProcent(c, direction = 'x') {

    let absX = Math.abs(Settings.satellites.x_min) + Settings.satellites.x_max;
    let partX = absX / 2;

    let absY = Math.abs(Settings.satellites.y_min) + Settings.satellites.y_max;
    let partY = absY / 2;

    if(direction == 'x') {
        return (c + partX) / absX * 100;
    }
    if(direction == 'y') {
        return (absY - (c + partY)) / absY * 100;
    }
}

function getRandomInt(max) {
    return Math.floor(Math.random() * max);
}

