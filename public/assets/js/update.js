document.addEventListener("DOMContentLoaded", _update_init);

function _update_init() {
    console.log('_update_init()');
    if(document.location.hostname != 'pisunrpi.local' ) {
        Settings.url = document.location.origin + '/';
    }
    renderMenu();
    getSettingsServer();
}



function doUpdate() {
    let iB = document.getElementById('infoBlockSuccess');
    let iA = document.getElementById('infoBlockError');

    let uBlock = document.getElementById('updateBlock');
    uBlock.innerHTML = '<div class="spinner-border text-primary" role="status">\n' +
        '  <span class="visually-hidden">Loading...</span>\n' +
        '</div>'
    ;

    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ action: 'update' })
    };
    let fetchRes = fetch(Settings.url + 'api/getupdate', options);
    fetchRes.then(res =>
        res.json()).then(d => {
            if(d.status == 'ok') {
                uBlock.innerHTML = '<div class="text-bg-success p-3">'
                    + d.message + "\n"
                    + '</div>';

                    iA.innerText = '';
                    iA.classList.add('d-none');
                    iB.innerText = d.message;
                    iB.classList.remove('d-none');

            } else {
                uBlock.innerHTML = '<div class="text-bg-danger p-3">'
                    + d.message + "\n"
                    + '</div>';

                iB.innerText = '';
                iB.classList.add('d-none');
                iA.innerText = d.message;
                iA.classList.remove('d-none');
            }
    })

}





