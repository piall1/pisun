document.addEventListener("DOMContentLoaded", _wifi_init);

function _wifi_init() {
    console.log('_wifi_init()');
    if(document.location.hostname != 'pisunrpi.local' ) {
        Settings.url = document.location.origin + '/';
    }
    renderMenu();
    getSettingsServer();
    getWifi();
    getSavedNet();
    getCurrentNetwork();
}


async function getWifi() {
    clearTable();
    let fetchRes = await fetch(Settings.url + 'api/wifi/get_available_net');
    let networks = await fetchRes.json();

    if(networks.status == 'error') {
        return;
    } else {
        insertTableData(networks);
        return networks;
    }
}

async function getSavedNet() {
    clearTableNet();
    let fetchRes = await fetch(Settings.url + 'api/wifi/get_saved_net');
    let saved_net = await fetchRes.json();

    if(saved_net.status == 'error') {
        return;
    } else {
        insertTableNet(saved_net);
        return saved_net;
    }
}

async function getCurrentNetwork() {
    let elNet = document.getElementById('elNameNet');
    let fetchRes = await fetch(Settings.url + 'api/wifi/get_previous_network');
    let data = await fetchRes.json();
    elNet.textContent = data.network + ' ' + `(${data.ip})`;

}

function onClickRefresh() {
    getWifi();
    getSavedNet();
    getCurrentNetwork();
}


function ConnectSavedNet(index, ssid) {
    let iB = document.getElementById('infoBlockSuccess');
    let iA = document.getElementById('infoBlockError');
    let res = { "ssid": ssid };
    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    showToast(`Подключение к сети ${ssid}`, "ok");

    iA.innerText = '';
    iA.classList.add('d-none');
    iB.innerText = `Подключение к сети ${ssid}`;
    iB.classList.remove('d-none');

    let fetchRes = fetch(Settings.url + 'api/wifi/connect_saved_network', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            iA.innerText = '';
            iA.classList.add('d-none');
            iB.innerText = d.message;
            iB.classList.remove('d-none');
            return;
        } else {
            iB.innerText = '';
            iB.classList.add('d-none');
            iA.innerText = d.message;
            iA.classList.remove('d-none');
        }
    })
}


function addNewNet(ssid, password) {
    let iB = document.getElementById('infoBlockSuccess');
    let iA = document.getElementById('infoBlockError');
    let res = { "ssid": ssid, "password": password};

    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    let fetchRes = fetch(Settings.url + 'api/wifi/new_network', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            getCurrentNetwork();
            iA.innerText = '';
            iA.classList.add('d-none');
            iB.innerText = d.message;
            iB.classList.remove('d-none');
            return;
        } else {
            iB.innerText = '';
            iB.classList.add('d-none');
            iA.innerText = d.message;
            iA.classList.remove('d-none');
        }
    })

}

function deleteWifi(ssid) {
    let iB = document.getElementById('infoBlockSuccess');
    let iA = document.getElementById('infoBlockError');
    let res = { "uuid": ssid };

    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    let fetchRes = fetch(Settings.url + 'api/wifi/delete_network', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            iA.innerText = '';
            iA.classList.add('d-none');
            iB.innerText = d.message;
            iB.classList.remove('d-none');
            return;
        } else {
            iB.innerText = '';
            iB.classList.add('d-none');
            iA.innerText = d.message;
            iA.classList.remove('d-none');
        }
    })
}

function openConnect(index, ssid) {
    let iB = document.getElementById('infoBlockSuccess');
    let iA = document.getElementById('infoBlockError');
    let res = { "ssid": ssid };

    let options = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(res)
    };

    let fetchRes = fetch(Settings.url + 'api/wifi/check_saved_network', options);
    fetchRes.then(res =>
        res.json()).then(d => {
        if(d.status == 'ok') {
            if (d.message == true) {
                ConnectSavedNet(index, ssid);
            } else {
                openModal(index, ssid);
            }
        } else {
            iB.innerText = '';
            iB.classList.add('d-none');
            iA.innerText = d.message;
            iA.classList.remove('d-none');
        }
    })

}

function openModal(index, ssid) {
    let myModalNet = new bootstrap.Modal(document.getElementById('myModalNet'));
    myModalNet.show();
    document.getElementById('elSsid').value = ssid;
    document.getElementById('doConnectWiFi').onclick = function() {
        ltSsid = document.getElementById('elSsid').value
        ltPwd = document.getElementById('elPinSsid').value

        addNewNet(ltSsid, ltPwd);
        myModalNet.hide();

        showToast(`Подключение к сети ${ssid}`, "ok");
        let iB = document.getElementById('infoBlockSuccess');
        let iA = document.getElementById('infoBlockError');

        iA.innerText = '';
        iA.classList.add('d-none');
        iB.innerText = `Подключение к сети ${ssid}`;
        iB.classList.remove('d-none');

    };
}

function clearTable() {
    let tbl = document.querySelector('#tblList');
    if(!tbl) {
        return;
    }

    tbl.removeChild(tbl.getElementsByTagName("tbody")[0]);
    tbl.appendChild(document.createElement("tbody"));
}

function clearTableNet() {
    let tbl = document.querySelector('#tblListSaved');
    if(!tbl) {
        return;
    }

    tbl.removeChild(tbl.getElementsByTagName("tbody")[0]);
    tbl.appendChild(document.createElement("tbody"));
}

function insertTableData(data) {
    let tbl = document.getElementById('tblList').getElementsByTagName('tbody')[0];
    let tmpDateRow = '';

    let ic = 0;
    for(var i=0; i < data.length; i++) {
        let row = tbl.insertRow(ic);

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);

        cell1.innerHTML = data[i].ssid;
        cell2.innerHTML = data[i].address;
//        cell2.innerHTML = data[i].signal;

        cell3.innerHTML = '<button class="btn btn-sm btn-dark py-0" data-bs-toggle="modal" data-bs-target="#staticBackdrop" id="b'+ i + '" onclick="openConnect('+ i +',\''
            + data[i].ssid +'\')" title="Подключиться">&crarr;</button>'

        ic++;
    }
}

function insertTableNet(data) {
    let tbl = document.getElementById('tblListSaved').getElementsByTagName('tbody')[0];
    let tmpDateRow = '';

    let ic = 0;
    for (var i = 0; i < data.length; i++) {
        let row = tbl.insertRow(ic);

        var cell1 = row.insertCell(0);
        var cell2 = row.insertCell(1);
        var cell3 = row.insertCell(2);
        var cell4 = row.insertCell(3);

//        cell1.innerHTML = '<i class="fa-solid fa-wifi fa-lg"></i>';

        cell1.innerHTML = data[i].ssid;
        cell2.innerHTML = data[i].ip;

        cell3.innerHTML = '<button class="btn btn-sm btn-dark py-0" data-bs-toggle="modal" data-bs-target="#staticBackdrop" id="b'+ i + '" onclick="ConnectSavedNet('+ i +',\''
            + data[i].ssid +'\')" title="Подключиться">&crarr;</button>'

        if (data[i].ssid !== 'Hotspot') {
            cell4.innerHTML = '<button class="btn btn-sm btn-danger py-0" id="b' + i + '" onclick="deleteWifi(\'' + data[i].ssid + '\')" title="Удалить сеть">х</button>';
        } else {
            cell4.innerHTML = '';
        }

        ic++;
    }
}


