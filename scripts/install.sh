#!/bin/bash
SYSTEMD_PATH="/etc/systemd/system/"
SCRIPT_PATH="$(realpath "$0")"
SCRIPT_DIR="$(dirname "$(dirname "$SCRIPT_PATH")")"
UNIT_SYSTEMD="$SCRIPT_DIR/resources/pisun.service"
echo $UNIT_SYSTEMD
echo $SYSTEMD_PATH
echo $SCRIPT_PATH
echo $SCRIPT_DIR
cp $UNIT_SYSTEMD $SYSTEMD_PATH
sed -i "s#/home/pi/pisun#$SCRIPT_DIR#g" "$SYSTEMD_PATH/pisun.service"
systemctl daemon-reload
systemctl enable pisun.service
systemctl start pisun.service