import ftplib
import os
from src.structures import JsonManager


class FTPClient:

    json_cont = JsonManager.JsonManager()
    is_running = False
    is_connect = False

    def __init__(self):
        self.json_file = "resources/files.json"

    def set_connection_details(self, host, port, login, pwd):
        self.host = host
        self.port = port
        self.login = login
        self.pwd = pwd

    def connect_to_ftp_server(self):
        try:
            if self.host == "":
                raise Exception("Имя хоста отсутсвует")
            if self.port == 0 or self.port is None:
                raise Exception("Порт отсутсвует")
            if self.login == "":
                raise Exception("Логин отсутсвует")
            if self.pwd == "":
                raise Exception("Пароль отсутсвует")

            self.ftp = ftplib.FTP()
            self.ftp.connect(self.host, self.port)
            self.ftp.login(self.login, self.pwd)
            self.is_connect = True
            print("Успешное подлючение к FTP серверу")
        except Exception as e:
            self.is_connect = False
            print(f'Произошла ошибка при подключении к FTP серверу: {e}')

    def send_file_to_server(self, files_ready):
        try:
            for file_name in files_ready:
                if file_name == "":
                    continue
                with open(("files/" + file_name), 'rb') as file:
                    self.ftp.storbinary(f'STOR {file_name}', file)
                print(f'Файл {file_name} загружен успешно.')

                self.json_cont.update_val(file_name, 3, self.json_file)

            self.is_running = True
        except Exception as e:
            self.is_running = False
            print(f'Произошла ошибка при отправке файла на сервер: {e}')
            self.disconnect()

    def disconnect(self):
        if self.ftp is None:
            return
        else:
            if self.is_connect:
                self.ftp.quit()
                print("Соединение с FTP сервером остановлено")

