import queue
import threading
import socket
import time
from src.utils.Console import print


class TcpClient:
    is_connected = False
    server_port = 5201
    server_host = "localhost"
    data_queue = queue.Queue()
    thread = None
    is_running = False

    def start_tcp(self):
        self.is_running = True
        self.thread = threading.Thread(target=self.thread_tcpserver, name="TCP Client")
        self.thread.start()
        return self.is_running

    def stop_tcp(self):
        if not self.is_running:
            return
        self.is_running = False
        self.thread.join()
        self.disconnect()

    def set_tcp_port(self, tcp_port):
        self.server_port = int(tcp_port)

    def set_tcp_host(self, tcp_host):
        self.server_host = str(tcp_host)

    def connect_tcp(self):
        if self.is_connected:
            print("Ошибка! Соединение уже существует.")
            return self.is_connected
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            self.client_socket.connect((self.server_host, self.server_port))
            self.is_connected = True
        except Exception:
            print(f"Ошибка: соединение с {self.server_host}:{self.server_port} отклонено")
        return self.is_connected

    def reconnect(self):
        self.disconnect()
        self.connect_tcp()

    def disconnect(self):
        if self.is_connected:
            self.is_connected = False
            self.client_socket.close()

    def thread_tcpserver(self):
        while self.is_running:
            data_buffer = b''
            try:
                data = self.data_queue.get(timeout=1)
            except queue.Empty:
                data = b''
            data_buffer += data

            try:
                if self.client_socket.fileno() == -1:
                    print("Сокет закрыт")
                    break
                self.client_socket.send(data_buffer)
            except BrokenPipeError as e:
                print(f"BrokenPipeError. {e}")
                self.client_socket.close()
                self.reconnect()
                time.sleep(0.1)
            except OSError as e:
                print(f"OSError {e}")
                self.reconnect()
                time.sleep(0.1)

    def append_data(self, bytearray_data):
        if self.is_running:
            self.data_queue.put(bytearray_data)
