import math


class CoordConverter:
    R2D = 180.0 / math.pi
    D2R = math.pi / 180.0
    range = [0.0, 360.0, -90.0, 90.0]

    def __init__(self):
        pass

    def convert_wgs_to_ecef(self, lat, lon, h):
        pos = [float(lat), float(lon), float(h)]
        ecef = [0.0, 0.0, 0.0]
        if pos[0] == 0.0 and pos[1] == 0.0 and pos[2] == 0.0:
            return ecef

        pos[0] = pos[0] * self.D2R
        pos[1] = pos[1] * self.D2R

        self.pos2ecef(pos, ecef)
        print(ecef)
        return ecef

    def convert_ecef_to_wgs(self, x, y, z):
        x = float(x)
        y = float(y)
        z = float(z)

        ecef = [x, y, z]
        pos = [0.0, 0.0, 0.0]
        if x == 0.0 and y == 0.0 and z == 0.0:
            return pos
        self.ecef2pos(ecef, pos)
        pos[0] = pos[0] * self.R2D
        pos[1] = pos[1] * self.R2D
        print(pos)
        return pos

    def ecef2pos(self, r, pos):
        FE_WGS84 = 1.0 / 298.257223563
        RE_WGS84 = 6378137.0
        e2 = FE_WGS84 * (2.0 - FE_WGS84)
        r2 = r[0] * r[0] + r[1] * r[1]
        z = r[2]
        zk = 0.0
        v = RE_WGS84
        sinp = 0.0

        while abs(z - zk) >= 1E-4:
            zk = z
            sinp = z / math.sqrt(r2 + z * z)
            v = RE_WGS84 / math.sqrt(1.0 - e2 * sinp * sinp)
            z = r[2] + v * e2 * sinp

        pos[0] = math.atan(z / math.sqrt(r2)) if r2 > 1E-12 else (math.pi / 2.0 if r[2] > 0.0 else -math.pi / 2.0)
        pos[1] = math.atan2(r[1], r[0]) if r2 > 1E-12 else 0.0
        pos[2] = math.sqrt(r2 + z * z) - v

    def pos2ecef(self, pos, r):
        FE_WGS84 = 1.0 / 298.257223563
        RE_WGS84 = 6378137.0
        sinp = math.sin(pos[0])
        cosp = math.cos(pos[0])
        sinl = math.sin(pos[1])
        cosl = math.cos(pos[1])

        e2 = FE_WGS84 * (2.0 - FE_WGS84)
        v = RE_WGS84 / math.sqrt(1.0 - e2 * sinp * sinp)

        r[0] = (v + pos[2]) * cosp * cosl
        r[1] = (v + pos[2]) * cosp * sinl
        r[2] = (v * (1.0 - e2) + pos[2]) * sinp

    def interpb(self, y, a, b):
        return y[0] * (1.0 - a) * (1.0 - b) + y[1] * a * (1.0 - b) + y[2] * (1.0 - a) * b + y[3] * a * b