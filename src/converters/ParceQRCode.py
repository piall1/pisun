import base64
import gzip
import io
import json
from src.utils import ImportLib


class ParceQRCode:
    _instance = None
    imp = ImportLib.ImportLib()

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(ParceQRCode, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def read_qr_code(self, photo_qr):
        try:
            self.imp.check_qr_lib()
            self.imp.check_image_lib()

            from pyzbar.pyzbar import decode
            from PIL import Image

            image = Image.open(photo_qr)
            if image is None:
                print("Ошибка: изображение не найдено.")
                return None
            decoded_objects = decode(image)
            for obj in decoded_objects:
                self.decode_qr_data(obj.data.decode('utf-8'))
        except Exception as e:
            print(f"Ошибка при чтении QR кода: {e}")

    def decode_qr_data(self, data):
        # Убираем префикс "CS://"
        if data.startswith("CS://"):
            data = data[5:]

        # Декодируем из Base64
        compressed_data = base64.b64decode(data)

        # Распаковываем данные
        with gzip.GzipFile(fileobj=io.BytesIO(compressed_data), mode='rb') as f:
            decompressed_data = f.read().decode('utf-8')

        self.data_json = json.loads(decompressed_data)

    def get_params_from_qr(self):
        if self.data_json is None:
            return
        params_cs = {}
        if "data" in self.data_json:
            json_string = self.data_json["data"]
            dict_data = json.loads(json_string)

            params_cs = {
                "Datum": dict_data["name"],

                "SourceName": dict_data["datum"]["sourceEllipsoid"]["ellipsoid_name"],
                "As_semi_major_axis": dict_data["datum"]["sourceEllipsoid"]["semi_major_axis"],
                "s_1_div_f": dict_data["datum"]["sourceEllipsoid"]["inv_flattening"],

                "TargetName": dict_data["datum"]["ellipsoid"]["ellipsoid_name"],
                "At_semi_major_axis": dict_data["datum"]["ellipsoid"]["semi_major_axis"],
                "t_1_div_f": dict_data["datum"]["ellipsoid"]["inv_flattening"],

                "TranslationinX": dict_data["datum"]["towgs84"]["dx"],
                "TranslationinY": dict_data["datum"]["towgs84"]["dy"],
                "TranslationinZ": dict_data["datum"]["towgs84"]["dz"],
                "RotationAroundtheX": dict_data["datum"]["towgs84"]["ex"],
                "RotationAroundtheY": dict_data["datum"]["towgs84"]["ey"],
                "RotationAroundtheZ": dict_data["datum"]["towgs84"]["ez"],
                "ScaleCorrection": dict_data["datum"]["towgs84"]["k"],

                "LatitudeNatural": dict_data["projection"]["parameters"][0]["value"],
                "LongitudeNatural": dict_data["projection"]["parameters"][1]["value"],
                "FalseEasting": dict_data["projection"]["parameters"][2]["value"],
                "FalseNorthing": dict_data["projection"]["parameters"][3]["value"],
                "ScaleFactorat": dict_data["projection"]["parameters"][4]["value"],

                "PlateNumber": "EURA",
                "ComputationIndicator": "StandardSevenParameterStrictFormula",
                "HeightIndicator": "PhysicalHeightResult_SourceSystem",
                "HorizontalHelmert": "UnknownQuality",
                "VerticalHelmert": "UnknownQuality",
                "ProjectionType": "TransverseMercator",
            }

        return params_cs
