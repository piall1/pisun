from src.converters import RtcmHandler
from src.structures import ParamsCS


class RtcmEncoder:

    def __init__(self):
        self.rtcm_handler = RtcmHandler.RtcmHandler()

    def RTCM3_Handler_1021(self, params_packet_1021):
        self.print_data_class(params_packet_1021)
        data_buffer = bytearray(1200)
        i = 0

        self.rtcm_handler.setbitu(data_buffer, i, 8, 0xd3)
        i += 8
        self.rtcm_handler.setbitu(data_buffer, i, 6, 0)
        i += 6
        self.rtcm_handler.setbitu(data_buffer, i, 10, 0)
        i += 10

        self.rtcm_handler.setbitu(data_buffer, i, 12, params_packet_1021.MessageNumber)
        i += 12
        self.rtcm_handler.setbitu(data_buffer, i, 5, len(params_packet_1021.SourceName))
        i += 5

        for char in params_packet_1021.SourceName:
            self.rtcm_handler.setbitu(data_buffer, i, 8, ord(char))
            i += 8

        self.rtcm_handler.setbitu(data_buffer, i, 5, len(params_packet_1021.TargetName))
        i += 5

        for char in params_packet_1021.TargetName:
            self.rtcm_handler.setbitu(data_buffer, i, 8, ord(char))
            i += 8

        self.rtcm_handler.setbitu(data_buffer, i, 5, params_packet_1021.SystemIdentificationNumber)
        i += 8

        indicator_flag = 0
        if ParamsCS.UtilizedTransformationMessageIndicator.p1023.value:
            indicator_flag = 1
        if ParamsCS.UtilizedTransformationMessageIndicator.p1024.value:
            indicator_flag = 2
        if ParamsCS.UtilizedTransformationMessageIndicator.p1025.value:
            indicator_flag = 4
        if ParamsCS.UtilizedTransformationMessageIndicator.p1026.value:
            indicator_flag = 8
        if ParamsCS.UtilizedTransformationMessageIndicator.p1027.value:
            indicator_flag = 16

        self.rtcm_handler.setbitu(data_buffer, i, 10, indicator_flag)
        i += 10
        self.rtcm_handler.setbitu(data_buffer, i, 5, params_packet_1021.PlateNumber)
        i += 5
        self.rtcm_handler.setbitu(data_buffer, i, 4, params_packet_1021.ComputationIndicator)
        i += 4
        self.rtcm_handler.setbitu(data_buffer, i, 2, params_packet_1021.HeightIndicator)
        i += 2

        self.rtcm_handler.setbits(data_buffer, i, 19, self.rtcm_handler.custom_round(params_packet_1021.LatitudeofOriginAreaofValidity / 2))
        i += 19
        self.rtcm_handler.setbits(data_buffer, i, 20, self.rtcm_handler.custom_round(params_packet_1021.LongitudeofOriginAreaofValidity / 2))
        i += 20
        self.rtcm_handler.setbitu(data_buffer, i, 14, self.rtcm_handler.custom_round(params_packet_1021.N_S_ExtensionAreaofValidity / 2))
        i += 14
        self.rtcm_handler.setbitu(data_buffer, i, 14, self.rtcm_handler.custom_round(params_packet_1021.E_W_ExtensionAreaofValidity / 2))
        i += 14

        self.rtcm_handler.setbits(data_buffer, i, 23, self.rtcm_handler.custom_round(params_packet_1021.TranslationinXdirection / 0.001))
        i += 23
        self.rtcm_handler.setbits(data_buffer, i, 23, self.rtcm_handler.custom_round(params_packet_1021.TranslationinYdirection / 0.001))
        i += 23
        self.rtcm_handler.setbits(data_buffer, i, 23, self.rtcm_handler.custom_round(params_packet_1021.TranslationinZdirection / 0.001))
        i += 23

        self.rtcm_handler.setbits(data_buffer, i, 32, self.rtcm_handler.custom_round(params_packet_1021.RotationAroundtheXaxis / 0.00002))
        i += 32
        self.rtcm_handler.setbits(data_buffer, i, 32, self.rtcm_handler.custom_round(params_packet_1021.RotationAroundtheYaxis / 0.00002))
        i += 32
        self.rtcm_handler.setbits(data_buffer, i, 32, self.rtcm_handler.custom_round(params_packet_1021.RotationAroundtheZaxis / 0.00002))
        i += 32

        self.rtcm_handler.setbits(data_buffer, i, 25, self.rtcm_handler.custom_round(params_packet_1021.ScaleCorrection / 0.00001))
        i += 25

        self.rtcm_handler.setbitu(data_buffer, i, 24, self.rtcm_handler.custom_round((params_packet_1021.As_semi_major_axis - 6370000) / 0.001))
        i += 24
        self.rtcm_handler.setbitu(data_buffer, i, 25, self.rtcm_handler.custom_round((params_packet_1021.Bs_semi_minor_axis - 6350000) / 0.001))
        i += 25
        self.rtcm_handler.setbitu(data_buffer, i, 24, self.rtcm_handler.custom_round((params_packet_1021.At_semi_major_axis - 6370000) / 0.001))
        i += 24
        self.rtcm_handler.setbitu(data_buffer, i, 25, self.rtcm_handler.custom_round((params_packet_1021.Bt_semi_minor_axis - 6350000) / 0.001))
        i += 25

        self.rtcm_handler.setbitu(data_buffer, i, 3, params_packet_1021.HorizontalHelmert_MolodenskiQualityIndicator)
        i += 3
        self.rtcm_handler.setbitu(data_buffer, i, 3, params_packet_1021.VerticalHelmert_MolodenskiQualityIndicator)
        i += 3

        while i % 8 != 0:
            self.rtcm_handler.setbitu(data_buffer, i, 1, 0)
            i += 1

        len_b = int(i / 8)
        self.rtcm_handler.setbitu(data_buffer, 14, 10, (len_b - 3))
        crc = self.rtcm_handler.rtk_crc24q(data_buffer, len_b)
        self.rtcm_handler.setbitu(data_buffer, i, 24, crc)
        i += 24
        len_b_with_crc = int(i / 8)
        trimmed_buffer_with_crc = data_buffer[:len_b_with_crc]

        # self.print_data(trimmed_buffer_with_crc)

        return trimmed_buffer_with_crc

    def RTCM3_Handler_1025(self, params_packet_1025):
        self.print_data_class(params_packet_1025)

        data_buffer = bytearray(1200)
        i = 0

        self.rtcm_handler.setbitu(data_buffer, i, 8, 0xd3)
        i += 8
        self.rtcm_handler.setbitu(data_buffer, i, 6, 0)
        i += 6
        self.rtcm_handler.setbitu(data_buffer, i, 10, 0)
        i += 10

        self.rtcm_handler.setbitu(data_buffer, i, 12, params_packet_1025.MessageNumber)
        i += 12
        self.rtcm_handler.setbitu(data_buffer, i, 8, params_packet_1025.SystemIdentificationNumber)
        i += 8
        self.rtcm_handler.setbitu(data_buffer, i, 6, params_packet_1025.ProjectionType)
        i += 6

        self.rtcm_handler.setbits(data_buffer, i, 34, self.rtcm_handler.custom_round(params_packet_1025.LatitudeofNaturalOrigin / 0.000000011))
        i += 34
        self.rtcm_handler.setbits(data_buffer, i, 35, self.rtcm_handler.custom_round(params_packet_1025.LongitudeofNaturalOrigin / 0.000000011))
        i += 35

        self.rtcm_handler.setbitu(data_buffer, i, 30, self.rtcm_handler.custom_round((params_packet_1025.ScaleFactoratNaturalOrigin * 1000000.0 - 993000) / 0.00001))
        i += 30

        self.rtcm_handler.setbitu(data_buffer, i, 36, self.rtcm_handler.custom_round(params_packet_1025.FalseEasting / 0.001))
        i += 36
        self.rtcm_handler.setbits(data_buffer, i, 35, self.rtcm_handler.custom_round(params_packet_1025.FalseNorthing / 0.001))
        i += 35

        while i % 8 != 0:
            self.rtcm_handler.setbitu(data_buffer, i, 1, 0)
            i += 1

        len_b = int(i/8)
        self.rtcm_handler.setbitu(data_buffer, 14, 10, (len_b - 3))
        crc = self.rtcm_handler.rtk_crc24q(data_buffer, len_b)
        self.rtcm_handler.setbitu(data_buffer, i, 24, crc)
        i += 24
        len_b_with_crc = int(i / 8)
        trimmed_buffer_with_crc = data_buffer[:len_b_with_crc]

        # self.print_data(trimmed_buffer_with_crc)

        return trimmed_buffer_with_crc

    def print_data_class(self, dataclass_instance):
        fields = [(attribute, value) for attribute, value in dataclass_instance.__dict__.items()]
        for field in fields:
            print("{}: {}".format(*field))

    def print_data(self, data):
        data_hex = data.hex()  # Преобразуем данные в строку шестнадцатеричных символов
        counter = 0
        printed_data = ""
        for i in data_hex:
            printed_data += i
            counter += 1
            if counter % 2 == 0:  # Добавляем пробел после каждого второго символа
                printed_data += " "
            if counter % 46 == 0:  # Переходим на новую строку после 40 символов
                printed_data += "\n"
        print("-----------------------------------------")
        print(printed_data)
        print("-----------------------------------------")