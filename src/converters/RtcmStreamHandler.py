from src.services import RtcmController
from src.converters import Parser
import time


class RtcmStreamHandler:

    def __init__(self):
        self.status_apply = False
        self.system_coords = None
        self.periodCSsending = 3
        self.lastCSsending = 0
        self.rtcm_cont = RtcmController.RtcmController()
        self.bin_parser = Parser.ParserDataBinary()
        self.subscribersPacked = []

    def set_params_packed(self, status, interval, system_coords):
        self.status_apply = status
        self.periodCSsending = interval
        self.system_coords = system_coords
        self.send_parameters()

    def send_parameters(self):
        if self.system_coords is not None:
            if bool(self.system_coords):
                self.rtcm_cont.get_params_packed(self.system_coords)
            else:
                print("Сoordinate system not selected")

    def handelPackets(self, data: bytes):
        if time.time() - self.lastCSsending > self.periodCSsending:
            if self.status_apply:
                data += self.rtcm_cont.get_packed_1025()
                data += self.rtcm_cont.get_packed_1021()
            self.lastCSsending = time.time()
        for notify in self.subscribersPacked:
            notify(data)

    def subscribersPackedRTCM(self, func):
        if func not in self.subscribersPacked:
            self.subscribersPacked.append(func)

    def unsubscribePacked(self, func):
        if func in self.subscribersPacked:
            self.subscribersPacked.remove(func)



