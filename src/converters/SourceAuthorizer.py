from src.structures import ParamsCS


class SourceAuthorizer:

    def __init__(self):
        pass

    def SourceAuthorizer(self, packed_params):

        params_packet_1021 = ParamsCS.packet_1021_raw(
            1021,
            packed_params["SourceName"],
            packed_params["TargetName"],
            0,
            self.get_enum_value(packed_params["PlateNumber"], ParamsCS.PlateNumbers),
            self.get_enum_value(packed_params["ComputationIndicator"], ParamsCS.TransformationMethods),
            self.get_enum_value(packed_params["HeightIndicator"], ParamsCS.HeightResultType),
            0.0000000000,
            0.0000000000,
            0.0000000000,
            0.0000000000,
            packed_params["TranslationinX"],
            packed_params["TranslationinY"],
            packed_params["TranslationinZ"],
            packed_params["RotationAroundtheX"],
            packed_params["RotationAroundtheY"],
            packed_params["RotationAroundtheZ"],
            packed_params["ScaleCorrection"],
            packed_params["As_semi_major_axis"],
            packed_params["Bs_semi_minor_axis"],
            packed_params["s_1_div_f"],
            packed_params["At_semi_major_axis"],
            packed_params["Bt_semi_minor_axis"],
            packed_params["t_1_div_f"],
            self.get_enum_value(packed_params["HorizontalHelmert"], ParamsCS.TransformationQuality),
            self.get_enum_value(packed_params["VerticalHelmert"], ParamsCS.TransformationQuality),
        )

        params_packet_1025 = ParamsCS.packet_1025_raw(
            1025,
            0,
            self.get_enum_value(packed_params["ProjectionType"], ParamsCS.ProjectionTypes),
            packed_params["LatitudeNatural"],
            packed_params["LongitudeNatural"],
            packed_params["ScaleFactorat"],
            packed_params["FalseEasting"],
            packed_params["FalseNorthing"],
        )

        return params_packet_1021, params_packet_1025

    def get_enum_value(self, enum_val, enum_class):
        for plate_number in enum_class:
            if plate_number.name == enum_val:
                value = plate_number.value
                return value
        raise ValueError("Неверное строковое значение для PlateNumbers")

