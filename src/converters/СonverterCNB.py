import os
import time
import requests
import json
import threading
from src.structures import ConvertDataClass
from src.request import AuthorizationClass
from src.utils.Console import print


class ConverterCNB:
    auth = AuthorizationClass.AuthorizationClass()
    _url_upload = 'https://gis.pidt.net/api/open/cnb/upload'
    _url_result = 'https://gis.pidt.net/api/open/cnb/result'

    def __init__(self):
        """
        data_list - массив из структуры данных
        """
        self.data_list = []
        self.firstRequest = True
        self.waitIsRunning = False
        self.is_running = False
        self.list_data = []

    def get_data_list(self):
        return self.data_list

    def clear_data_list(self):
        self.data_list.clear()

    def convert_cnb(self, file_path: str):
        try:
            access_token = self.auth.get_access_token()
            self._request_upload_cnb(access_token, file_path)
            if not self.waitIsRunning:
                threading.Thread(target=self._request_convert_results, args=(access_token, )).start()
        except Exception as e:
            print(f"Error converting: {e}")

    def _check_file_exists(self, path) -> bool:
        """Возвращает True если файл существует, False если не существует"""
        return os.path.isfile(path) and os.access(path, os.R_OK)

    def _get_request_id_list(self):
        request_ids = []
        for item in self.data_list:
            if item.status == ConvertDataClass.Status.converting:
                request_ids.append(item.request_id)
        return request_ids

    def _get_index_from_request_id(self, request_id):
        for item in self.data_list:
            if item.request_id == request_id:
                return self.data_list.index(item)
        return None

    def _request_upload_cnb(self, access_token, file):
        if access_token is None:
            raise requests.HTTPError(f"access_token is none")
        if not self._check_file_exists(file):
            raise Exception("file not found")

        id = len(self.data_list)
        filename = os.path.basename(file)
        self.data_list.append(ConvertDataClass.CnbData(id, filename, None, None, ConvertDataClass.Status.upload))

        files = {'file': open(file, 'rb')}
        headers = {'Authorization': f'Bearer {access_token}'}

        try:
            p = requests.post(self._url_upload, files=files, headers=headers)
        except Exception as e:
            self.data_list[id].status = ConvertDataClass.Status.error
            raise requests.RequestException(f"{e}")

        if p.status_code != 200:
            self.data_list[id].status = ConvertDataClass.Status.error
            raise requests.HTTPError(f"HTTP code bad {p.status_code}")

        try:
            responce_json = json.loads(p.text)
        except Exception as e:
            self.data_list[id].status = ConvertDataClass.Status.error
            raise requests.RequestException(f"Bad responce. {e}")

        status = responce_json.get("status")
        if status != "ok":
            self.data_list[id].status = ConvertDataClass.Status.error
            message = responce_json.get("message")
            raise requests.RequestException(f"Status not ok! {message}")

        request_id = responce_json.get("request_id")
        if request_id is None:
            self.data_list[id].status = ConvertDataClass.Status.error
            message = responce_json.get("message")
            raise requests.RequestException(f"request_id not found! {message}")

        self.data_list[id].request_id = request_id
        self.data_list[id].status = ConvertDataClass.Status.converting

    def _request_convert_results(self, access_token):
        if access_token is None:
            raise requests.HTTPError(f"access_token is none")

        self.waitIsRunning = True
        error_counter = 0
        while error_counter < 50:
            request_ids = self._get_request_id_list()
            if len(request_ids) == 0:
                break

            headers = {'Authorization': f'Bearer {access_token}'}
            request = {"request_ids": request_ids}

            try:
                p = requests.post(self._url_result, json=request, headers=headers, timeout=10)
                if p.status_code != 200: raise RuntimeError("Bad responce")
                responce_json = json.loads(p.text)
                status = responce_json.get("status")
                if status != "ok": raise RuntimeError("Bad responce")
                for item in responce_json["response"]:
                    if item.get("status") == "ok":
                        request_id = item.get("request_id")
                        link = item.get("link")
                        index = self._get_index_from_request_id(request_id)
                        self.data_list[index].link = link
                        self.data_list[index].status = ConvertDataClass.Status.ready
                error_counter = 0
            except Exception as e:
                error_counter += 1
                time.sleep(5)
                continue
            time.sleep(5)
        self.waitIsRunning = False

    def request_convert_scope(self):
        access_token = self.auth.get_access_token()
        if access_token is None:
            raise requests.HTTPError(f"access_token is none")

        data_list = []
        headers = {'Authorization': f'Bearer {access_token}'}
        scope = {"scope": "user"}

        p = requests.post(self._url_result, json=scope, headers=headers, timeout=10)
        if p.status_code != 200: raise RuntimeError("Bad responce")
        responce_json = json.loads(p.text)
        status = responce_json.get("status")
        if status != "ok": raise RuntimeError("Bad responce")
        for item in responce_json["response"]:
            if item.get("status") == "ok":
                link = item.get("link")
                filename = link.split("/")[-1]
                id = len(data_list)
                data_list.append(ConvertDataClass.CnbData(id, filename, None, link, ConvertDataClass.Status.ready))
        return data_list





