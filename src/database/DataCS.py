import uuid
from src.database import Database


class DataCS:

    def __init__(self):
        self.database = Database.Database()
        self.cursor = self.database.cursor
        self.connection = self.database.connection

    def add_package_params(self, params):
        myuuid = uuid.uuid4()
        Bs_semi_minor_axis = self.get_bs_semi_minor(params["s_1_div_f"], params['As_semi_major_axis'])
        Bt_semi_minor_axis = self.get_bt_semi_minor(params['t_1_div_f'], params["At_semi_major_axis"])
        self.cursor.execute(
            'INSERT INTO Package_Params_Table (uuid, Datum, SourceName, TargetName, PlateNumber,'
            'TranslationinX, TranslationinY, TranslationinZ, RotationAroundtheX, RotationAroundtheY,'
            'RotationAroundtheZ, ScaleCorrection, As_semi_major_axis, At_semi_major_axis,'
            'Bs_semi_minor_axis, Bt_semi_minor_axis, s_1_div_f, t_1_div_f,'
            'LatitudeNatural, LongitudeNatural, ScaleFactorat, FalseEasting, FalseNorthing,'
            'ComputationIndicator, HeightIndicator, HorizontalHelmert, VerticalHelmert, ProjectionType)'
            ' VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            (str(myuuid), params['Datum'], params['SourceName'], params['TargetName'], params['PlateNumber'],
             params['TranslationinX'], params['TranslationinY'], params["TranslationinZ"],
             params['RotationAroundtheX'], params['RotationAroundtheY'], params["RotationAroundtheZ"],
             params['ScaleCorrection'], params['As_semi_major_axis'], params["At_semi_major_axis"],
             Bs_semi_minor_axis, Bt_semi_minor_axis,
             params["s_1_div_f"], params['t_1_div_f'],
             params['LatitudeNatural'], params['LongitudeNatural'], params['ScaleFactorat'],
             params['FalseEasting'], params['FalseNorthing'], params['ComputationIndicator'],
             params['HeightIndicator'], params['HorizontalHelmert'], params['VerticalHelmert'], params['ProjectionType']
             ))
        self.connection.commit()

    def get_package_params(self):
        self.cursor.execute('SELECT * FROM Package_Params_Table')
        params = self.cursor.fetchall()
        list_of_dicts = []
        for item in params:
            dict_cs = {
                "uuid":                item[0],
                "Datum":               item[1],
                "SourceName":          item[2],
                "TargetName":          item[3],
                "PlateNumber":         item[4],
                "TranslationinX":      item[5],
                "TranslationinY":      item[6],
                "TranslationinZ":      item[7],
                "RotationAroundtheX":  item[8],
                "RotationAroundtheY":  item[9],
                "RotationAroundtheZ":  item[10],
                "ScaleCorrection":     item[11],
                "As_semi_major_axis":  item[12],
                "At_semi_major_axis":  item[13],
                "Bs_semi_minor_axis":  item[14],
                "Bt_semi_minor_axis":  item[15],
                "s_1_div_f":           item[16],
                "t_1_div_f":           item[17],
                "LatitudeNatural":     item[18],
                "LongitudeNatural":    item[19],
                "ScaleFactorat":       item[20],
                "FalseEasting":        item[21],
                "FalseNorthing":       item[22],
                "ComputationIndicator":item[23],
                "HeightIndicator":     item[24],
                "HorizontalHelmert":   item[25],
                "VerticalHelmert":     item[26],
                "ProjectionType":      item[27],
            }
            list_of_dicts.append(dict_cs)
        return list_of_dicts

    def update_params_package(self, uuid, params):
        Bs_semi_minor_axis = self.get_bs_semi_minor(params["s_1_div_f"], params['As_semi_major_axis'])
        Bt_semi_minor_axis = self.get_bt_semi_minor(params['t_1_div_f'], params["At_semi_major_axis"])
        self.cursor.execute('UPDATE Package_Params_Table set Datum = ?, SourceName = ?, TargetName = ?, PlateNumber = ?,'
                            'TranslationinX = ?, TranslationinY = ?, TranslationinZ = ?, RotationAroundtheX = ?, '
                            'RotationAroundtheY = ?, RotationAroundtheZ = ?, ScaleCorrection = ?, As_semi_major_axis = ?,'
                            ' At_semi_major_axis = ?, Bs_semi_minor_axis = ?, Bt_semi_minor_axis = ?, '
                            's_1_div_f = ?, t_1_div_f = ?,LatitudeNatural = ?, LongitudeNatural = ?, '
                            'ScaleFactorat = ?, FalseEasting = ?, FalseNorthing = ?,'
                            'ComputationIndicator = ?, HeightIndicator = ?, '
                            'HorizontalHelmert = ?, VerticalHelmert = ?, ProjectionType = ?'
                            'where uuid = ?',
                            (params['Datum'], params['SourceName'], params['TargetName'], params['PlateNumber'],
                             params['TranslationinX'], params['TranslationinY'], params["TranslationinZ"],
                             params['RotationAroundtheX'], params['RotationAroundtheY'], params["RotationAroundtheZ"],
                             params['ScaleCorrection'], params['As_semi_major_axis'], params["At_semi_major_axis"],
                             Bs_semi_minor_axis, Bt_semi_minor_axis,
                             params["s_1_div_f"], params['t_1_div_f'],
                             params['LatitudeNatural'], params['LongitudeNatural'], params['ScaleFactorat'],
                             params['FalseEasting'], params['FalseNorthing'], params['ComputationIndicator'],
                             params['HeightIndicator'], params['HorizontalHelmert'], params['VerticalHelmert'], params['ProjectionType'],
                             uuid,))
        self.connection.commit()

    def delete_params_package(self, uuid):
        self.cursor.execute('DELETE FROM Package_Params_Table WHERE uuid = ?', (uuid,))
        self.connection.commit()
        self.get_package_params()

    def get_bs_semi_minor(self, s_1_div, as_semi_major):
        bs_semi_minor = (float(as_semi_major) * (float(s_1_div) - 1)) / float(s_1_div)
        return bs_semi_minor

    def get_bt_semi_minor(self, t_1_div, at_semi_major):
        bt_semi_minor = (float(at_semi_major) * (float(t_1_div) - 1)) / float(t_1_div)
        return bt_semi_minor
