import json
from datetime import datetime, timezone
from src.database import Database


class DataPoints:

    def __init__(self):
        self.database = Database.Database()
        self.cursor = self.database.cursor
        self.connection = self.database.connection

    def add_points(self, points):
        if type(points["edit_at"]) == str:
            edit_at = datetime.strptime(points["edit_at"], "%Y-%m-%d %H:%M:%S").timestamp()
            points["edit_at"] = edit_at
        if type(points["sync_at"]) == str:
            sync_at = datetime.strptime(points["sync_at"], "%Y-%m-%d %H:%M:%S").timestamp()
            points["sync_at"] = sync_at
        if type(points["created_at"]) == str:
            created_at = datetime.strptime(points["created_at"], "%Y-%m-%d %H:%M:%S").timestamp()
            points["created_at"] = created_at
        if type(points["updated_at"]) == str:
            update_at = datetime.strptime(points["updated_at"], "%Y-%m-%d %H:%M:%S").timestamp()
            points["updated_at"] = update_at
        self.cursor.execute(
            'INSERT INTO Points_Table (id, name, data, deleted_at, created_at, update_at, uuid, device, '
            'version, edit_at, sync_at) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
            (points["id"], points['name'], json.dumps(points['data'], indent=4), points['deleted_at'],
             points['created_at'],
             points['updated_at'], points['uuid'], points['device'],
             points['version'], points['edit_at'], points['sync_at']
             ))
        self.connection.commit()

    def get_list_points(self):
        self.cursor.execute('SELECT * FROM Points_Table')
        tuple_points = self.cursor.fetchall()
        return tuple_points

    def update_point(self, uuid, name, coords):
        sync_at = datetime.utcfromtimestamp(0)
        edit_at = datetime.now(timezone.utc).timestamp()
        self.cursor.execute('UPDATE Points_Table set name = ?, data = ?, sync_at = ?, edit_at = ?  where uuid = ?',
                            (name, coords, sync_at, edit_at, uuid,))
        self.connection.commit()

    def delete_point(self, uuid, status_authorize):
        self.cursor.execute('SELECT id FROM Points_Table WHERE uuid = ?', (uuid,))
        points_tuple = self.cursor.fetchall()
        points_id = points_tuple[0][0]

        if points_id == None or status_authorize == False:
            self.cursor.execute('DELETE FROM Points_Table WHERE uuid = ?', (uuid,))
            self.connection.commit()
            self.refresh_list_points()
        else:
            deleted_at = datetime.now(timezone.utc).timestamp()
            self.cursor.execute('UPDATE Points_Table set deleted_at = ? WHERE uuid = ?', (deleted_at, uuid,))
            self.connection.commit()

    def delete_all_local_points(self):
        self.cursor.execute("DELETE FROM Points_Table")
        self.connection.commit()

    def refresh_list_points(self):
        self.get_list_points()
