import sqlite3


class Database:
    connection = None
    cursor = None
    _instance = None

    def __init__(self):
        try:
            self.connect()
            self.create_points_table()
            self.create_packed_table()
        except Exception as e:
            print(f"Error database: {e}")

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(Database, cls).__new__(cls)
        return cls._instance

    def connect(self):
        # Устанавливаем соединение с базой данных
        self.connection = sqlite3.connect('resources/database.db', check_same_thread=False)
        self.cursor = self.connection.cursor()

    def disconnect(self):
        ### Закрыть соединение после остановки программы
        self.connection.close()

    def create_points_table(self):
        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS Points_Table (
        id INTEGER ,
        name TEXT,
        data TEXT,
        deleted_at INTEGER,
        created_at TEXT,
        update_at TEXT,
        uuid TEXT PRIMARY KEY,
        device TEXT,
        version TEXT,
        edit_at INTEGER,
        sync_at INTEGER
        )
        ''')

    def create_packed_table(self):
        self.cursor.execute('''
        CREATE TABLE IF NOT EXISTS Package_Params_Table (
        uuid INTEGER,
        Datum TEXT,
        SourceName TEXT,
        TargetName TEXT,
        PlateNumber TEXT,
        TranslationinX REAL,
        TranslationinY REAL,
        TranslationinZ REAL,
        RotationAroundtheX REAL,
        RotationAroundtheY REAL,
        RotationAroundtheZ REAL,
        ScaleCorrection REAL,
        As_semi_major_axis REAL,
        At_semi_major_axis REAL,
        Bs_semi_minor_axis REAL,
        Bt_semi_minor_axis REAL,
        s_1_div_f REAL,
        t_1_div_f REAL,
        LatitudeNatural REAL,
        LongitudeNatural REAL,
        ScaleFactorat REAL,
        FalseEasting REAL,
        FalseNorthing REAL,
        ComputationIndicator TEXT,
        HeightIndicator TEXT,
        HorizontalHelmert TEXT,
        VerticalHelmert TEXT,
        ProjectionType TEXT
        )
        ''')