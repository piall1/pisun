import datetime
import os


class FileRecorder:
    def __init__(self):
        self.files_deleted = []
        self.max_files = 200
        self.file_path = "files/"
        self.filename = ""
        self.status_file = "close"
        self.format_file = "cnb"
        if not os.path.exists(self.file_path):
            os.makedirs(self.file_path)
        else:
            pass

    def new_file(self):
        if self.file:
            self.file.close()
            self.file = open(self.file_path + self.filename, "wb")

    def make_filename(self, unixtime):
        dt = datetime.datetime.utcfromtimestamp(unixtime)
        year = dt.year
        month = dt.month
        day = dt.day
        hour = dt.hour
        minute = dt.minute
        second = dt.second

        name_file = f"{year:04}{month:02}{day:02}{hour:02}{minute:02}{second:02}.{self.format_file}"
        self.filename = name_file

    def open_file(self):
        if self.status_file == "close":
            try:
                self.file = open(self.file_path + self.filename, "wb")
                self.status_file = "open"
            except Exception as e:
                print(f"Error opening file: {e}")

    def close_file(self):
        if self.status_file == "open":
            try:
                self.file.close()
                self.status_file = "close"
                self.filename = ""
            except Exception as e:
                print(f"Error closing file: {e}")

    def write(self, binary_data):
        if self.status_file == "open":
            try:
                self.file.write(binary_data)
                self.file.flush()
            except Exception as e:
                print(f"Error writing to file: {e}")

    def set_max_files(self, max_size):
        self.max_files = max_size

    def delete_files(self):
        files = os.listdir(self.file_path)
        try:
            files.sort(reverse=True)
            files_list = files[self.max_files:]
            self.files_deleted = files_list
            for file in files_list:
                os.remove(self.file_path + file)
            return files_list
        except Exception as e:
            print(f"Exception delete_files {e}")
