import subprocess
import socket
from src.utils.Console import print
import platform


class LocalNtrip():
    tcpserverPort = 5201
    tcp_out_status = False
    tcp_out_port = 12345
    ntripPort = 2101
    ntripMountpoint = 'PiGO'
    ntripStr = '\"RTCM3;RTCM 3;;2;GPS+GLONASS+GALILEO+BDS;IQProxy;RUS;53.4632;51.4750;1;0;IQProxy;;B;Y;;\"'
    ntripUser = 1234
    ntripPassword = 1234
    error_message = ""
    process = None
    is_connect = False

    def change_mountpoint(self, new_moutpoint):
        self.ntripMountpoint = new_moutpoint

    def get_ip_addresses(self):
        ip_addresses = []
        hostname = socket.gethostname()
        try:
            addresses = socket.getaddrinfo(hostname, None, socket.AF_INET)
            ip_addresses = [addr[4][0] for addr in addresses]
        except socket.gaierror as e:
            print(f"Error: {e}")
        return ip_addresses

    def check_ports(self):
        counter = 0
        while counter < 10000:
            try:
                with socket.socket(socket.AF_INET,socket.SOCK_STREAM) as s:
                    s.bind(("127.0.0.1", self.tcpserverPort))
                    s.listen(1)
                    print(f"Порт {self.tcpserverPort} доступен.")
                    return True
            except OSError as e:
                print(f"Порт {self.tcpserverPort} занят. Пробуем следующий.")
                self.tcpserverPort += 1
                counter += 1
        return False

    def set_tcpout(self, tcp_out_status: bool, tcp_out_port: int):
        self.tcp_out_status = tcp_out_status
        self.tcp_out_port = tcp_out_port

    def launch_str2str(self):
        if not self.check_ports():
            print(f"Порты {self.tcpserverPort} - {self.tcpserverPort + 1} недоступны.")
            return
        gen_in = f'tcpsvr://:{str(self.tcpserverPort)}'

        if platform.system() == 'Windows':
            str2str_executable = "resources/str2str.exe"
        elif platform.system() == 'Darwin':
            str2str_executable = "resources/str2str"
        elif platform.system() == "Linux":
            str2str_executable = "resources/str2str"

        gen_out = f'ntripc://{str(self.ntripUser)}:{str(self.ntripPassword)}@:{str(self.ntripPort)}/{str(self.ntripMountpoint)}:{str(self.ntripStr)}'

        if self.tcp_out_status:
            gen_out_tcp = f'tcpsvr://:{self.tcp_out_port}'
            gen_launch = [str2str_executable, "-in", gen_in, "-out", gen_out, "-out", gen_out_tcp]
        else:
            gen_launch = [str2str_executable, "-in", gen_in, "-out", gen_out]

        try:
            print("Запускаю str2str: ", " ".join(gen_launch))
            print(f"Для подключения к кастеру вы должны находиться в одной сети с этим устройством!")
            print(f"Логин и пароль '1234'.Подключиться можно по следующим ip адресам: \n{self.get_ip_addresses()}\n")
            self.is_connect = True
            self.process = subprocess.Popen(gen_launch)
        except FileNotFoundError:
            self.is_connect = False
            print(f"Исполняемый файл {str2str_executable} не найден.")
        return self.is_connect

    def stop_str2str(self):
        if not self.is_connect:
            return
        if self.process is not None:
            returncode = self.process.poll()
            if returncode is None:
                 self.process.terminate()
                 try:
                    self.process.wait(timeout=5)
                    return True
                 except subprocess.TimeoutExpired as e:
                    print("Не удалось остановить процесс. Превышено время ожидания.", str(e))
                    return False
            else:
                return False
        else:
            print("NTRIP local еще не был запущен.")


