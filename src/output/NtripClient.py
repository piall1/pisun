import base64
import queue
import socket
import threading
import time


class NtripClient:
    host = ''
    port = 0
    user_agent = 'NTRIP'
    login = ""
    password = ""
    station_name = ""
    stations = []
    subscribersClient = []
    data_queue = queue.Queue()
    is_running = False
    is_connected = False
    response = b''

    def __init__(self):
        self.client_socket = None
        self.thread = None

    def subscribeClient(self, func):
        if func not in self.subscribersClient:
            self.subscribersClient.append(func)

    def unsubscribeClient(self, func):
        if func in self.subscribersClient:
            self.subscribersClient.remove(func)

    def set_settings(self, host, port, login, pwd):
        self.host = host
        self.port = port
        self.login = login
        self.password = pwd

    def make_source_table_request(self):
        request_header = ('GET / HTTP/1.0\r\n' +
                               'Host: %s:%s\r\n' +
                               'User-Agent: %s\r\n' +
                               'Accept: */*\r\n' +
                               '\r\n') % (self.host, self.port, self.user_agent)
        return request_header

    def make_basestation_request(self, station_name):
        authorization = base64.b64encode((self.login + ":" + self.password).encode()).decode()
        basestation_request = (f'GET /{station_name} HTTP/1.0\r\n' +
                              'User-Agent: NTRIP Checker/4.2.1\r\n' +
                              'Accept: */*\r\n' +
                              f'Authorization: Basic {authorization}\r\n' +
                              '\r\n')
        return basestation_request

    def connect(self, request):
        if self.is_connected:
            print("Уже есть подключение с ntrip кастером.")
            return self.is_connected
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_address = (self.host, self.port)
        self.client_socket.connect(server_address)
        self.client_socket.send(request.encode())
        self.is_connected = True
        return self.is_connected

    def source_table_request(self):
        answer_find = False
        request = self.make_source_table_request()
        self.connect(request)
        while True:
            recv = self.client_socket.recv(1024)
            if not recv:
                break
            self.response += recv
            if b"SOURCETABLE 200 OK" in self.response:
                answer_find = True
        self.disconnect()
        return answer_find

    def get_list_basestation(self):
        data = self.response.decode("utf-8")
        for line in data.splitlines():
            if line.startswith("STR"):
                columns = line.split(";")
                station_names = columns[1]
                self.stations.append(station_names)
        self.response = b''
        print("Список баз:", self.stations)
        return self.stations

    def basestation_request(self, base: str):
        answer_find = False
        self.station_name = base
        if self.station_name in self.stations:
            request = self.make_basestation_request(self.station_name)
            self.connect(request)
            data = self.client_socket.recv(100)
            if b"ICY 200 OK" in data:
                answer_find = True
                print("Ответ от базы", data)
            else:
                print("Base not found")
        return answer_find

    def reconnect(self):
        self.disconnect()
        self.basestation_request(self.station_name)

    def disconnect(self):
        if self.is_connected:
            self.is_connected = False
            self.client_socket.close()

    def start(self):
        self.is_running = True
        self.thread = threading.Thread(target=self.receive_data_thread, name="NTRIP Client")
        self.thread.start()
        return self.is_running

    def stop(self):
        if self.is_running:
            self.is_running = False
            self.thread.join()

    def receive_data_thread(self):
        if not self.is_connected:
            print(self.is_connected)
            try:
                self.basestation_request(self.station_name)
            except Exception as e:
                print(f"Connection error: {e}")
        while self.is_running:
            print(self.is_running)
            try:
                recv = self.client_socket.recv(1024)
                if not recv:
                    pass
                for notify in self.subscribersClient:
                    notify(recv)
            except Exception as e:
                print(f"Ошибка соединения![{e}]. Восстановление соединения...")
                time.sleep(5)
                try: self.reconnect()
                except Exception: pass
        self.is_running = False