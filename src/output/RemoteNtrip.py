import queue
import threading
import socket
import time
from src.utils.Console import print


class RemoteNtrip:
    is_connected = False
    is_running = False
    data_queue = queue.Queue()
    client_socket = None
    ntrip_request = ""
    host = ""
    port = ""
    password = ""
    mountpoint = ""

    def __init__(self):
        self.thread = None

    def set_settings(self, host, port, pwd, mount):
        self.host = host
        self.port = int(port)
        self.password = pwd
        self.mountpoint = mount

    def make_ntrip_request(self):
        self.ntrip_request = f'SOURCE {self.password} {self.mountpoint}\r\n'
        self.ntrip_request += 'Source-Agent: NTRIP PiSun\r\n'
        self.ntrip_request += 'STR: RTCM3;RTCM 3;;2;GPS+GLONASS+GALILEO+BDS;PiGO;RUS;53.4632;51.4750;1;0;IQProxy;;B;Y;;\r\n'
        self.ntrip_request += '\r\n\r\n'

    def connect(self):
        if self.is_connected:
            print("Уже есть подключение с ntrip кастером.")
            return self.is_connected
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.connect((self.host, self.port))
        self.client_socket.sendall(self.ntrip_request.encode())
        data = self.client_socket.recv(100)
        self.is_connected = True
        print("Сервер ответил:", data)
        return self.is_connected

    def reconnect(self):
        self.disconnect()
        self.connect()

    def disconnect(self):
        if self.is_connected:
            self.is_connected = False
            self.client_socket.close()

    def start(self):
        self.is_running = True
        self.thread = threading.Thread(target=self.thread_function, name="NTRIP Manager")
        self.thread.start()
        return self.is_running

    def stop(self):
        if not self.is_running:
            return
        self.is_running = False
        self.thread.join()
        self.disconnect()
        self.thread = None

    def thread_function(self):
        data_buffer = b''
        if not self.is_connected:
            try:
                self.connect()
            except Exception as e:
                print(f"Connection error: {e}")
        while self.is_running:
            try:
                data = self.data_queue.get(timeout=1)
                data_buffer += data
                self.client_socket.send(data_buffer)
            except queue.Empty:
                pass
            except Exception as e:
                print(f"Ошибка соединения с сервером [{e}]. Восстановление соединения...")
                time.sleep(5)
                try: self.reconnect()
                except Exception: pass
            data_buffer = b''
        self.is_running = False

    def append_data(self, bytearray_data):
        self.data_queue.put(bytearray_data)


