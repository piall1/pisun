import os
import requests
import json
import threading
from datetime import datetime, timezone


class AuthorizationClass:
    _secret = None
    _instance = None
    _url_secret = 'https://gis.pidt.net/api/auth/login'
    _url_access_token = 'https://gis.pidt.net/api/auth/login2w'
    _url_refresh_token = 'https://gis.pidt.net/api/auth/refresh_token'
    file_auth = "resources/authorization"

    def __init__(self):
        self.event = threading.Event()
        self._user_pin = 0
        self._access_token = None
        self._token_refresh = None
        self._expires_in = None
        self.status_validity = None
        self._user = None
        self._limits = None
        self._status_dict = {}
        self.status_authorize = None
        try:
            self._load_token()
        except Exception as e:
            print(f"Error log in: {e}")

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(AuthorizationClass, cls).__new__(cls)
        return cls._instance

    def get_access_token(self):
        if self._access_token is None:
            self._load_token()
        validity_result = self.check_for_validity()
        if not validity_result:
            self._refresh_token()
        return self._access_token

    def get_authorized(self, telegram_id: str, pin: str):
        if telegram_id == "":
            raise Exception("Login not found.")
        if pin == "":
            raise Exception("Password not found.")
        self._secret = self._request_secret_key(telegram_id, pin)

    def get_status_authorize(self):
        if self._access_token is None:
            self.status_authorize = False
            self._status_dict = {"auth": self.status_authorize, "limits": self._limits}
        else:
            self.status_authorize = True
            self._status_dict = {"auth": self.status_authorize, "limits": self._limits}
        return self._status_dict

    def log_out_of_account(self):
        with open(self.file_auth, 'w'):
            pass
        self._access_token = None
        self._token_refresh = None
        self._expires_in = None
        self._user = None
        self._limits = None
        self.status_authorize = False

    def entry_user_pin(self, pin_code):
        if self._secret is None:
            raise Exception("Secret not found.")
        if len(pin_code) == 0:
            raise Exception("Pin code not found.")
        self._request_access_token(self._secret, pin_code)
        self._save_access_token()

    def get_name_profile(self):
        try:
            if self._user is None:
                self._load_token()
            return self._user["name"]
        except Exception as e:
            print(f"Error set name profile: {e}")

    def _save_access_token(self):
        dict_data = {"access_token": self._access_token,
                     "refresh_token": self._token_refresh,
                     "expires_in": self._expires_in,
                     "user": self._user,
                     "limits": self._limits}
        with open(self.file_auth, "w") as file:
            json.dump(dict_data, file)

    def _load_token(self):
        if not self._check_settings_file(self.file_auth):
            raise Exception("Log in to pidt services!")
        with open(self.file_auth, "r") as file:
            data = file.read()
            if data == "":
                raise Exception("Log in!")
            responce = json.loads(data)
            self._access_token = responce["access_token"]
            self._token_refresh = responce["refresh_token"]
            self._expires_in = responce["expires_in"]
            self._user = responce["user"]
            self._limits = responce["limits"]

    def _check_settings_file(self, path) -> bool:
        """Возвращает True если файл существует, False если не существует"""
        return os.path.isfile(path) and os.access(path, os.R_OK)

    def check_for_validity(self):
        min = 600
        current_data = datetime.now(timezone.utc).timestamp()
        expires_in = datetime.strptime(self._expires_in,"%Y-%m-%d %H:%M:%S").timestamp()
        if expires_in is None:
            raise Exception("expires in not found")
        res = expires_in - min
        if res > current_data:
            self.status_validity = True
        else:
            self.status_validity = False
        return self.status_validity

    def _refresh_token(self):
        refresh_token = {'refresh_token': self._token_refresh}
        if self._token_refresh is None:
            raise Exception("refresh token not found")
        p = requests.post(self._url_refresh_token, data=refresh_token)
        if p.status_code != 200:
            raise requests.HTTPError(f"HTTP code bad {p.status_code}")
        try:
            responce_json = json.loads(p.text)
            status = responce_json["status"]
            if status != "ok":
                message = responce_json.get("message", "")
                raise requests.RequestException(f"Status not ok! {message}")
            self._access_token = responce_json["access_token"]
            self._token_refresh = responce_json["refresh_token"]
            self._expires_in = responce_json["expires_in"]
            self._save_access_token()
        except Exception as e:
            raise requests.RequestException(f"Bad responce. {e}")

    def _request_secret_key(self, telegram_id: str, pin: str):
        reqiest = {'telegram_id': telegram_id, 'pin': pin}
        p = requests.post(self._url_secret, data=reqiest)
        if p.status_code != 200:
            raise requests.HTTPError(f"HTTP code bad {p.status_code}")
        try:
            responce_json = json.loads(p.text)
            status = responce_json["status"]
            if status != "ok":
                message = responce_json.get("message",  "")
                raise requests.RequestException(f"Status not ok! {message}")
            secret = responce_json["secret"]
            return secret
        except Exception as e:
            raise requests.RequestException(f"Bad responce. {e}")

    def _request_access_token(self, secret: str, pin: int):
        request = {'secret': secret, 'pin': pin}
        p = requests.post(self._url_access_token, data=request)
        if p.status_code != 200:
            raise requests.HTTPError(f"HTTP code bad {p.status_code}")
        try:
            responce_json = json.loads(p.text)
            status = responce_json["status"]
            if status != "ok":
                message = responce_json.get("message", "")
                raise requests.RequestException(f"Status not ok! {message}")
            self._access_token = responce_json["access_token"]
            self._token_refresh = responce_json["refresh_token"]
            self._expires_in = responce_json["expires_in"]
            self._user = responce_json["user"]
            self._limits = responce_json["user"]["limits"]
            return
        except Exception as e:
            raise requests.RequestException(f"Bad responce. {e}")
