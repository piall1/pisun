import json
import requests


class PointsClass:
    _url_sync = 'https://gis.pidt.net/api/cloud/gpoint/sync'

    def sync_points(self, access_token, points):
        list_sync_points = self._request_sync_points(access_token, points)
        return list_sync_points

    def _request_sync_points(self, access_token, points):
        if access_token is None:
            raise requests.HTTPError(f"access_token is none")

        headers = {'Authorization': f'Bearer {access_token}'}
        point_json = {"points": points}

        try:
            p = requests.post(self._url_sync, json=point_json, headers=headers)
            if p.status_code != 200: raise RuntimeError("Bad responce")
            responce_json = json.loads(p.text)
            status = responce_json.get("status")
            if status != "ok": raise RuntimeError("Bad responce")
            points = responce_json["points"]
            return points
        except Exception as e:
            print(f"Error request points: {e}")
