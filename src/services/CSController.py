from src.database import DataCS


class ControllerCS:
    _instance = None
    select_cs = None

    def __init__(self):
        self.database = DataCS.DataCS()

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(ControllerCS, cls).__new__(cls)
        return cls._instance

    def get_select_cs(self):
        return self.select_cs

    def get_cs_from_view(self, params_cs):
        self.select_cs = params_cs

    def get_list_coords_system(self):
        return self.database.get_package_params()

    def add_coords_system(self, params):
        self.database.add_package_params(params)

    def update_coords_system(self, uuid, params):
        if uuid is None:
            raise Exception("Система координат не выбрана")
        self.database.update_params_package(uuid, params)

    def delete_coords_system(self, uuid):
        self.database.delete_params_package(uuid)



