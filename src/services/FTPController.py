import threading

from src.structures import FileManager
from src.connections import FTPClient


class FTPController:

    def __init__(self):
        self.is_thread_running = False
        self.lock = threading.Lock()
        self.ftp_client = FTPClient.FTPClient()
        self.file_manager = FileManager.FileManager()

    def create_data_json(self):
        self.file_manager.create_data_json()

    def set_status_record(self, current_file, status):
        self.file_manager.set_status_record(current_file, status)

    def set_status_deleted(self, list_deleted_file):
        self.file_manager.set_status_deleted(list_deleted_file)

    def set_params_to_connect(self, host, port, login, pwd):
        self.ftp_client.set_connection_details(host, port, login, pwd)

    def connect_to_server(self):
        self.ftp_client.connect_to_ftp_server()

    def check_ready_files(self):
        return self.file_manager.check_ready_files()

    def delete_file_from_json(self):
        deleted_files = self.file_manager.check_deleted_files()
        if len(deleted_files):
            return
        else:
            self.file_manager.delete_name_files(deleted_files)

    def start_thread_send(self):
        with self.lock:
            if self.is_thread_running:
                print("FTP Client уже запущен.")
                return

            self.is_thread_running = True
            self.thread = threading.Thread(target=self.thread_send, name="FTP Client")
            self.thread.start()
            print("Запущен FTP Client")

    def thread_send(self):
        try:
            files_ready = self.check_ready_files()
            if len(files_ready) != 0:
                self.connect_to_server()
                self.ftp_client.send_file_to_server(files_ready)
                self.ftp_client.disconnect()
            return
        except Exception as e:
            print(f"Ошибка при работе с FTP сервером: {e}")
        finally:
            with self.lock:
                self.is_thread_running = False
                print("FTP Client завершил работу.")