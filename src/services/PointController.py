import json
import uuid
from src.database import DataPoints
from src.request import AuthorizationClass, PointsClass
from datetime import datetime, timezone


class PointController:

    def __init__(self):
        self.database = DataPoints.DataPoints()
        self.auth = AuthorizationClass.AuthorizationClass()
        self.points_settings = PointsClass.PointsClass()
        self.params_dict = {}

    def save_input_points(self, name, coords):
        coords_dict = {
            "lat": coords["lat"],
            "lon": coords["lon"],
            "height": coords["height"]
        }
        self.params_dict['name'] = name
        self.params_dict['data'] = coords_dict
        self.params_dict['id'] = None
        self.set_params_in_db()
        self.database.add_points(self.params_dict)

    def get_list_points(self, trashed: bool):
        tuple_points = self.database.get_list_points()
        list_points = self.parce_params(tuple_points)
        if trashed:
            return list_points
        else:
            filtered_list = [point for point in list_points if point["deleted_at"] is None]
            return filtered_list

    def delete_point(self, uuid):
        status_authorize = self.auth.get_status_authorize()
        self.database.delete_point(uuid, status_authorize["auth"])

    def update_point(self, uuid, name, coords):
        if uuid is None:
            raise Exception("Точка не выбрана!")
        if name is None:
            raise Exception("point name is empty")
        if len(coords) == 0:
            raise Exception("coords is empty")
        coords_js = self.parse_into_js(coords)
        self.database.update_point(uuid, name, coords_js)

    def sync_points(self):
        token = self.auth.get_access_token()
        local_points = self.get_list_points(True)
        sync_points = self.points_settings.sync_points(token, local_points)
        self.database.delete_all_local_points()
        for point in sync_points:
            self.database.add_points(point)
        return sync_points

    def set_params_in_db(self):
        myuuid = uuid.uuid4()
        self.params_dict['deleted_at'] = None
        self.params_dict['created_at'] = None
        self.params_dict['updated_at'] = None
        self.params_dict['uuid'] = str(myuuid)
        self.params_dict['device'] = "PiSun"
        self.params_dict['version'] = 0
        self.params_dict['edit_at'] = datetime.now(timezone.utc).timestamp()
        self.params_dict['sync_at'] = datetime.utcfromtimestamp(0)

    def parce_params(self, tuple):
        list_of_dicts = []
        for item in tuple:
            dict_point = {
                "id": item[0],
                "name": item[1],
                "data": json.loads(item[2]),
                "deleted_at": item[3],
                "uuid": item[6],
                "device": item[7],
                "version": item[8],
                "edit_at": item[9],
                "sync_at": item[10]
            }

            edit_at = datetime.utcfromtimestamp(item[9])
            dict_point["edit_at"] = datetime.strftime(edit_at, "%Y-%m-%d %H:%M:%S")
            list_of_dicts.append(dict_point)
        return list_of_dicts

    def parse_into_js(self, coords):
        coords_dict = {
            'lat': coords["lat"],
            'lon': coords["lon"],
            'height': coords["height"]
        }
        coords_json = json.dumps(coords_dict)
        return coords_json



