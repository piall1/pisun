import json
from src.converters import SourceAuthorizer
from src.converters import RtcmEncoder
from src.database import DataCS


class RtcmController:

    def __init__(self):
        self.authorizer = SourceAuthorizer.SourceAuthorizer()
        self.encoder = RtcmEncoder.RtcmEncoder()
        self.database = DataCS.DataCS()
        self.formed_packed_1021 = None
        self.formed_packed_1025 = None

    def get_params_packed(self, params):
        parameters_1021, parameters_1025 = self.authorizer.SourceAuthorizer(params)
        self.formed_packed_1021 = self.encoder.RTCM3_Handler_1021(parameters_1021)
        self.formed_packed_1025 = self.encoder.RTCM3_Handler_1025(parameters_1025)
        return self.formed_packed_1021, self.formed_packed_1025

    def get_packed_1021(self):
        return self.formed_packed_1021

    def get_packed_1025(self):
        return self.formed_packed_1025

