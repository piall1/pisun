import os
import subprocess
import threading
import time
from src.utils import ImportLib
from src.structures import SettingsWifi
from src.structures import CommandWifi
from src.utils.Console import print


class WifiController:

    def __init__(self):
        try:
            self.wifi_manager = SettingsWifi.SettingsWifi()
            self.command = CommandWifi.CommandWifi()
            self.check_lib = ImportLib.ImportLib()

            self.is_connect = None
            self.is_run_monitoring = False
            self.thread_net = None

            self.connect_to_available_network()
            self.start_monitoring_net()

        except Exception as e:
            print(f"{e}")

    def __del__(self):
        self.stop_monitoring_net()

    def start_monitoring_net(self):
        try:
            self.is_run_monitoring = True
            self.thread_net = threading.Thread(target=self.thread_monitoring, name="Monitoring Net").start()
        except Exception as e:
            print(f"Ошибка при запуске мониторинга сети: {e}")

    def stop_monitoring_net(self):
        if self.is_run_monitoring:
            self.is_run_monitoring = False
            self.thread_net.join()

    def thread_monitoring(self):
        try:
            while self.is_run_monitoring:
                previous_net, ip = self.wifi_manager.get_previous_network()
                if previous_net is None or previous_net == "":
                    return

                name = self.check_connect()
                if name == "":
                    return

                if name == previous_net:
                    print(f"Устройство подключено к сети {previous_net}.")
                else:
                    print(f"Соединение c {previous_net} было разорвано. Пробуем подключиться к сети...")

                    result = self.wifi_manager.connect_to_saved_net(previous_net)

                    time.sleep(1)

                    if result == 0:
                        print(f"Подключение к предыдущей сети {previous_net}")
                    if result == 1:
                        print("Не удалось подключиться к сохранённой сети.")
                        self.try_reconnect(previous_net)

                time.sleep(300)

        except Exception as e:
            print(f"Ошибка мониторинга сети: {e}")

    def check_connect(self):
        result = subprocess.check_output(self.command.current_connections(), text=True)
        connections = result.strip().split('\n')

        if not connections:
            print("Не удалось определить текущее подключение.")
            return

        for connection in connections:
            name, type_, device = connection.split(':')
            if "wlan" in device:
                print(f"Текущая сеть: {name}.")
                return name
        else:
            print(f"Нету соединения")
            return ""

    def try_reconnect(self, previous_net):
        networks = []
        for x in range(5):
            available_net = self.wifi_manager.get_available_networks()
            networks = available_net
        for net in networks:
            ssid = net["SSID"]
            if ssid == previous_net:
                response = self.wifi_manager.connect_to_saved_net(ssid)

                time.sleep(1)

                if response == 0:
                    print(f"Удалось подключиться к предыдущей сети {previous_net}.")
                    self.wifi_manager.save_ssid_net(previous_net)
                    return
                else:
                    print("Не удалось подключиться к предыдущей сети. Подключитесь к новой сети через Hotspot")
                    self.wifi_manager.save_ssid_net("Hotspot")
                    self.activated_hotspot()
                    return
            else:
                print("Последняя используемая сеть не найдена. Подключитесь к новой сети через Hotspot")
                self.wifi_manager.save_ssid_net("Hotspot")
                self.activated_hotspot()
                return

    def activated_hotspot(self):
        name = self.check_connect()
        if name == "":
            return
        if name == "Hotspot":
            return
        self.wifi_manager.create_hotspot()

    def connect_to_available_network(self):
        self.check_lib.check_network_manager()
        net_list = self.wifi_manager.get_save_networks()
        net_list = [net for net in net_list if net["ssid"] != "Hotspot"]

        if len(net_list) == 0:
            print("Не удалось найти сохранённые сети. Создаём точку доступа.")
            self.wifi_manager.create_hotspot()
            self.wifi_manager.save_ssid_net("Hotspot")
            return

        if len(net_list) >= 2:
            print("Несколько сохранённых сетей. Создаём точку доступа.")
            self.wifi_manager.create_hotspot()
            self.wifi_manager.save_ssid_net("Hotspot")
            return

        ssid = None
        for net in net_list:
            ssid = net["ssid"]
            if ssid == "Hotspot":
                continue
            result = os.system(self.command.connections(ssid))
            time.sleep(1)

            if result == 0:
                self.wifi_manager.save_ssid_net(ssid)
                self.is_connect = True
                break
            else:
                self.is_connect = False

        if self.is_connect:
            print(f"Успешное подключение к сети {ssid}.")
            return
        else:
            self.wifi_manager.create_hotspot()
            self.wifi_manager.save_ssid_net("Hotspot")
            print(f"Создаём точку доступа.")
            return



