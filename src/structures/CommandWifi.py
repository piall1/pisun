
class CommandWifi:

    def scanning_available_net(self):
        return ["sudo", "iwlist", "wlan0", "scan"]

    def scanning_saved_net(self):
        return "nmcli -g NAME,UUID,DEVICE,TYPE connection show"

    def connections(self, ssid):
        return f"sudo nmcli connection up {ssid}"

    def profile(self, ssid):
        return f"sudo nmcli connection show | grep -w \"{ssid}\""

    def ip_address(self, ssid):
        return f"sudo nmcli device show | grep -A 10 -w {ssid}"

    def deletions(self, ssid):
        return f"sudo nmcli con delete {ssid}"

    def current_connections(self):
        return ["sudo", "nmcli", "-t", "-f", "NAME,TYPE,DEVICE", "connection", "show", "--active"]

    def additions_network(self, ssid, password):
        list_settings = [f"sudo nmcli connection add type wifi ifname wlan0 con-name {ssid} ssid \"{ssid}\"",
                         f"sudo nmcli connection modify {ssid} wifi-sec.key-mgmt wpa-psk",
                         f"sudo nmcli connection modify {ssid} wifi-sec.psk \"{password}\"",
                         f"sudo nmcli connection up {ssid}"]
        return list_settings

    def creations_hotspot(self):
        hotspot = ["sudo nmcli con add type wifi ifname wlan0 con-name Hotspot autoconnect yes ssid PiHatel",
                   "sudo nmcli con modify Hotspot 802-11-wireless.mode ap 802-11-wireless.band bg ipv4.method shared",
                   "sudo nmcli con modify Hotspot wifi-sec.key-mgmt wpa-psk ipv4.addresses 192.168.10.1/24",
                   "sudo nmcli con modify Hotspot wifi-sec.psk \"12345678\"", "sudo nmcli con up Hotspot"]
        return hotspot