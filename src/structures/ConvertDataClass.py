from enum import Enum
from dataclasses import dataclass


class Status(Enum):
    error = -1
    upload = 0
    converting = 1
    ready = 2


@dataclass
class CnbData:
    id: int
    file_path: str  # Путь к файлу
    request_id: str  # id запроса
    link: str  # Ссылка
    status: int  # ConvertStatus