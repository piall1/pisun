import os
from src.structures import JsonManager


class FileManager:

    json_cont = JsonManager.JsonManager()

    def __init__(self):
        self.file_path = "files"
        self.statuses = {
            'recorded': 1,      # записан, не отправлен
            'in_process': 2,    # в процессе записи, не отправлен
            'send': 3,          # записан, отправлен
            'deleted': 4        # удалён
        }
        self.json_file = "resources/files.json"

    def get_all_files(self):
        files = os.listdir(self.file_path)
        return files

    def check_ready_files(self):
        data = self.json_cont.load_data_json(self.json_file)
        list_ready_files = [key for key, value in data.items() if value == 1]
        return list_ready_files

    def check_deleted_files(self):
        data = self.json_cont.load_data_json(self.json_file)
        list_deleted_files = [key for key, value in data.items() if value == 4]
        return list_deleted_files

    def set_status_record(self, current_file, status):
        data = self.json_cont.load_data_json(self.json_file)
        if current_file not in data:
            data[current_file] = status
        self.json_cont.update_data_json(data, self.json_file)
        print(f"Обновлен статус текущего файла {current_file}: {status}")

    def set_status_deleted(self, list_files):
        if len(list_files) == 0:
            return
        data = self.json_cont.load_data_json(self.json_file)

        for file in list_files:
            data[file] = self.statuses['deleted']
            self.json_cont.update_data_json(data, self.json_file)

    def delete_name_files(self, files_deleted):
        try:
            files_static = self.get_all_files()
            for file_name in files_deleted:
                if file_name == "":
                    continue
                if file_name not in files_static:
                    self.json_cont.delete_kye(file_name, self.json_file)
                    print("Ключи удалены из файла json")
        except Exception as e:
            print(f'Произошла ошибка при удалении файлов: {e}')

    def create_data_json(self):
        if os.path.exists(self.json_file):
            data = self.json_cont.load_data_json(self.json_file)
            files = self.get_all_files()

            for file in files:
                if file not in data:
                    data[file] = self.statuses['recorded']
                    self.json_cont.update_data_json(data, self.json_file)

            for key, value in data.items():
                if value == 2:
                    data[key] = self.statuses['recorded']
                    self.json_cont.update_data_json(data, self.json_file)
        else:
            files = self.get_all_files()
            data = {}

            for file in files:
                data[file] = self.statuses['recorded']
            self.json_cont.update_data_json(data, self.json_file)