import json


class JsonManager:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(JsonManager, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def update_data_json(self, data, json_file):
        with open(json_file, 'w', encoding='utf-8') as f:
            json.dump(data, f, ensure_ascii=False, indent=4)

    def update_val(self, kye, new_val, file_json):
        data = self.load_data_json(file_json)
        data[kye] = new_val
        self.update_data_json(data, file_json)

    def load_data_json(self, json_file):
        with open(json_file, 'r', encoding='utf-8') as f:
            data = json.load(f)
        return data

    def delete_kye(self, kye, file_json):
        data = self.load_data_json(file_json)
        del data[kye]
        self.update_data_json(data, file_json)