from enum import Enum
from dataclasses import dataclass


class PlateNumbers(Enum):
    Unknown = 0
    AFRC = 1
    ANTA = 2
    ARAB = 3
    AUST = 4
    CARB = 5
    COCO = 6
    EURA = 7
    INDI = 8
    NOAM = 9
    NAZC = 10
    PCFC = 11
    SOAM = 12
    JUFU = 13
    PHIL = 14
    RIVR = 15
    SCOT = 16


# print('\nMember name: {}'.format(PlateNumbers.UnknownPlate.name))
# print('Member value: {}'.format(PlateNumbers.UnknownPlate.value))


class ProjectionTypes(Enum):
    UnknownProjectionType     = 0
    TransverseMercator        = 1
    TransverseMercatorSouth   = 2
    LambertConicConformal1SP  = 3
    LambertConicConformal2SP  = 4
    LambertConicConformalWest = 5
    CassiniSoldner            = 6
    ObliqueMercator           = 7
    ObliqueStereographic      = 8
    Mercator                  = 9
    PolarStereographic        = 10
    DoubleStereographic       = 11


class TransformationMethods(Enum):
    StandardSevenParameterApproximation = 0
    StandardSevenParameterStrictFormula = 1
    MolodenskiAbridged                  = 2
    MolodenskiBadekas                   = 3


class HeightResultType(Enum):
    GeometricHeightResult                  = 0
    PhysicalHeightResult_HelmertMolodenski = 1
    PhysicalHeightResult_SourceSystem      = 2
    Reserved                               = 3


class TransformationQuality(Enum):
    UnknownQuality         = 0
    QualityBetterThan21mm  = 1
    Quality21to50mm        = 2
    Quality51to200mm       = 3
    Quality201to500mm      = 4
    Quality501to2000mm     = 5
    Quality2001to5000mm    = 6
    QualityWorseThan5001mm = 7


class UtilizedTransformationMessageIndicator(Enum):
    p1023 = False
    p1024 = False
    p1025 = True
    p1026 = False
    p1027 = False


@dataclass
class packet_1021_raw:
    MessageNumber: int
    SourceName: str
    TargetName: str
    SystemIdentificationNumber: int
    PlateNumber: int
    ComputationIndicator: int
    HeightIndicator: int
    LatitudeofOriginAreaofValidity: float
    LongitudeofOriginAreaofValidity: float
    N_S_ExtensionAreaofValidity: float
    E_W_ExtensionAreaofValidity: float
    TranslationinXdirection: float
    TranslationinYdirection: float
    TranslationinZdirection: float
    RotationAroundtheXaxis: float
    RotationAroundtheYaxis: float
    RotationAroundtheZaxis: float
    ScaleCorrection: float
    As_semi_major_axis: float
    Bs_semi_minor_axis: float
    s_1_div_f: float
    At_semi_major_axis: float
    Bt_semi_minor_axis: float
    t_1_div_f: float
    HorizontalHelmert_MolodenskiQualityIndicator: int
    VerticalHelmert_MolodenskiQualityIndicator: int


@dataclass
class packet_1025_raw:
    MessageNumber: int
    SystemIdentificationNumber: int
    ProjectionType: int
    LatitudeofNaturalOrigin: float
    LongitudeofNaturalOrigin: float
    ScaleFactoratNaturalOrigin: float
    FalseEasting: float
    FalseNorthing: float


class ViewStructure:

    source_ellipsoid = [
        ("Datum", "entryDatum", ""),
        ("SourceName", "entrySourceName", ""),
        ("As_semi_major_axis", "entryAs_semi_major_axis", ""),
        ("s_1_div_f", "entrys_1_div_f", "")
    ]

    target_ellipsoid = [
        ("TargetName", "entryTargetName", ""),
        ("At_semi_major_axis", "entryAt_semi_major_axis", ""),
        ("t_1_div_f", "entryt_1_div_f", "")
    ]

    to_WGS_84 = [
        ("TranslationinX", "entryTranslationinX", ""),
        ("TranslationinY", "entryTranslationinY", ""),
        ("TranslationinZ", "entryTranslationinZ", ""),
        ("RotationAroundtheX", "entryRotationAroundX", ""),
        ("RotationAroundtheY", "entryRotationAroundY", ""),
        ("RotationAroundtheZ", "entryRotationAroundZ", ""),
        ("ScaleCorrection", "entryScaleCorrection", "")
    ]

    projection = [
        ("LatitudeNatural", "entryLatitude_1025", ""),
        ("LongitudeNatural", "entryLongitude_1025", ""),
        ("FalseEasting", "entryFalseEasting_1025", ""),
        ("FalseNorthing", "entryFalseNorthing_1025", ""),
        ("ScaleFactorat", "entryScaleFactorat_1025", "")
    ]


class TypeMapping:

    name_key = {
        "Datum": "Datum",
        "SourceName": "Source Ellipsoid",
        "TargetName": "Target Ellipsoid",
        "ScaleCorrection": "K(ppm)",
        "TranslationinX": "DX",
        "TranslationinY": "DY",
        "TranslationinZ": "DZ",
        "RotationAroundtheX": "RX",
        "RotationAroundtheY": "RY",
        "RotationAroundtheZ": "RZ",
        "As_semi_major_axis": "a",
        "At_semi_major_axis": "a",
        "s_1_div_f": "1/f",
        "t_1_div_f": "1/f",
        "LatitudeNatural": "Origin Lat",
        "LongitudeNatural": "Central meridian",
        "ScaleFactorat": "Scale",
        "FalseEasting": "False Easting",
        "FalseNorthing": "False Northing"
    }

    type_mapping = {
        "Datum": str,
        "SourceName": str,
        "TargetName": str,
        "ScaleCorrection": float,
        "TranslationinX": float,
        "TranslationinY": float,
        "TranslationinZ": float,
        "RotationAroundtheX": float,
        "RotationAroundtheY": float,
        "RotationAroundtheZ": float,
        "As_semi_major_axis": float,
        "At_semi_major_axis": float,
        "s_1_div_f": float,
        "t_1_div_f": float,
        "LatitudeNatural": float,
        "LongitudeNatural": float,
        "ScaleFactorat": float,
        "FalseEasting": float,
        "FalseNorthing": float
    }

    def check_types(self, data):
        for key, value in data.items():
            if key in self.type_mapping:
                expected_type = self.type_mapping[key]
                if not isinstance(value, expected_type):
                    try:
                        data[key] = expected_type(value)  # Попытка преобразовать тип
                    except ValueError:
                        if expected_type == int:
                            raise ValueError(f"Параметр '{self.name_key[key]}' должен быть целым числом.")
                        if expected_type == float:
                            raise ValueError(f"Параметр '{self.name_key[key]}' должен быть числом с запятой.")
                        if expected_type == str:
                            raise ValueError(f"Параметр '{self.name_key[key]}' должен быть строковым значением.")

    def check_type_view(self, value, expected_type, params):
        try:
            if value == "":
                print(f"Заполните параметр: {params}")
            if expected_type == int:
                return int(value)
            elif expected_type == float:
                return float(value)
            elif expected_type == str:
                return str(value)
            else:
                raise ValueError(f"Unsupported type: {expected_type}")
        except ValueError:
            if expected_type == int:
                raise ValueError(f"Параметр '{self.name_key[params]}' должен быть целым числом.")
            if expected_type == float:
                raise ValueError(f"Параметр '{self.name_key[params]}' должен быть числом с запятой.")
            if expected_type == str:
                raise ValueError(f"Параметр '{self.name_key[params]}' должен быть строковым значением.")
