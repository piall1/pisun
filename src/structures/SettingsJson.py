import json
import os
from src.structures.Params import Params

params_values = {
    "coords_input":                 {"type": list,       "default": [0.0, 0.0, 0.0]},
    "tcp_local_port":               {"type": int,        "default": 5200},
    "coords_status":                {"type": str,        "default": "auto",      "values": ("auto", "fix")},
    "coords_format":                {"type": str,        "default": "wgs84",     "values": ("wgs84", "ecef")},
    "rtcm_type":                    {"type": str,        "default": "3.2",       "values": ("3.0", "3.2")},
    "port_name":                    {"type": str,        "default": ""},
    "to_com_port":                  {"type": str,        "default": ""},
    "auto_connect":                 {"type": bool,       "default": False},
    "ntrip_local":                  {"type": bool,       "default": False},
    "tcp_local":                    {"type": bool,       "default": False},
    "ntrip_remote":                 {"type": bool,       "default": False},
    "pps_output":                   {"type": bool,       "default": False},
    "event_input":                  {"type": bool,       "default": False},
    "to_com":                       {"type": bool,       "default": False},
    "hardware_comport1":            {"type": str,        "default": "none",      "values": ("3.0", "3.2", "3.2.radio", "none")},
    "hardware_comport2":            {"type": str,        "default": "none",      "values": ("3.0", "3.2", "3.2.radio", "none")},
    "hardware_comport3":            {"type": str,        "default": "none",      "values": ("3.0", "3.2", "3.2.radio", "none")},
    "hardware_speed1":              {"type": int,        "default": 115200,      "values": (115200, 38400)},
    "hardware_speed2":              {"type": int,        "default": 115200,      "values": (115200, 38400)},
    "hardware_speed3":              {"type": int,        "default": 115200,      "values": (115200, 38400)},
    "hardware_checkbox1":           {"type": bool,       "default": False},
    "hardware_checkbox2":           {"type": bool,       "default": False},
    "hardware_checkbox3":           {"type": bool,       "default": False},
    "max_num_files":                {"type": int,         "default": 200},
    "ntrip_client":                 {"type": bool,        "default": False},
    "hardware_sats_system":         {"type": list,        "default": ["GPS", "GLO", "GAL", "BDS"], "values": ("GPS", "GLO", "GAL", "BDS")},
    "hardware_interval_rtcm":       {"type": int,         "default": 1,          "values": (1, 2, 3, 4, 5)},
    "periodCS_sending":             {"type": int,         "default": 1},
    "statusCS_application":         {"type": bool,        "default": False},
    "caster_host":                  {"type": str,         "default": "pidt.net"},
    "caster_port":                  {"type": str,         "default": "2101"},
    "caster_mount_point":           {"type": str,         "default": "PH"},
    "caster_password":              {"type": str,         "default": "1234"},
    "tcp_remote_host":              {"type": str,         "default": ""},
    "tcp_remote_port":              {"type": int,         "default": 5200},
    "tcp_remote":                   {"type": bool,        "default": False},
    "reset_status":                 {"type": bool,        "default": False},
    "entry_interval_rtcm":          {"type": int,         "default": 1},
    "ftp_host":                     {"type": str,         "default": ""},
    "ftp_port":                     {"type": int,         "default": 21},
    "ftp_login":                    {"type": str,         "default": ""},
    "ftp_password":                 {"type": str,         "default": ""},
    "status_work_ftp":              {"type": bool,        "default": False},
    "work_buzzer":                  {"type": bool,        "default": False}
}


class SettingsJson:

    settings_path = "resources/settings.json"

    def __init__(self):
        self.params = Params(params_values)
        try:
            if not self._check_settings_file(self.settings_path):
                raise Exception("Settings file not found")
            self.read_settings()
        except Exception as e:
            print(f"Не удалось считать настройки из фaйла [{e}] Запись дефолтных значений.")
            self.write_settings()

    ### PUBLIC ###
    def write_settings(self):
        jsonString = json.dumps(self.params.to_dict(), indent=4)
        self._write(jsonString)

    def read_settings(self):
        json_string = self._read()
        decoded_data = json.loads(json_string)
        self.params.update(decoded_data)

    ### PRIVATE ###
    def _write(self, str_data):
        with open(self.settings_path, "w") as file:
            file.write(str_data)

    def _read(self):
        with open(self.settings_path, "r") as file:
            data = file.read()
            return data

    def _check_settings_file(self, path) -> bool:
        if os.path.isfile(path) and os.access(path, os.R_OK):
            return True
        else:
            return False