import os
import time
import subprocess
from src.structures import JsonManager
from src.structures import CommandWifi


class SettingsWifi:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(SettingsWifi, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        self.command = CommandWifi.CommandWifi()
        self.settings_json = JsonManager.JsonManager()
        self.is_connect = None
        self.json_file = "resources/settings_wifi.json"
        self.file_saved_net = "resources/saved_net.json"

    def get_previous_network(self):
        if os.path.exists(self.json_file):

            previous_net = self.settings_json.load_data_json(self.json_file)
            if previous_net["SSID"] is None:
                return "", ""
            else:
                return previous_net["SSID"], previous_net["IpAddress"]
        else:
            print("Имя сети ещё не получено.")
            return "", ""

    def get_save_networks(self):
        new_list_net = []
        saved_networks = []
        list_net = []

        result = subprocess.check_output(self.command.scanning_saved_net(), shell=True, text=True)
        if result is None or len(result) == 0:
            return []

        data_networks = [line.split(":") for line in result.strip().splitlines()]
        for item in data_networks:
            if "wireless" in item[3]:
                saved_networks.append(item[0])

        for item in saved_networks:
            list_net.append({"ssid": item, "ip": ""})

        if os.path.exists(self.json_file):
            networks = self.settings_json.load_data_json(self.file_saved_net)
            if networks is None:
                return list_net

            for net in saved_networks:
                if net not in networks:
                    new_list_net.append({"ssid": net, "ip": ""})
                else:
                    new_list_net.append({"ssid": net, "ip": networks[net]})
            return new_list_net
        else:
            return list_net

    def get_available_networks(self):
        result = subprocess.check_output(self.command.scanning_available_net(), text=True)
        if result is None or len(result) == 0:
            return []
        networks = self.get_networks(result)
        return networks

    def create_hotspot(self):
        saved_net = self.get_save_networks()
        for net in saved_net:
            ssid = net["ssid"]
            if ssid == "Hotspot":
                self.connect_hotspot(ssid)
                print(f"Поднимаем Hotspot")
                return
            continue

        for command in self.command.creations_hotspot():
            result = os.system(command)
            time.sleep(1)

        if result == 0:
            return
        else:
            raise Exception("Не удалось создать точку доступа.")

    def connect_hotspot(self, ssid):
        if ssid is None or ssid == "":
            print("Имя сети не найдено.")
            return

        command = f"sudo nmcli connection up {ssid}"
        result = os.system(command)

        if result == 0:
            return
        else:
            raise Exception("Не удалось поднять Hotspot.")

    def connect_to_saved_net(self, ssid):
        if ssid is None or ssid == "":
            print("Имя сети не найдено.")
            return

        if ssid == "Hotspot":
            self.connect_hotspot(ssid)

        result = os.system(self.command.connections(ssid))
        time.sleep(1)

        if result == 0:
            self.save_ssid_net(ssid)
        else:
            raise Exception(f"Не удалось подключиться к сети {ssid}.")
        return result

    def add_new_network(self, ssid, password):
        self.check_added_network(ssid)

        if ssid is None or ssid == "":
            print("Имя сети не найдено.")
            return

        if password is None or password == "":
            raise Exception("Проверьте пароль!")

        for command in self.command.additions_network(ssid, password):
            result = os.system(command)
            time.sleep(1)

        if result == 0:
            self.save_ssid_net(ssid)
            return
        else:
            raise Exception(f"Не удалось подключиться к сети {ssid}.")

    def delete_network(self, ssid):
        if ssid is None or ssid == "":
            print("Имя сети не найдено.")
            return

        result = os.system(self.command.deletions(ssid))

        if result == 0:
            self.settings_json.delete_kye(ssid, self.file_saved_net)
            return
        else:
            raise Exception(f"Не удалось удалить сеть {ssid}.")

    def check_added_network(self, ssid):
        if ssid is None or ssid == "":
            print("Имя сети не найдено.")
            return

        result = os.system(self.command.profile(ssid))

        if result == 0:
            print(f"Cеть {ssid} уже добавлена.")
            self.delete_network(ssid)
        else:
            print(f"Новая сеть - {ssid}.")
        return result

    def check_saved_network(self, ssid):
        if ssid is None or ssid == "":
            print("Имя сети не найдено")
            return

        result = os.system(self.command.profile(ssid))
        if result == 0:
            print(f"Cеть {ssid} уже сохранена.")
            return True
        else:
            print(f"Новая сеть - {ssid}.")
            return False

    def get_networks(self, scan_result):
        if not scan_result:
            return []

        data_list = []
        for line in scan_result.splitlines():
            if "Address:" in line:
                dict_net = {}
                dict_net["address"] = line.split("Address:")[1].strip()
            if "ESSID:" in line:
                dict_net["ssid"] = line.split("ESSID:")[1].strip('"')
            if "Signal level" in line:
                dict_net["signal"] = line.split("Signal level=")[1].strip()
                data_list.append(dict_net)

        sorted_scan_result = sorted(data_list, key=self.sorted_signal, reverse=True)
        return sorted_scan_result

    def sorted_signal(self, item):
        return int(item['signal'].split()[0])

    def get_ip_address(self, current_net):
        dict_net = {}

        result = subprocess.check_output(self.command.ip_address(current_net), shell=True, text=True)

        for line in result.splitlines():
            if "IP4.ADDRESS[1]:" in line:
                dict_net["IpAddress"] = line.split("IP4.ADDRESS[1]:")[1].strip()
            if "GENERAL.CONNECTION:" in line:
                dict_net["SSID"] = line.split("GENERAL.CONNECTION:")[1].strip()

        return dict_net

    def save_ssid_net(self, current_net):
        try:
            dict_net = self.get_ip_address(current_net)
            self.settings_json.update_data_json(dict_net, self.json_file)
            self.write_saved_net(dict_net)
        except Exception as e:
            print(f"Ошибка при сохранении текущей сети: {e}.")

    def write_saved_net(self, current_net):
        if current_net is None:
            return
        if os.path.exists(self.file_saved_net):
            saved_net = self.settings_json.load_data_json(self.file_saved_net)
            if current_net["SSID"] not in saved_net:
                saved_net[current_net["SSID"]] = current_net["IpAddress"]
            else:
                self.settings_json.delete_kye(current_net["SSID"], self.file_saved_net)
                saved_net[current_net["SSID"]] = current_net["IpAddress"]
            self.settings_json.update_data_json(saved_net, self.file_saved_net)
        else:
            self.settings_json.update_data_json({current_net["SSID"]: current_net["IpAddress"]}, self.file_saved_net)

