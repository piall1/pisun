import tkinter as tk
import ttkbootstrap as ttk
from src.services.Controller import Controller
from src.utils.Console import print
from src.request.AuthorizationClass import AuthorizationClass


class AuthorizationView:
    con = Controller()
    auth = AuthorizationClass()

    def __init__(self):
        self.is_running = False
        self.name_profile = False
        self.icon_output = tk.PhotoImage(file="resources/image/icon_output.png")

    def init_settings(self, frame: ttk.Frame):
        self.frame = frame

        self.label = tk.Label(self.frame, text="Profile:", bg="#e9e9e9")
        self.label.grid(row=0, column=0, padx=5, pady=5, sticky="we", columnspan=1)
        self.entry_profile = ttk.Label(self.frame, bootstyle="dark")
        self.entry_profile.grid(row=0, column=1, padx=5, pady=5, sticky="we")

        self.log_out_btn = ttk.Button(self.frame, image=self.icon_output, bootstyle="light", width=2,  command=self.on_click_log_out)
        self.log_out_btn.grid(row=0, column=2, padx=5, pady=5, sticky="e", columnspan=1)

        self.label_login = tk.Label(self.frame, text="Login", bg="#e9e9e9")
        self.entry_login = ttk.Entry(self.frame, bootstyle="dark")
        self.label_login.grid(row=1, column=0, padx=5, pady=5, sticky="ns")
        self.entry_login.grid(row=1, column=1, padx=5, pady=5, sticky="ns", columnspan=2)

        self.label_pin = tk.Label(self.frame, text="Password", bg="#e9e9e9")
        self.entry_pin = ttk.Entry(self.frame, bootstyle="dark", show="●")
        self.label_pin.grid(row=2, column=0, padx=5, pady=5, sticky="ns")
        self.entry_pin.grid(row=2, column=1, padx=5, pady=5, sticky="ns", columnspan=2)

        self.sig_in_btn = ttk.Button(self.frame, text="Login", bootstyle="dark", command=self.on_click_sig_in)
        self.sig_in_btn.grid(row=3, column=1, padx=5, pady=5, sticky="we", columnspan=2)

        self.set_name_profile()

    def on_click_sig_in(self):
        try:
            telegram_id = self.entry_login.get()
            pin = self.entry_pin.get()
            if self.is_running:
                print("Окно уже открыто")
                self.popup.destroy()
            self.auth.get_authorized(telegram_id, pin)
            self.show_popup_message()
        except Exception as e:
            print("Error ", str(e))

    def on_click_log_out(self):
        self.auth.log_out_of_account()
        print("Logout successful!")
        self.entry_profile.destroy()
        self.entry_profile = ttk.Label(self.frame, bootstyle="dark")
        self.entry_profile.grid(row=0, column=1, padx=5, pady=5, sticky="we")

    def set_name_profile(self):
        self.name_profile = self.auth.get_name_profile()
        self.entry_profile.config(text=self.name_profile)

    def show_popup_message(self):
        self.popup = tk.Toplevel()
        self.popup.title(f"PiSun Cors {self.con.get_firmware_version()}")
        self.popup.iconbitmap("resources/image/Pi_sun_w_1_ico.ico")
        self.popup.update()

        win_width = 250
        win_height = 90
        screen_width = self.popup.winfo_screenwidth()
        screen_height = self.popup.winfo_screenheight()
        x = int((screen_width / 2) - (win_width / 2))
        y = int((screen_height / 2) - (win_height / 2))
        geometry_str = f"{win_width}x{win_height}+{x}+{y}"
        self.popup.geometry(geometry_str)
        self.popup.resizable(False, False)

        self.frm = ttk.Frame(self.popup, borderwidth=1)
        self.frm.grid(row=1, column=1, padx=5, pady=5, sticky="ns", columnspan = 2)

        self.entry_label = ttk.Label(self.frm, text="Enter a code:", bootstyle="dark")
        self.entry_label.grid(row=0, column=0,  padx=5, pady=5, sticky="we")
        self.entry_key = ttk.Entry(self.frm, bootstyle="dark")
        self.entry_key.grid(row=0, column=1, padx=5, pady=5, sticky="we")

        self.submit_button = ttk.Button(self.frm, text="Send", bootstyle="dark",command=self.get_input_pin)
        self.submit_button.grid(row=1, column=1, padx=5, pady=5, sticky="we")

        self.is_running = True

    def get_input_pin(self):
        key = self.entry_key.get()
        self.auth.entry_user_pin(key)
        self.popup.destroy()
        self.set_name_profile()
        print("Authorization was successful!")

    def lock_configures(self, value: bool):
        if value:
            self.log_out_btn.configure(state="disabled")
            self.sig_in_btn.configure(state="disabled")
            self.entry_login.configure(state="disabled")
            self.entry_pin.configure(state="disabled")
        else:
            self.log_out_btn.configure(state="normal")
            self.sig_in_btn.configure(state="normal")
            self.entry_login.configure(state="normal")
            self.entry_pin.configure(state="normal")




