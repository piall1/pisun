import threading
import tkinter as tk
import ttkbootstrap as ttk
from easygui import fileopenbox
from src.structures import ParamsCS
from src.ui import ViewPacked
from src.services import CSController
from src.services import Controller
from src.utils.Console import print
from src.converters import ParceQRCode


class CSView:

    def __init__(self):
        self.import_cs = None
        self.params_dict = None
        self.is_open_window = False
        self.cont = Controller.Controller()
        self.card_view_package = ViewPacked.CSCardView()
        self.cont_cs = CSController.ControllerCS()
        self.type = ParamsCS.TypeMapping()
        self.parce_qr = ParceQRCode.ParceQRCode()

        self.source_ellipsoid = ParamsCS.ViewStructure.source_ellipsoid
        self.target_ellipsoid = ParamsCS.ViewStructure.target_ellipsoid
        self.to_WGS_84 = ParamsCS.ViewStructure.to_WGS_84
        self.projection = ParamsCS.ViewStructure.projection

        self.card_view_package.callback_func_set_params = self.set_settings_package
        self.icon_qr_code = tk.PhotoImage(file="resources/image/icon_qr-code.png")

    def init_settings(self, frame: ttk.Frame):
        self.frame = frame

        label_package = tk.Label(self.frame, text="Select Datum: ", bg="#e9e9e9")
        label_package.grid(row=0, column=0, padx=5, pady=5, sticky="w", columnspan=1)

        self.input_settings = ttk.Button(self.frame, text="New Datum", bootstyle="dark", command=self.on_click_open_form_params)
        self.input_settings.grid(row=1, column=0, padx=5, pady=5, sticky="we", columnspan=1)

        self.open_settings = ttk.Button(self.frame, text="Coord System", bootstyle="dark", command=self.on_click_open_list_package)
        self.open_settings.grid(row=1, column=1, padx=5, pady=5, sticky="we")

        self.combo_speed = ttk.Combobox(self.frame, bootstyle="dark")
        self.combo_speed['values'] = [1, 2, 3, 4, 5]
        self.combo_speed.insert(3, "Interval stream")
        self.combo_speed.grid(row=2, column=0, padx=5, pady=5, sticky="we")
        self.combo_speed.config(width=13, postcommand=self.on_click_combobox_speed)

        self.apply_var = tk.IntVar()
        self.apply_var.set(0)
        self.apply_ckeckbox = ttk.Checkbutton(self.frame, text="Apply", variable=self.apply_var, bootstyle="dark-outline-toolbutton")
        self.apply_ckeckbox.grid(row=2, column=1, padx=5, pady=10, columnspan=1, sticky="we")

    def on_click_open_list_package(self):
        self.card_view_package.show_window()
        self.card_view_package.on_click_update_window()

    def on_click_open_form_params(self):
        if self.is_open_window:
            print("Окно уже открыто")
            return
        self.root = tk.Toplevel()
        self.root.title(f"PiSun Cors {self.cont.get_firmware_version()}")
        self.root.protocol("WM_DELETE_WINDOW", self.on_click_close_window)
        self.root.iconbitmap("resources/image/Pi_sun_w_1_ico.ico")
        self.root.update()

        win_width = 460
        win_height = 315
        screen_width = self.root.winfo_screenwidth()
        screen_height = self.root.winfo_screenheight()
        x = int((screen_width / 2) - (win_width / 2))
        y = int((screen_height / 2) - (win_height / 2))
        geometry_str = f"{win_width}x{win_height}+{x}+{y}"
        self.root.geometry(geometry_str)
        self.root.resizable(False, False)

        self.notebook = ttk.Notebook(self.root, bootstyle="dark")
        self.notebook.grid(row=0, column=0, padx=5, pady=5, columnspan=6, sticky="we")
        self.Frame_notebook_1 = tk.Frame(self.notebook)
        self.Frame_notebook_1.grid(row=0, column=0, padx=5, pady=5, columnspan=1, sticky="we")
        self.Frame_notebook_2 = tk.Frame(self.notebook)
        self.Frame_notebook_2.grid(row=1, column=0, padx=5, pady=5, columnspan=1, sticky="we")
        self.Frame_notebook_3 = tk.Frame(self.notebook)
        self.Frame_notebook_3.grid(row=2, column=1, padx=5, pady=5, columnspan=1, sticky="we")
        self.Frame_notebook_4 = tk.Frame(self.notebook)
        self.Frame_notebook_4.grid(row=3, column=2, padx=5, pady=5, columnspan=1, sticky="we")
        self.Frame_notebook_5 = tk.Frame(self.notebook)
        self.Frame_notebook_5.grid(row=4, column=3, padx=5, pady=5, columnspan=1, sticky="we")

        self.notebook.add(self.Frame_notebook_1, text="Source Ellipsoid")
        self.notebook.add(self.Frame_notebook_2, text="Target Ellipsoid")
        self.notebook.add(self.Frame_notebook_3, text="toWGS84")
        self.notebook.add(self.Frame_notebook_4, text="Projection")
        self.notebook.add(self.Frame_notebook_5, text="Advanced Settings")

        self.open_form_source_ellipsoid()
        self.open_form_target_ellipsoid()
        self.open_form_to_WGS_84()
        self.open_form_projection()
        self.open_form_advanced_settings()

        self.is_open_window = True

    def open_form_source_ellipsoid(self):

        open_qr = ttk.Button(self.Frame_notebook_1, image=self.icon_qr_code, bootstyle="light", command=self.on_click_open_photo)
        open_qr.grid(row=0, column=3, padx=2, pady=5, sticky="we", columnspan=1)

        self.save_button = ttk.Button(self.Frame_notebook_1, text="Create New", bootstyle="dark", command=self.save_settings_package)
        self.save_button.grid(row=5, column=0, padx=5, pady=5, sticky="we")

        self.create_widgets(self.source_ellipsoid, 4, self.Frame_notebook_1)

    def open_form_target_ellipsoid(self):
        self.create_widgets(self.target_ellipsoid, 3, self.Frame_notebook_2)

    def open_form_to_WGS_84(self):
        self.create_widgets(self.to_WGS_84, 7, self.Frame_notebook_3)

    def open_form_projection(self):
        self.create_widgets(self.projection, 5, self.Frame_notebook_4)

    def open_form_advanced_settings(self):

        label_plate = ttk.Label(self.Frame_notebook_5, text="Plate", bootstyle="dark")
        label_plate.grid(row=0, column=0, padx=5, pady=5, sticky="we")
        self.combo_plate = ttk.Combobox(self.Frame_notebook_5, bootstyle="dark")
        self.combo_plate['values'] = self.set_plate_number()
        self.combo_plate.insert(3, ParamsCS.PlateNumbers.EURA.name)
        self.combo_plate.grid(row=0, column=1, padx=5, pady=5, sticky="we")
        self.combo_plate.config(width=35, postcommand=self.on_click_combobox)

        label_computation = ttk.Label(self.Frame_notebook_5, text="Computation Indicator", bootstyle="dark")
        label_computation.grid(row=1, column=0, padx=5, pady=5, sticky="we")
        self.combo_computation = ttk.Combobox(self.Frame_notebook_5, bootstyle="dark")
        self.combo_computation['values'] = self.set_computation_id()
        self.combo_computation.insert(3, ParamsCS.TransformationMethods.StandardSevenParameterStrictFormula.name)
        self.combo_computation.grid(row=1, column=1, padx=5, pady=5, sticky="we")
        self.combo_computation.config(width=35, postcommand=self.on_click_combobox)

        label_height = ttk.Label(self.Frame_notebook_5, text="Height Indicator", bootstyle="dark")
        label_height.grid(row=2, column=0, padx=5, pady=5, sticky="we")
        self.combo_height = ttk.Combobox(self.Frame_notebook_5, bootstyle="dark")
        self.combo_height['values'] = self.set_height_id()
        self.combo_height.insert(3, ParamsCS.HeightResultType.PhysicalHeightResult_SourceSystem.name)
        self.combo_height.grid(row=2, column=1, padx=5, pady=5, sticky="we")
        self.combo_height.config(width=35, postcommand=self.on_click_combobox)

        label_horizontal = ttk.Label(self.Frame_notebook_5, text="Horizontal Helmert", bootstyle="dark")
        label_horizontal.grid(row=3, column=0, padx=5, pady=5, sticky="we")
        self.combo_horizontal = ttk.Combobox(self.Frame_notebook_5, bootstyle="dark")
        self.combo_horizontal['values'] = self.set_horizont_helmert()
        self.combo_horizontal.insert(3, ParamsCS.TransformationQuality.UnknownQuality.name)
        self.combo_horizontal.grid(row=3, column=1, padx=5, pady=5, sticky="we")
        self.combo_horizontal.config(width=35, postcommand=self.on_click_combobox)

        label_vertical = ttk.Label(self.Frame_notebook_5, text="Vertical Helmert", bootstyle="dark")
        label_vertical.grid(row=4, column=0, padx=5, pady=5, sticky="we")
        self.combo_vertical = ttk.Combobox(self.Frame_notebook_5, bootstyle="dark")
        self.combo_vertical['values'] = self.set_vertical_helmert()
        self.combo_vertical.insert(3, ParamsCS.TransformationQuality.UnknownQuality.name)
        self.combo_vertical.grid(row=4, column=1, padx=5, pady=5, sticky="we")
        self.combo_vertical.config(width=35, postcommand=self.on_click_combobox)

        label_projection_type = ttk.Label(self.Frame_notebook_5, text="Projection Type", bootstyle="dark")
        label_projection_type.grid(row=5, column=0, padx=5, pady=5, sticky="we")
        self.combo_projection_type = ttk.Combobox(self.Frame_notebook_5, bootstyle="dark")
        self.combo_projection_type['values'] = self.set_projection_type()
        self.combo_projection_type.insert(3, ParamsCS.ProjectionTypes.TransverseMercator.name)
        self.combo_projection_type.grid(row=5, column=1, padx=5, pady=5, sticky="we")
        self.combo_projection_type.config(width=35)

    def on_click_combobox_speed(self):
        self.combo_speed.state(["readonly"])

    def on_click_combobox(self):
        self.combo_plate.state(["readonly"])
        self.combo_computation.state(["readonly"])
        self.combo_height.state(["readonly"])
        self.combo_horizontal.state(["readonly"])
        self.combo_vertical.state(["readonly"])
        self.combo_projection_type.state(["readonly"])

    def on_click_close_window(self):
        def on_window_closing_async():
            if self.is_open_window:
                self.is_open_window = False
            self.root.destroy()
            return

        closing_thread = threading.Thread(
            target=on_window_closing_async, name="on_window_closing_async"
        )
        closing_thread.start()

    def on_click_open_photo(self):
        try:
            filenames = fileopenbox()
            if filenames is None:
                raise Exception("Файл не выбран")
            self.parce_qr.read_qr_code(filenames)
            if self.parce_qr.data_json is not None:
                self.import_cs = True
                self.set_settings_package()
        except Exception as e:
            print(f"Ошибка при загрузки QR кода: {e}")

    def create_widgets(self, labels_entries_defaults, num, frm):
        for i, (label_text, entry_attr, default_value) in enumerate(labels_entries_defaults):
            row = i % num
            col = (i // num) * 2
            label = ttk.Label(frm, text=self.type.name_key[label_text], style="dark")
            label.grid(row=row, column=col, padx=5, pady=5, sticky="we")
            entry = ttk.Entry(frm, style="dark")
            entry.grid(row=row, column=col + 1, padx=5, pady=5, sticky="we")
            entry.insert(0, default_value)
            setattr(self, entry_attr, entry)

    def save_settings_package(self):
        try:
            type_mapping = ParamsCS.TypeMapping.type_mapping
            settings = {}

            def process_entries(entries):
                for label_text, entry_attr, val in entries:
                    entry_value = getattr(self, entry_attr).get()
                    for key, expected_type in type_mapping.items():
                        if key in label_text:
                            self.type.check_type_view(entry_value, expected_type, key)
                            break
                    settings[label_text] = entry_value

            process_entries(self.source_ellipsoid)
            process_entries(self.target_ellipsoid)
            process_entries(self.to_WGS_84)
            process_entries(self.projection)

            settings["PlateNumber"] = self.get_plate_number()
            settings["ComputationIndicator"] = self.get_computation_id()
            settings["HeightIndicator"] = self.get_height_id()
            settings["HorizontalHelmert"] = self.get_horizont_helmert()
            settings["VerticalHelmert"] = self.get_vertical_helmert()
            settings["ProjectionType"] = self.get_projection_type()

            if all(settings.values()):
                print("Успешное сохранение")
            else:
                print("Ошибка при сохранении: Некоторые значения пустые")

            self.cont_cs.add_coords_system(settings)
            self.card_view_package.on_click_update_window()

        except ValueError as e:
            print(e)

    def edit_settings_package(self):
        try:
            if self.card_view_package.select_id is None:
                raise Exception("Выберите СК для изменений")

            type_mapping = ParamsCS.TypeMapping.type_mapping
            settings = {}

            def process_entries(entries):
                for label_text, entry_attr, val in entries:
                    entry_value = getattr(self, entry_attr).get()
                    for key, expected_type in type_mapping.items():
                        if key in label_text:
                            self.type.check_type_view(entry_value, expected_type, key)
                            break
                    settings[label_text] = entry_value

            process_entries(self.source_ellipsoid)
            process_entries(self.target_ellipsoid)
            process_entries(self.to_WGS_84)
            process_entries(self.projection)

            settings["PlateNumber"] = self.get_plate_number()
            settings["ComputationIndicator"] = self.get_computation_id()
            settings["HeightIndicator"] = self.get_height_id()
            settings["HorizontalHelmert"] = self.get_horizont_helmert()
            settings["VerticalHelmert"] = self.get_vertical_helmert()
            settings["ProjectionType"] = self.get_projection_type()

            if all(settings.values()):
                print("Изменения сохранены")
                self.cont_cs.update_coords_system(self.card_view_package.select_id, settings)
            else:
                print("Ошибка при сохранении: Некоторые значения пустые")

            self.card_view_package.on_click_update_window()

        except Exception as e:
            print(f"Ошибка: {e}")

    def set_settings_package(self):
        self.params_dict = self.card_view_package.get_selected_sc()
        self.cont_cs.get_cs_from_view(self.params_dict)

        if self.import_cs:
            self.params_dict = self.parce_qr.get_params_from_qr()

        label_package = tk.Label(self.frame, text=self.params_dict["Datum"],  anchor="nw", wraplength=120, bg="#e9e9e9")
        label_package.grid(row=0, column=1, padx=5, pady=5, sticky="we", columnspan=1)

        def update_list_with_defaults(list_to_update, params_dict):
            updated_list = []
            for label_text, entry_attr, _ in list_to_update:
                if label_text in params_dict:
                    updated_list.append((label_text, entry_attr, params_dict[label_text]))
                else:
                    updated_list.append((label_text, entry_attr, ""))
            return updated_list

        self.source_ellipsoid = update_list_with_defaults(self.source_ellipsoid, self.params_dict)
        self.target_ellipsoid = update_list_with_defaults(self.target_ellipsoid, self.params_dict)
        self.to_WGS_84 = update_list_with_defaults(self.to_WGS_84, self.params_dict)
        self.projection = update_list_with_defaults(self.projection, self.params_dict)

        if self.is_open_window:
            self.root.destroy()
            self.on_click_open_form_params()
            self.save_button.destroy()

        self.is_open_window = False
        self.on_click_open_form_params()
        self.save_button.destroy()

        self.insert_val_combo()

        self.combo_plate.insert(0, self.params_dict["PlateNumber"])
        self.combo_computation.insert(0, self.params_dict["ComputationIndicator"])
        self.combo_height.insert(0, self.params_dict["HeightIndicator"])
        self.combo_horizontal.insert(0, self.params_dict["HorizontalHelmert"])
        self.combo_vertical.insert(0, self.params_dict["VerticalHelmert"])
        self.combo_projection_type.insert(0, self.params_dict["ProjectionType"])

        edit_button = ttk.Button(self.Frame_notebook_1, text="Save Changes", bootstyle="dark", command=self.edit_settings_package)
        edit_button.grid(row=5, column=0, padx=5, pady=5, sticky="we")

        self.import_cs = False

    def insert_val_combo(self):
        self.clear_combobox()
        self.combo_plate.insert(0, "")
        self.combo_computation.insert(0, "")
        self.combo_height.insert(0, "")
        self.combo_horizontal.insert(0, "")
        self.combo_vertical.insert(0, "")
        self.combo_projection_type.insert(0, "")

    def clear_combobox(self):
        self.combo_plate.delete(0, 'end')
        self.combo_computation.delete(0, 'end')
        self.combo_height.delete(0, 'end')
        self.combo_horizontal.delete(0, 'end')
        self.combo_vertical.delete(0, 'end')
        self.combo_projection_type.delete(0, 'end')

    def get_status_apply(self):
        if self.apply_var.get() == 0:
            return False
        else:
            return True

    def get_stream_interval(self):
        val = self.combo_speed.get()
        try:
            if len(val) == 0:
                raise ValueError("Zero value")
            interval = int(val)
        except Exception:
            interval = 3
        return interval

    def get_plate_number(self):
        val = self.combo_plate.get()
        try:
            if len(val) == 0:
                raise ValueError("Zero value")
            plate_number = str(val)
        except Exception:
            plate_number = "EURA"
        return plate_number

    def get_computation_id(self):
        val = self.combo_computation.get()
        try:
            if len(val) == 0:
                raise ValueError("Zero value")
            self.computation_id = str(val)
        except Exception:
            self.computation_id = "StandardSevenParameterStrictFormula"
        return self.computation_id
    
    def get_height_id(self):
        val = self.combo_height.get()
        try:
            if len(val) == 0:
                raise ValueError("Zero value")
            self.height_id = str(val)
        except Exception:
            self.height_id = "PhysicalHeightResult_SourceSystem"
        return self.height_id

    def get_horizont_helmert(self):
        val = self.combo_horizontal.get()
        try:
            if len(val) == 0:
                raise ValueError("Zero value")
            self.horizont_helmert = str(val)
        except Exception:
            self.horizont_helmert = "UnknownQuality"
        return self.horizont_helmert
    
    def get_vertical_helmert(self):
        val = self.combo_vertical.get()
        try:
            if len(val) == 0:
                raise ValueError("Zero value")
            self.vertical_helmert = str(val)
        except Exception:
            self.vertical_helmert = "UnknownQuality"
        return self.vertical_helmert

    def get_projection_type(self):
        val = self.combo_projection_type.get()
        try:
            if len(val) == 0:
                raise ValueError("Zero value")
            self.projection_type = str(val)
        except Exception:
            self.projection_type = "TransverseMercator"
        return self.projection_type

    def set_plate_number(self):
        list_plate = []
        for val in ParamsCS.PlateNumbers:
            list_plate.append(val.name)
        return list_plate

    def set_computation_id(self):
        list_plate = []
        for val in ParamsCS.TransformationMethods:
            list_plate.append(val.name)
        return list_plate

    def set_height_id(self):
        list_plate = []
        for val in ParamsCS.HeightResultType:
            list_plate.append(val.name)
        return list_plate

    def set_horizont_helmert(self):
        list_plate = []
        for val in ParamsCS.TransformationQuality:
            list_plate.append(val.name)
        return list_plate

    def set_vertical_helmert(self):
        list_plate = []
        for val in ParamsCS.TransformationQuality:
            list_plate.append(val.name)
        return list_plate

    def set_projection_type(self):
        list_plate = []
        for val in ParamsCS.ProjectionTypes:
            list_plate.append(val.name)
        return list_plate

    def lock_configures(self, value: bool):
        if value:
            self.input_settings.configure(state="disabled")
            self.open_settings.configure(state="disabled")
            self.combo_speed.configure(state="disabled")
            self.apply_ckeckbox.configure(state="disabled")
        else:
            self.input_settings.configure(state="normal")
            self.open_settings.configure(state="normal")
            self.combo_speed.configure(state="normal")
            self.apply_ckeckbox.configure(state="normal")