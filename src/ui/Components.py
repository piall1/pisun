import threading
import tkinter as tk
import ttkbootstrap as ttk
from src.services        import Controller
from src.converters      import CoordСonverter
from src.ui              import Plotter
from src.ui              import ComponentsSettings
from src.ui              import ComponentsCoords
from src.ui              import ComponentsNTRIP
from src.ui              import ScrollbarFrame
from src.ui              import PointsSettings
from src.ui              import RoverSettings
from src.ui              import AuthorizationView
from src.ui              import ConverterCNBView
from src.ui              import CSView
from src.ui              import FTPCompoents


class Components:
    is_running = False
    coords_format = None
    connect_thread = True
    update_thread = None
    open_convert = None
    start_processing_status = False
    event = threading.Event()

    def __init__(self):
        self.cont = Controller.Controller()
        self.root = tk.Tk()
        self.root.title(f"PiSun Cors {self.cont.get_firmware_version()}")
        self.root.iconbitmap("resources/image/Pi_sun_w_1_ico.ico")
        self.root.protocol("WM_DELETE_WINDOW", self.on_window_closing)
        self.root.geometry("737x930+0+0")
        self.root.update()
        self.root.height = None
        self.root.width = 737
        self.root.resizable(False, True)

        self.plotter = Plotter.Plotter()
        self.additional_settings = ComponentsSettings.ComponentsSettings()
        self.settings_coords = ComponentsCoords.CoordsSettings()
        self.settings_ntrip = ComponentsNTRIP.ComponentsNtrip()
        self.obj_scrollbar = ScrollbarFrame.ScrollableFrame(self.root, height=930, width=715)
        self.convert = CoordСonverter.CoordConverter()
        self.points_settings = PointsSettings.PointsSettings()
        self.rtk_settings = RoverSettings.RoverSettings()
        self.auth_settings = AuthorizationView.AuthorizationView()
        self.converter_cnb = ConverterCNBView.ConverterCNBView()
        self.cs_view = CSView.CSView()
        self.ftp_server = FTPCompoents.FTPComponents()

        self.settings_ntrip.get_output_ports = self.cont.get_ports
        self.settings_coords.update_coords_callback = self.update_coords
        self.settings_coords.on_click_open_list = self.points_settings.on_click_open_window
        self.points_settings.get_list_points = self.cont.get_list_points
        self.points_settings.get_point_callback = self.input_point
        self.settings_coords.update_point = self.points_settings.update_save_point
        # self.ftp_server.connect_callback = self.connect_ftp

        self.scr = self.obj_scrollbar.frame
        self.Frame_logo = tk.Frame(self.scr)
        self.Frame_logo.grid(row=0, column=0, padx=5, pady=5, columnspan=6, sticky="ns")
        self.Frame_btn_con_auto = tk.Frame(self.scr)
        self.Frame_btn_con_auto.grid(row=1, column=1, padx=5, pady=5, sticky="ns")
        self.Frame_settings = tk.Frame(self.scr)
        self.Frame_settings.grid(row=2, column=0, padx=5, pady=5, columnspan=2, sticky="ns")
        self.Frame_btn = tk.Frame(self.scr)
        self.Frame_btn.grid(row=3, column=1, padx=100, pady=7,  sticky="w")
        self.Frame_status = tk.Frame(self.scr)
        self.Frame_status.grid(row=4, column=0, padx=5, pady=5, columnspan=2, sticky="ns")

        self.notebook = ttk.Notebook(self.Frame_settings, bootstyle="dark")
        self.notebook.grid(row=0, column=0, padx=5, pady=5, columnspan=6, sticky="we")
        self.Frame_notebook_1 = tk.Frame(self.notebook)
        self.Frame_notebook_1.grid(row=0, column=0, padx=5, pady=5, columnspan=1, sticky="we")
        self.Frame_notebook_2 = tk.Frame(self.notebook)
        self.Frame_notebook_2.grid(row=0, column=1, padx=5, pady=5, columnspan=1, sticky="we")
        self.Frame_notebook_4 = tk.Frame(self.notebook)
        self.Frame_notebook_4.grid(row=0, column=2, padx=5, pady=5, columnspan=1, sticky="we")
        self.Frame_notebook_5 = tk.Frame(self.notebook)
        self.Frame_notebook_5.grid(row=0, column=3, padx=5, pady=5, columnspan=1, sticky="we")
        self.Frame_notebook_6 = tk.Frame(self.notebook)
        self.Frame_notebook_6.grid(row=0, column=4, padx=5, pady=5, columnspan=1, sticky="we")

        self.Frame_coords_settings = tk.Frame(self.Frame_notebook_1)
        self.Frame_coords_settings.grid(row=0, column=0, padx=5, pady=1, columnspan=1, sticky="we")
        self.settings_coords.init_settings(self.Frame_coords_settings)
        self.Frame_ntrip_settings = tk.Frame(self.Frame_notebook_1)
        self.Frame_ntrip_settings.grid(row=0, column=1, padx=5, pady=1, columnspan=1, sticky="we")
        self.settings_ntrip.init_settings(self.Frame_ntrip_settings)

        self.Frame_additional_settings = tk.Frame(self.Frame_notebook_2)
        self.Frame_additional_settings.grid(row=0, column=0, padx=1, pady=5, columnspan=1, sticky="we")
        self.additional_settings.init_settings(self.Frame_additional_settings)

        self.Frame_auth_settings = tk.Frame(self.Frame_notebook_4)
        self.Frame_auth_settings.grid(row=0, column=0, padx=1, pady=5, columnspan=1, sticky="ns")
        self.auth_settings.init_settings(self.Frame_auth_settings)

        self.Frame_cs_settings = tk.Frame(self.Frame_notebook_5)
        self.Frame_cs_settings.grid(row=0, column=0, padx=1, pady=5, columnspan=1, sticky="ns")
        self.cs_view.init_settings(self.Frame_cs_settings)

        self.Frame_ftp_settings = tk.Frame(self.Frame_notebook_6)
        self.Frame_ftp_settings.grid(row=0, column=0, padx=1, pady=5, columnspan=1, sticky="ns")
        self.ftp_server.init_settings(self.Frame_ftp_settings)

        self.notebook.add(self.Frame_notebook_1, text="Settings")
        self.notebook.add(self.Frame_notebook_2, text="Additional Settings")
        self.notebook.add(self.Frame_notebook_4, text="Authorization")
        self.notebook.add(self.Frame_notebook_5, text="Coord System")
        self.notebook.add(self.Frame_notebook_6, text="FTP-Server")

        # LogoLabel
        self.logo = tk.PhotoImage(file="resources/image/Pi_sun_w_2_long.png").subsample(3, 3)
        self.logo_label = tk.Label(self.Frame_logo, image=self.logo)
        self.logo_label.grid(row=0, column=0, padx=5, pady=5, columnspan=2, sticky="we")

        # ComboBox
        self.combo = ttk.Combobox(self.Frame_btn_con_auto, bootstyle="dark", width=45)
        self.combo.config(postcommand=self.on_click_port_combobox)
        self.combo.grid(row=0, column=0, padx=5, pady=5, sticky="e")
        self.combo.func = None

        # ButtonConnect/Disconnect
        self.connect_button = ttk.Button(self.Frame_btn_con_auto, text="Connect", bootstyle="dark", command=self.connect_button_thread, width=15)
        self.connect_button.grid(row=0, column=1, padx=5, pady=5, sticky="we")
        self.connect_button.callback = None

        self.auto_connect_var = tk.IntVar()
        self.auto_connect_var.set(0)
        self.status_connection = "ON"
        self.auto_connect_ckeckbox = ttk.Checkbutton(self.Frame_btn_con_auto, text="Auto connection", variable=self.auto_connect_var,bootstyle="dark-toolbutton")
        self.auto_connect_ckeckbox.grid(row=0, column=2, padx=5, pady=5, sticky="we")

        # Load_Button
        self.connect_load = ttk.Button(self.Frame_btn, text="Load", width=15, command=self.on_click_load_button, bootstyle="dark")
        self.connect_load.grid(row=0, column=0, padx=5, pady=5, sticky="wn")
        self.connect_load.configure(width=13)

        # Save_Button
        self.connect_save = ttk.Button(self.Frame_btn, text="Save", width=15, command=self.on_click_save_button, bootstyle="dark")
        self.connect_save.grid(row=0, column=1, padx=5, pady=5, sticky="wn")
        self.connect_save.configure(width=13)

        # Satellites_btn
        self.btn_trek = ttk.Button(self.Frame_btn, text="Satellites", width=15, command=self.on_click_btn_satallites, bootstyle="dark")
        self.btn_trek.grid(row=0, column=2, padx=5, pady=5, sticky="wn")
        self.btn_trek.configure(width=13)

        # Converter_btn
        self.btn_convert = ttk.Button(self.Frame_btn, text="Converter CNB", width=15, command=self.on_click_open_convert,bootstyle="dark")
        self.btn_convert.grid(row=0, column=3, padx=5, pady=5, sticky="wn")
        self.btn_convert.configure(width=13)

        # NtripStatus
        self.status = tk.Label(self.Frame_status, text="Ntrip status", bg="#e9e9e9")
        self.ntrip_status = tk.Text(self.Frame_status, height=2)
        self.ntrip_status.config(borderwidth=2, highlightthickness=2)
        self.Frame_status.grid_rowconfigure(5, minsize=25)
        self.status.grid(row=3, column=1, sticky="e")
        self.ntrip_status.grid(row=3, column=2, padx=5, pady=5, columnspan=5, sticky="we")

        self.static = tk.Label(self.Frame_status, text="RAW file", bg="#e9e9e9")
        self.static_file = ttk.Text(self.Frame_status, height=1)
        self.static_file.config(borderwidth=2, highlightthickness=2)
        self.static.grid(row=4, column=1, sticky="e")
        self.static_file.grid(row=4, column=2, padx=5, pady=5, columnspan=5, sticky="we")

        # Автоматические координаты с чипа
        self.auto_label1 = tk.Label(self.Frame_status, text="Lat", bg="#e9e9e9")
        self.auto_coords1 = ttk.Text(self.Frame_status, height=1, width=15)
        self.auto_coords1.config(borderwidth=2, highlightthickness=2)
        self.auto_label1.grid(row=5, column=1, sticky="e")
        self.auto_coords1.grid(row=5, column=2, padx=5, pady=5, sticky="we")

        self.auto_label2 = tk.Label(self.Frame_status, text="Lon", bg="#e9e9e9")
        self.auto_coords2 = ttk.Text(self.Frame_status, height=1, width=15)
        self.auto_coords2.config(borderwidth=2, highlightthickness=2)
        self.auto_label2.grid(row=5, column=3, sticky="e")
        self.auto_coords2.grid(row=5, column=4, padx=5, pady=5, sticky="we")

        self.auto_label3 = tk.Label(self.Frame_status, text="H", bg="#e9e9e9")
        self.auto_coords3 = ttk.Text(self.Frame_status, height=1, width=15)
        self.auto_coords3.config(borderwidth=2, highlightthickness=2)
        self.auto_label3.grid(row=5, column=5, sticky="e")
        self.auto_coords3.grid(row=5, column=6, padx=5, pady=5, sticky="we")

        # LabelSerial
        self.serial_label = tk.Label(self.Frame_status, text="Serial", bg="#e9e9e9")
        self.serial_entry = ttk.Text(self.Frame_status, height=1, width=15)
        self.serial_entry.config(borderwidth=2, highlightthickness=2)
        self.serial_label.grid(row=8, column=1, sticky="e")
        self.serial_entry.grid(row=8, column=2, padx=5, pady=5, sticky="we")

        # LabelSats
        self.sats_label = tk.Label(self.Frame_status, text="Sats", bg="#e9e9e9")
        self.sats_entry = ttk.Text(self.Frame_status, height=1, width=15)
        self.sats_entry.config(borderwidth=2, highlightthickness=2)
        self.sats_label.grid(row=8, column=3, sticky="e")
        self.sats_entry.grid(row=8, column=4, padx=5, pady=5, sticky="we")

        # LabelFix
        self.fix_label = tk.Label(self.Frame_status, text="Fix", bg="#e9e9e9")
        self.fix_entry = ttk.Text(self.Frame_status, height=1, width=15)
        self.fix_entry.config(borderwidth=2, highlightthickness=2)
        self.fix_label.grid(row=8, column=5, sticky="e")
        self.fix_entry.grid(row=8, column=6, padx=5, pady=5, sticky="we")

        # Console
        self.console_label = tk.Label(self.Frame_status, text="Console", bg="#e9e9e9")
        self.console = tk.Text(self.Frame_status, height=10)
        self.console.config(borderwidth=2, highlightthickness=2)
        self.Frame_status.grid_rowconfigure(9, minsize=25)
        self.console_label.grid(row=10, column=0, padx=5, pady=1, sticky="we", columnspan=7)
        self.console.grid(row=11, column=0, padx=5, pady=10, sticky="we", columnspan=7)

    def get_params(self):
        params_val = {}
        params_val["port_name"] = self.combo.get()
        params_val["to_com_port"] = self.settings_ntrip.get_output_port()
        params_val["to_com"] = self.settings_ntrip.get_output_checkbox()
        params_val["event_input"] = self.additional_settings.get_event_var()
        params_val["pps_output"] = self.additional_settings.get_pps_var()
        params_val["tcp_local_port"] = self.settings_ntrip.get_tcp_local_port()
        params_val["tcp_local"] = self.settings_ntrip.get_var_tcp_checkbox()
        params_val["coords_status"] = self.settings_coords.get_coords_status()
        params_val["auto_connect"] = self.get_var_auto_connect()
        params_val["coords_format"] = self.settings_coords.get_coords_format()
        params_val["rtcm_type"] = self.settings_coords.get_rtcm_type()
        params_val["ntrip_local"] = self.settings_ntrip.get_var_local_ntrip()
        params_val["ntrip_remote"] = self.settings_ntrip.get_var_remote_ntrip()
        params_val["coords_input"] = self.settings_coords.get_input_coords()
        params_val["max_num_files"] = self.additional_settings.get_max_files()
        params_val["entry_interval_rtcm"] = self.settings_coords.get_entry_rtcm_interval()

        params_val["hardware_comport1"] = self.additional_settings.get_combo_format_com1()
        params_val["hardware_comport2"] = self.additional_settings.get_combo_format_com2()
        params_val["hardware_comport3"] = self.additional_settings.get_combo_format_com3()
        params_val["hardware_checkbox1"] = self.additional_settings.get_hardware1_checkbox()
        params_val["hardware_checkbox2"] = self.additional_settings.get_hardware2_checkbox()
        params_val["hardware_checkbox3"] = self.additional_settings.get_hardware3_checkbox()
        params_val["hardware_speed1"] = self.additional_settings.get_combo_speed_com1()
        params_val["hardware_speed2"] = self.additional_settings.get_combo_speed_com2()
        params_val["hardware_speed3"] = self.additional_settings.get_combo_speed_com3()
        params_val["hardware_sats_system"] = self.additional_settings.get_satellite_constellations()
        params_val["hardware_interval_rtcm"] = self.additional_settings.get_interval_list()

        params_val["periodCS_sending"] = self.cs_view.get_stream_interval()
        params_val["statusCS_application"] = self.cs_view.get_status_apply()

        params_val["caster_host"] = self.settings_ntrip.get_htrip_host()
        params_val["caster_port"] = self.settings_ntrip.get_ntrip_port()
        params_val["caster_password"] = self.settings_ntrip.get_ntrip_pwd()
        params_val["caster_mount_point"] = self.settings_ntrip.get_ntrip_mountpoint()

        params_val["tcp_remote"] = self.settings_ntrip.get_tcp_remote()
        params_val["tcp_remote_host"] = self.settings_ntrip.get_tcp_remote_host()
        params_val["tcp_remote_port"] = self.settings_ntrip.get_tcp_remote_port()

        params_val["ftp_host"] = self.ftp_server.get_host()
        params_val["ftp_port"] = self.ftp_server.get_port()
        params_val["ftp_login"] = self.ftp_server.get_login()
        params_val["ftp_password"] = self.ftp_server.get_password()
        params_val["status_work_ftp"] = self.ftp_server.get_status_work_ftp()

        return params_val

    def set_params(self, params_val):
        self.set_combobox_port(params_val["port_name"])
        self.settings_ntrip.set_combobox_output_port(params_val["to_com_port"])
        self.settings_ntrip.set_val_checkbox_output_port(params_val["to_com"])
        self.additional_settings.set_val_event_input(params_val["event_input"])
        self.additional_settings.set_val_pps_output(params_val["pps_output"])
        self.settings_ntrip.set_tcp_port_entry(params_val["tcp_local_port"])
        self.settings_ntrip.set_val_tcp_checkbox(params_val["tcp_local"])
        self.settings_coords.set_coords_status(params_val["coords_status"])
        self.set_val_auto_connect(params_val["auto_connect"])
        self.settings_coords.set_coords_format(params_val["coords_format"])
        self.settings_coords.set_rtcm_type(params_val["rtcm_type"])
        self.settings_ntrip.set_val_ntrip_local(params_val["ntrip_local"])
        self.settings_ntrip.set_val_ntrip_remote(params_val["ntrip_remote"])
        self.settings_coords.set_input_coords(params_val["coords_input"])
        self.additional_settings.set_max_files(params_val["max_num_files"])
        self.settings_coords.set_entry_rtcm_interval(params_val["entry_interval_rtcm"])

        self.additional_settings.set_combo_format_com1(params_val["hardware_comport1"])
        self.additional_settings.set_combo_format_com2(params_val["hardware_comport2"])
        self.additional_settings.set_combo_format_com3(params_val["hardware_comport3"])
        self.additional_settings.set_hardware1_checkbox(params_val["hardware_checkbox1"])
        self.additional_settings.set_hardware2_checkbox(params_val["hardware_checkbox2"])
        self.additional_settings.set_hardware3_checkbox(params_val["hardware_checkbox3"])
        self.additional_settings.set_combo_speed_com1(params_val["hardware_speed1"])
        self.additional_settings.set_combo_speed_com2(params_val["hardware_speed2"])
        self.additional_settings.set_combo_speed_com3(params_val["hardware_speed3"])
        self.additional_settings.set_satellite_constellations(params_val["hardware_sats_system"])
        self.additional_settings.set_interval_list(params_val["hardware_interval_rtcm"])

        self.settings_ntrip.set_ntrip_host(params_val["caster_host"])
        self.settings_ntrip.set_ntrip_port(params_val["caster_port"])
        self.settings_ntrip.set_ntrip_mount(params_val["caster_mount_point"])
        self.settings_ntrip.set_ntrip_pwd(params_val["caster_password"])

        self.settings_ntrip.set_tcp_remote(params_val["tcp_remote"])
        self.settings_ntrip.set_tcp_remote_host(params_val["tcp_remote_host"])
        self.settings_ntrip.set_tcp_remote_port(params_val["tcp_remote_port"])

        self.ftp_server.set_status_work_ftp(params_val["status_work_ftp"])
        self.ftp_server.set_host(params_val["ftp_host"])
        self.ftp_server.set_port(params_val["ftp_port"])
        self.ftp_server.set_login(params_val["ftp_login"])
        self.ftp_server.set_pwd(params_val["ftp_password"])

    def set_combobox_port(self, port_name):
        self.combo.set(port_name)

    def set_coords_status(self, coords_status):
        coords = coords_status
        self.auto_coords1.config(state="normal")
        self.auto_coords2.config(state="normal")
        self.auto_coords3.config(state="normal")
        self.coords_format = self.settings_coords.get_coords_format()
        if self.coords_format == "ecef":
            coords = self.convert.convert_wgs_to_ecef(coords[0], coords[1], coords[2])
        self.auto_coords1.delete(1.0, "end")
        self.auto_coords1.insert(1.0, coords[0])
        self.auto_coords1.config(state="disabled")
        self.auto_coords2.delete(1.0, "end")
        self.auto_coords2.insert(1.0, coords[1])
        self.auto_coords2.config(state="disabled")
        self.auto_coords3.delete(1.0, "end")
        self.auto_coords3.insert(1.0, coords[2])
        self.auto_coords3.config(state="disabled")

    def set_value_serial(self, serial):
        self.serial_entry.config(state="normal")
        self.serial_entry.delete(1.0, "end")
        self.serial_entry.insert(1.0, serial)
        self.serial_entry.config(state="disabled")

    def set_value_console(self, message):
        self.console.config(state="normal")
        self.console.insert("end", message)
        self.console.see("end")
        text = self.console.get(1.0, "end")
        if len(text) > 2000:
            self.console.delete(1.0, "end-2000c")
            self.console.see("end")
        self.console.config(state="disabled")

    def set_val_auto_connect(self, val: bool):
        self.auto_connect_var.set(0) if not val else self.auto_connect_var.set(1)

    def get_var_auto_connect(self):
        return False if self.auto_connect_var.get() == 0 else True

    def lock_configures(self, state):
        self.settings_coords.lock_configures(state)
        self.additional_settings.lock_configures(state)
        self.settings_ntrip.lock_configures(state)
        self.auth_settings.lock_configures(state)
        self.cs_view.lock_configures(state)
        if state:
            self.auto_connect_ckeckbox.configure(state="disabled")
            self.connect_load.configure(state="disabled")
            self.connect_save.configure(state="disabled")
        else:
            self.auto_connect_ckeckbox.configure(state="normal")
            self.connect_load.configure(state="normal")
            self.connect_save.configure(state="normal")

    def clear_configs(self):
        self.sats_entry.config(state="normal")
        self.sats_entry.delete(1.0, "end")
        self.fix_entry.config(state="normal")
        self.fix_entry.delete(1.0, "end")
        self.ntrip_status.config(state="normal")
        self.ntrip_status.delete(1.0, "end")
        self.static_file.config(state="normal")
        self.static_file.delete(1.0, "end")
        self.serial_entry.config(state="normal")
        self.serial_entry.delete(1.0, "end")

    def update_coords(self, current_format, selected_format):
        coords_current = self.cont.get_current_coords()
        self.set_coords_status(coords_current)
        if current_format == "wgs84":
            self.auto_label1.config(text="X")
            self.auto_label2.config(text="Y")
            self.auto_label3.config(text="Z")
        else:
            self.auto_label1.config(text="Lat")
            self.auto_label2.config(text="Lon")
            self.auto_label3.config(text="H")

    def input_point(self):
        point_coords, point_name = self.points_settings.get_select_points()
        self.settings_coords.set_input_coords(point_coords)
        self.settings_coords.set_name_points(point_name)
        self.settings_coords.set_auto_btn()

    def on_click_save_button(self):
        params_value = self.get_params()
        self.cont.save_params(params_value)

    def on_click_load_button(self):
        params_dict = self.cont.load_params()
        self.set_params(params_dict)

    def on_click_port_combobox(self):
        ports = self.cont.get_ports()
        self.combo.config(values=ports)
        self.combo.state(["readonly"])

    def on_click_btn_satallites(self):
        self.plotter.start_plotter(self.root, self.cont.get_plot_data)

    def on_click_open_convert(self):
        self.converter_cnb.start_converter(self.root)

    def update_status_thread(self):
        while self.connect_thread:

            self.sats_entry.config(state="normal")
            self.fix_entry.config(state="normal")
            self.ntrip_status.config(state="normal")
            self.static_file.config(state="normal")

            sats = self.cont.get_sats_num()
            fix_status = self.cont.get_fix_status()
            ntrip_status = self.cont.get_ntrip_status()
            record_file = self.cont.get_file_status()
            coords_current = self.cont.get_current_coords()
            serial = self.cont.get_serial()

            self.sats_entry.delete(1.0, "end")
            self.sats_entry.insert(1.0, sats)
            self.sats_entry.config(state="disabled")

            self.fix_entry.delete(1.0, "end")
            self.fix_entry.insert(1.0, fix_status)
            self.fix_entry.config(state="disabled")

            self.ntrip_status.delete(1.0, "end")
            self.ntrip_status.insert(1.0, ntrip_status)
            self.ntrip_status.config(state="disabled")

            self.static_file.delete(1.0, "end")
            self.static_file.insert(1.0, record_file)
            self.static_file.config(state="disabled")

            self.set_coords_status(coords_current)
            self.set_value_serial(serial)
            self.event.wait(timeout=3)
            self.event.clear()

    def connect_button_thread(self):
        threading.Thread(target=self.set_connect_callback, name="connect_callback").start()

    def set_connect_callback(self):
        if self.start_processing_status:
            print(f"Подождите!\n")
            return
        self.start_processing_status = True
        self.lock_configures(True)
        button_status = self.connect_button.cget("text")
        try:
            if button_status == "Connect":
                self.connect_button.config(text="Disconnect")
                self.is_running = True
                self.lock_configures(True)
                params_values = self.get_params()
                self.cont.start_controller(params_values)
                self.connect_thread = True
                self.update_thread = threading.Thread(target=self.update_status_thread, name="Form Updater")
                self.update_thread.start()
            else:
                self.connect_button.config(text="Connect")
                self.is_running = False
                self.lock_configures(False)
                self.connect_thread = False
                self.event.set()
                self.cont.stop_controller()
                self.clear_configs()

        except Exception as e:
            print(f"Ошибка {e}")
            self.connect_button.config(text="Connect")
            self.is_running = False
            self.lock_configures(False)
            self.connect_thread = False
            self.event.set()
            self.cont.stop_controller()
            self.clear_configs()

        self.start_processing_status = False
        return

    def on_window_closing(self):
        def on_window_closing_async():
            self.connect_thread = False
            self.event.set()
            self.plotter.stop_plotter()
            self.cont.stop_controller()
            self.root.quit()
            return

        closing_thread = threading.Thread(
            target=on_window_closing_async, name="on_window_closing_async"
        )
        closing_thread.start()
