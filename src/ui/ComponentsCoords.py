import tkinter as tk
import ttkbootstrap as ttk
from src.converters import CoordСonverter
from src.services import Controller
from src.utils.Console import print


class CoordsSettings:
    is_running = False
    update_coords_callback = None
    open_points = None
    coords_input = [0.0, 0.0, 0.0]
    convert = CoordСonverter.CoordConverter()
    cont = Controller.Controller()

    def __init__(self):
        self.point_name = None
        self.update_point = None
        self.interval_rtcm_var = tk.IntVar()
        self.interval_rtcm_var.set(1)

    def init_settings(self, frame: ttk.Frame):
        self.frame = frame
        self.is_running = True

        self.frame_format = ttk.Frame(self.frame)
        self.frame_format.grid(row=0, column=0, padx=5, pady=5, sticky="we")

        self.frame_rtcm = ttk.Frame(self.frame)
        self.frame_rtcm.grid(row=1, column=0, padx=5, pady=5, sticky="we")

        self.frame_coords = ttk.Frame(self.frame)
        self.frame_coords.grid(row=0, column=1, padx=5, pady=5, sticky="we", rowspan=2)

        # Coord_Format
        self.format_label = tk.Label(self.frame_format, text="Coord format", bg="#e9e9e9")
        self.format_label.grid(row=0, column=0, padx=5, pady=5, sticky="we")
        self.selected_format = tk.StringVar()
        self.radiobutton_ecef = ttk.Radiobutton(self.frame_format, text="ecef    ", variable=self.selected_format, value="ecef", command=self.on_click_coords_format_radiobtn, bootstyle="dark", )
        self.radiobutton_wgs84 = ttk.Radiobutton(self.frame_format, text="wgs84", variable=self.selected_format, value="wgs84", command=self.on_click_coords_format_radiobtn, bootstyle="dark")
        self.radiobutton_ecef.grid(row=1, column=0, padx=5, pady=5, sticky="e")
        self.radiobutton_wgs84.grid(row=2, column=0, padx=5, pady=5, sticky="e")

        self.selected_format.set("wgs84")
        self.current_format = "wgs84"

        #RTCM btn

        self.label_rtcm = tk.Label(self.frame_rtcm, text="RTCM version")
        self.label_rtcm.grid(row=0, column=0, padx=10, pady=1, sticky="we")

        self.rtcm_values = tk.StringVar()
        self.rtcm_values.set("3.2")

        self.rtcm_1 = ttk.Radiobutton(self.frame_rtcm, text="RTCM 3.0", variable=self.rtcm_values, value="3.0", bootstyle="dark")
        self.rtcm_2 = ttk.Radiobutton(self.frame_rtcm, text="RTCM 3.2", variable=self.rtcm_values, value="3.2", bootstyle="dark")

        self.rtcm_1.grid(row=1, column=0, padx=5, pady=5, sticky="e")
        self.rtcm_2.grid(row=2, column=0, padx=5, pady=5, sticky="e")

        self.lb_interval = tk.Label(self.frame_rtcm, text="Interval RTCM", bg="#e9e9e9")
        self.lb_interval.grid(row=3, column=0, padx=1, pady=5, sticky="we")
        self.interval_rtcm = ttk.Entry(self.frame_rtcm, bootstyle="dark", textvariable=self.interval_rtcm_var)
        self.interval_rtcm.grid(row=3, column=1, padx=1, pady=5, sticky="we")
        self.interval_rtcm.configure(width=2)

        #Auto_Button
        self.fix_auto_var = tk.IntVar()
        self.fix_auto_var.set(1)
        self.check_auto = ttk.Checkbutton(self.frame_coords, text="Auto", variable=self.fix_auto_var, bootstyle="dark-round-toggle")
        self.check_auto.grid(row=0, column=2, padx=10, pady=5, sticky="w")

        self.point_name = tk.Label(self.frame_coords, text="Name:")
        self.point_name.grid(row=0, column=3, padx=1, pady=5, sticky="e")
        self.point = ttk.Entry(self.frame_coords, bootstyle="dark")
        self.point.grid(row=0, column=4, padx=1, pady=5, sticky="we")
        self.point.configure(width=1)

        # CoordsLabels
        self.coord1 = tk.Label(self.frame_coords, text="Lat", bg="#e9e9e9")
        self.label1 = ttk.Entry(self.frame_coords, bootstyle="dark")
        self.coord1.grid(row=1, column=1, padx=5, pady=5, sticky="e")
        self.label1.grid(row=1, column=2, padx=5, pady=5, sticky="we", columnspan=3)

        self.coord2 = tk.Label(self.frame_coords, text="Lon", bg="#e9e9e9")
        self.label2 = ttk.Entry(self.frame_coords, bootstyle="dark")
        self.coord2.grid(row=2, column=1, padx=5, pady=5, sticky="e")
        self.label2.grid(row=2, column=2, padx=5, pady=5, sticky="we",  columnspan=3)

        self.coord3 = tk.Label(self.frame_coords, text="H", bg="#e9e9e9")
        self.label3 = ttk.Entry(self.frame_coords, bootstyle="dark")
        self.coord3.grid(row=3, column=1, padx=5, pady=5, sticky="e")
        self.label3.grid(row=3, column=2, padx=5, pady=5, sticky="we",  columnspan=3)

        self.btn_add_input_points = ttk.Button(self.frame_coords, text="Save points", command=self.on_click_add_input_points, bootstyle="dark-outline")
        self.btn_add_input_points.grid(row=4, column=2, padx=3, pady=7, sticky="we", columnspan=2)
        self.btn_add_input_points.configure(width=1)

        self.btn_list = ttk.Button(self.frame_coords, text="Points", command=self.on_click_open_list, bootstyle="dark-outline")
        self.btn_list.grid(row=4, column=4, padx=3, pady=7, sticky="we", columnspan=1)
        self.btn_list.configure(width=5)

    def on_click_add_input_points(self):
        try:
            coords = self.get_input_coords()
            coords_dict = {
                "lat":    coords[0],
                "lon":    coords[1],
                "height": coords[2]
            }
            if self.point_name == "":
                self.point_name = "point"
            if coords_dict["lat"] == 0.0:
                raise Exception("Lat is empty")
            if coords_dict["lon"] == 0.0:
                raise Exception("Lon is empty")
            if coords_dict["height"] == 0.0:
                raise Exception("Height is empty")
            self.cont.save_input_points(self.point_name, coords_dict)
            self.update_point()
        except Exception as e:
            print(f"Error save point {e}")

    def on_click_open_list(self):
        self.open_points()

    def on_click_coords_format_radiobtn(self):
        coords = self.get_input_coords()
        self.set_input_coords(coords)
        self.update_label_coords(self.current_format)
        selected_format = self.selected_format.get()
        if self.update_coords_callback is not None:
            self.update_coords_callback(self.current_format, selected_format)
        else:
            print("ERROR callback is none")
        self.current_format = selected_format

    def on_click_combo_interval(self):
        pass

    def get_entry_rtcm_interval(self):
        value = self.interval_rtcm.get()
        try:
            if value == "":
                print("Интервал RTCM не задан!")
                raise ValueError("Zero value")
            result = int(value)
        except Exception:
            result = 1
        return result
    def set_entry_rtcm_interval(self, interval):
        self.interval_rtcm.delete(0, "end")
        self.interval_rtcm.insert(0, interval)

    def get_rtcm_type(self):
        return "3.2" if self.rtcm_values.get() == "3.2" else "3.0"

    def set_rtcm_type(self, val: str):
        self.rtcm_values.set("3.2") if val == "3.2" else self.rtcm_values.set("3.0")

    def get_coords_status(self):
        return "fix" if self.fix_auto_var.get() == 0 else "auto"

    def get_coords_format(self):
        return self.selected_format.get()

    def get_input_coords(self):
        '''Возвращает координаты в wgs84 формате'''
        coord1 = self.label1.get()
        coord2 = self.label2.get()
        coord3 = self.label3.get()

        if coord1 == "":
            coord1 = "0.0"
        if coord2 == "":
            coord2 = "0.0"
        if coord3 == "":
            coord3 = "0.0"

        try:
            coord1 = float(coord1)
        except ValueError:
            print("Ошибка: Невозможно преобразовать Lat в число")
            coord1 = 0.0

        try:
            coord2 = float(coord2)
        except ValueError:
            print("Ошибка: Невозможно преобразовать Lon в число")
            coord2 = 0.0

        try:
            coord3 = float(coord3)
        except ValueError:
            print("Ошибка: Невозможно преобразовать Heigh в число")
            coord3 = 0.0

        coords = [float(coord1), float(coord2), float(coord3)]
        if self.current_format == "ecef":
            coords = self.convert.convert_ecef_to_wgs(coords[0], coords[1], coords[2])
        point_name = self.point.get()
        self.point_name = point_name
        return coords

    def set_coords_status(self, val: str):
        self.fix_auto_var.set(0) if val == "fix" else self.fix_auto_var.set(1)

    def set_auto_btn(self):
        self.fix_auto_var.set(0)

    def set_coords_format(self, coords_format):
        if self.current_format == coords_format:
            return
        self.selected_format.set(coords_format)
        self.on_click_coords_format_radiobtn()
        return

    def set_input_coords(self, coords_input):
        '''Устанавливает координаты в поля coords input
        :parameter coords_input координаты в формате wgs84'''

        disable_flag = False
        coords_temp = [coords_input[0], coords_input[1], coords_input[2]]

        if self.selected_format.get() == "ecef":
            coords_temp = self.convert.convert_wgs_to_ecef(coords_temp[0], coords_temp[1], coords_temp[2])

        if str(self.label1["state"]) == "disabled":
            self.lock_configures(True)
            disable_flag = True
        self.label1.delete(0, "end")
        self.label1.insert(0, coords_temp[0])
        self.label2.delete(0, "end")
        self.label2.insert(0, coords_temp[1])
        self.label3.delete(0, "end")
        self.label3.insert(0, coords_temp[2])
        if disable_flag:
            self.lock_configures(False)

    def set_name_points(self, name_point):
        self.point.delete(0, 'end')
        self.point.insert(0, name_point)

    def lock_configures(self, value: bool):
        if value:
            self.label1.configure(state="disabled")
            self.label2.configure(state="disabled")
            self.label3.configure(state="disabled")
            self.check_auto.configure(state="disabled")
            self.rtcm_1.configure(state="disabled")
            self.rtcm_2.configure(state="disabled")
        else:
            self.label1.configure(state="normal")
            self.label2.configure(state="normal")
            self.label3.configure(state="normal")
            self.check_auto.configure(state="normal")
            self.rtcm_1.configure(state="normal")
            self.rtcm_2.configure(state="normal")

    def update_label_coords(self, coords_format):
        if coords_format == "wgs84":
            self.coord1.config(text="X")
            self.coord2.config(text="Y")
            self.coord3.config(text="Z")
        else:
            self.coord1.config(text="Lat")
            self.coord2.config(text="Lon")
            self.coord3.config(text="H")

