import tkinter as tk
import ttkbootstrap as ttk


class ComponentsNtrip:
    is_running = False
    get_output_ports = None

    def __init__(self):
        self.ntrip_remote_host_var = tk.StringVar()
        self.ntrip_remote_host_var.set("pidt.net")

        self.ntrip_remote_port_var = tk.StringVar()
        self.ntrip_remote_port_var.set("2101")

        self.ntrip_remote_mount_var = tk.StringVar()
        self.ntrip_remote_mount_var.set("PH")

        self.ntrip_remote_pwd_var = tk.StringVar()
        self.ntrip_remote_pwd_var.set("1234")

    def init_settings(self, frame: ttk.Frame):
        self.frame = frame
        self.is_running = True

        notebook = ttk.Notebook(self.frame, bootstyle="dark")
        notebook.grid(row=0, column=0, padx=5, pady=1, sticky="nsew")

        frame1 = ttk.Frame(notebook)
        frame2 = ttk.Frame(notebook)
        frame3 = ttk.Frame(notebook)
        frame4 = ttk.Frame(notebook)

        notebook.add(frame1, text='NTRIP Remote')
        notebook.add(frame2, text='Local NTRIP')
        notebook.add(frame3, text='TCP Remote')
        notebook.add(frame4, text='To port')

        # NTRIP Remote

        self.remote_compilation = tk.IntVar()
        self.remote_compilation.set(0)
        self.frm_2 = ttk.Checkbutton(frame1, text="NTRIP Remote", variable=self.remote_compilation, bootstyle="dark")
        self.frm_2.grid(row=0, column=0, padx=5, pady=10, columnspan=2, sticky="we")

        self.label_ntrip_host = tk.Label(frame1, text="Host")
        self.label_ntrip_host.grid(row=1, column=0, padx=5, pady=5, sticky="w")
        self.ntrip_remote_host = ttk.Entry(frame1, bootstyle="dark",width=9, textvariable=self.ntrip_remote_host_var)
        self.ntrip_remote_host.grid(row=1, column=1, padx=5, pady=5, sticky="we")

        self.label_ntrip_port = tk.Label(frame1, text="Port")
        self.label_ntrip_port.grid(row=1, column=2, padx=5, pady=5, sticky="w")
        self.ntrip_remote_port = ttk.Entry(frame1, bootstyle="dark", width=9, textvariable=self.ntrip_remote_port_var)
        self.ntrip_remote_port.grid(row=1, column=3, padx=5, pady=5, sticky="we")

        self.label_ntrip_mount = tk.Label(frame1, text="Mountpoint")
        self.label_ntrip_mount.grid(row=2, column=0, padx=5, pady=5, sticky="w")
        self.ntrip_remote_mount = ttk.Entry(frame1, bootstyle="dark", width=9, textvariable=self.ntrip_remote_mount_var)
        self.ntrip_remote_mount.grid(row=2, column=1, padx=5, pady=5, sticky="we")

        self.label_ntrip_pwd = tk.Label(frame1, text="Password")
        self.label_ntrip_pwd.grid(row=2, column=2, padx=5, pady=5, sticky="w")
        self.ntrip_remote_pwd = ttk.Entry(frame1, bootstyle="dark", width=9, textvariable=self.ntrip_remote_pwd_var, show="●")
        self.ntrip_remote_pwd.grid(row=2, column=3, padx=5, pady=5, sticky="we")

        # Local NTRIP

        self.local_compilation = tk.IntVar()
        self.local_compilation.set(0)
        self.frm_1 = ttk.Checkbutton(frame2, text="Local NTRIP", variable=self.local_compilation, command=self.on_click_local_radiobutton, bootstyle="dark")
        self.frm_1.grid(row=0, column=0, padx=5, pady=10, columnspan=1, sticky="we")

        # TCP_Port

        self.tcp_checkbox_var = tk.IntVar()
        self.tcp_checkbox_var.set(0)
        self.check_tcp = ttk.Checkbutton(frame2, text="TCP port", variable=self.tcp_checkbox_var, bootstyle="dark")
        self.check_tcp.grid(row=1, column=0, padx=5, pady=5, sticky="we")

        self.ntrip_port = ttk.Entry(frame2, bootstyle="dark", width=13)
        self.ntrip_port.grid(row=1, column=1, padx=5, pady=5, sticky="we")

        # TCP Remote

        self.tcp_remote_var = tk.IntVar()
        self.tcp_remote_var.set(0)
        self.tcp_remote = ttk.Checkbutton(frame3, text="TCP Remote", variable=self.tcp_remote_var, bootstyle="dark")
        self.tcp_remote.grid(row=0, column=0, padx=5, pady=10, columnspan=2, sticky="we")

        self.label_tcp_host = tk.Label(frame3, text="Host")
        self.label_tcp_host.grid(row=1, column=0, padx=5, pady=5, sticky="w")
        self.tcp_remote_host = ttk.Entry(frame3, bootstyle="dark", width=10)
        self.tcp_remote_host.grid(row=1, column=1, padx=5, pady=5, sticky="we")

        self.label_tcp_port = tk.Label(frame3, text="Port")
        self.label_tcp_port.grid(row=1, column=2, padx=5, pady=5, sticky="w")
        self.tcp_remote_port = ttk.Entry(frame3, bootstyle="dark", width=10)
        self.tcp_remote_port.grid(row=1, column=3, padx=5, pady=5, sticky="we")

        # To com port

        self.output_checkbox_var = tk.IntVar()
        self.output_checkbox_var.set(0)
        self.virt_port_status = "off"
        self.check_open_port = ttk.Checkbutton(frame4, text="RTCM to port", variable=self.output_checkbox_var, bootstyle="dark")
        self.combo_virt_port = ttk.Combobox(frame4, bootstyle="dark")
        self.combo_virt_port.insert(0, "Select port")
        self.combo_virt_port.config(postcommand=self.on_click_output_port_combobox)
        self.check_open_port.grid(row=1, column=0, padx=5, pady=10, sticky="we")
        self.combo_virt_port.grid(row=1, column=1, padx=5, pady=10, sticky="we", columnspan=1)
        self.combo_virt_port.config(width=13)
        self.combo_virt_port.func = None

        self.frame.grid_rowconfigure(9, minsize=41)

    def update_tcp_settings_state(self):
        status_local_caster = self.local_compilation.get()
        if status_local_caster == 0:
            self.check_tcp.configure(state="disabled")
            self.ntrip_port.configure(state="disabled")
        else:
            self.check_tcp.configure(state="normal")
            self.ntrip_port.configure(state="normal")

    def on_click_local_radiobutton(self):
        self.update_tcp_settings_state()

    def on_click_output_port_combobox(self):
        if self.get_output_ports is not None:
            ports = self.get_output_ports()
            self.combo_virt_port.config(values=ports)
            self.combo_virt_port.state(["readonly"])
        else:
            print("ERROR callback is none")

    def get_output_port(self):
        val = self.combo_virt_port.get()
        try:
            if val == "Select port":
                return ""
            elif len(val) == 0:
                raise ValueError("Zero value")
            result = str(val)
        except Exception as e:
            result = ""
        return result

    def get_output_checkbox(self):
        return False if self.output_checkbox_var.get() == 0 else True

    def get_tcp_local_port(self):
        value = self.ntrip_port.get()
        try:
            if len(value) == 0:
                raise ValueError("Zero value")
            result = int(value)
        except Exception as e:
            result = 5200
        return result

    def get_var_tcp_checkbox(self):
        return False if self.tcp_checkbox_var.get() == 0 else True

    def get_var_local_ntrip(self):
        status_local = self.local_compilation.get()
        if status_local == 1:
            ntrip_local = True
        else:
            ntrip_local = False
        return ntrip_local

    def get_var_remote_ntrip(self):
        status_remote = self.remote_compilation.get()
        if status_remote == 1:
            ntrip_remote = True
        else:
            ntrip_remote = False
        return ntrip_remote

    def get_ntrip_port(self):
        value = self.ntrip_remote_port.get()
        result = str(value)
        return result

    def get_htrip_host(self):
        value = self.ntrip_remote_host.get()
        result = str(value)
        return result

    def get_ntrip_mountpoint(self):
        value = self.ntrip_remote_mount.get()
        result = str(value)
        return result

    def get_ntrip_pwd(self):
        value = self.ntrip_remote_pwd.get()
        result = str(value)
        return result

    def get_tcp_remote(self):
        status_remote = self.tcp_remote_var.get()
        if status_remote == 1:
            tcp_remote = True
        else:
            tcp_remote = False
        return tcp_remote

    def get_tcp_remote_port(self):
        value = self.tcp_remote_port.get()
        try:
            if len(value) == 0:
                raise ValueError("Zero value")
            result = int(value)
        except Exception as e:
            result = 5200
        return result

    def get_tcp_remote_host(self):
        value = self.tcp_remote_host.get()
        try:
            if value == "":
                raise ValueError("Zero value")
            result = str(value)
        except Exception:
            result = ""
        return result

    def set_combobox_output_port(self, to_com_port):
        self.combo_virt_port.set(to_com_port)

    def set_val_checkbox_output_port(self, val: bool):
        self.output_checkbox_var.set(0) if not val else self.output_checkbox_var.set(1)

    def set_tcp_port_entry(self, tcp_local_port):
        self.ntrip_port.delete(0, "end")
        self.ntrip_port.insert(0, tcp_local_port)

    def set_val_tcp_checkbox(self, val: bool):
        self.tcp_checkbox_var.set(0) if not val else self.tcp_checkbox_var.set(1)

    def set_val_ntrip_remote(self, ntrip_remote):
        if ntrip_remote:
            self.remote_compilation.set(1)
        else:
            self.remote_compilation.set(0)

    def set_val_ntrip_local(self, ntrip_local):
        if ntrip_local:
            self.local_compilation.set(1)
        else:
            self.local_compilation.set(0)

    def set_ntrip_port(self, ntrip_remote_port):
        self.ntrip_remote_port.delete(0, "end")
        self.ntrip_remote_port.insert(0, ntrip_remote_port)

    def set_ntrip_host(self, ntrip_remote_host):
        self.ntrip_remote_host.delete(0, "end")
        self.ntrip_remote_host.insert(0, ntrip_remote_host)

    def set_ntrip_mount(self, ntrip_remote_mount):
        self.ntrip_remote_mount.delete(0, "end")
        self.ntrip_remote_mount.insert(0, ntrip_remote_mount)

    def set_ntrip_pwd(self, ntrip_remote_pwd):
        self.ntrip_remote_pwd.delete(0, "end")
        self.ntrip_remote_pwd.insert(0, ntrip_remote_pwd)

    def set_tcp_remote(self, val:bool):
        self.tcp_remote_var.set(0) if not val else self.tcp_remote_var.set(1)

    def set_tcp_remote_port(self, port):
        self.tcp_remote_port.delete(0, "end")
        self.tcp_remote_port.insert(0, port)

    def set_tcp_remote_host(self, host):
        self.tcp_remote_host.delete(0, "end")
        self.tcp_remote_host.insert(0, host)

    def lock_configures(self, value):
        if value:
            self.frm_1.configure(state="disabled")
            self.frm_2.configure(state="disabled")
            self.check_tcp.configure(state="disabled")
            self.ntrip_port.configure(state="disabled")
            self.check_open_port.configure(state="disabled")
            self.combo_virt_port.configure(state="disabled")
            self.ntrip_remote_port.configure(state="disabled")
            self.ntrip_remote_host.configure(state="disabled")
            self.ntrip_remote_mount.configure(state="disabled")
            self.ntrip_remote_pwd.configure(state="disabled")
            self.tcp_remote.configure(state="disabled")
            self.tcp_remote_port.configure(state="disabled")
            self.tcp_remote_host.configure(state="disabled")

        else:
            self.frm_1.configure(state="normal")
            self.frm_2.configure(state="normal")
            self.check_tcp.configure(state="normal")
            self.ntrip_port.configure(state="normal")
            self.check_open_port.configure(state="normal")
            self.combo_virt_port.configure(state="normal")
            self.ntrip_remote_port.configure(state="normal")
            self.ntrip_remote_host.configure(state="normal")
            self.ntrip_remote_mount.configure(state="normal")
            self.ntrip_remote_pwd.configure(state="normal")
            self.tcp_remote.configure(state="normal")
            self.tcp_remote_port.configure(state="normal")
            self.tcp_remote_host.configure(state="normal")


