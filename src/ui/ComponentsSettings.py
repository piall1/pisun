import tkinter as tk
import ttkbootstrap as ttk


class ComponentsSettings:
    is_running = False

    def __init__(self):
        pass

    def init_settings(self, frame: ttk.Frame):
        self.frame = frame
        self.is_running = True

        self.hardware_com1_var = tk.IntVar()
        self.hardware_com1_var.set(0)
        self.checkbox_hardware1 = ttk.Checkbutton(self.frame, text="COM1", variable=self.hardware_com1_var, bootstyle="dark")
        self.checkbox_hardware1.grid(row=0, column=1, padx=5, pady=5, sticky="we")

        self.hardware_com2_var = tk.IntVar()
        self.hardware_com2_var.set(0)
        self.checkbox_hardware2 = ttk.Checkbutton(self.frame, text="COM2", variable=self.hardware_com2_var, bootstyle="dark")
        self.checkbox_hardware2.grid(row=1, column=1, padx=5, pady=5, sticky="we")

        self.hardware_com3_var = tk.IntVar()
        self.hardware_com3_var.set(0)
        self.checkbox_hardware3 = ttk.Checkbutton(self.frame, text="COM3", variable=self.hardware_com3_var, bootstyle="dark")
        self.checkbox_hardware3.grid(row=2, column=1, padx=5, pady=5, sticky="we")

        self.combo_speed_com1 = ttk.Combobox(self.frame, bootstyle="dark")
        self.combo_speed_com1.insert(0, "Select speed")
        self.combo_speed_com1['values'] = [115200, 38400]
        self.combo_speed_com1.grid(row=0, column=2, padx=5, pady=5, sticky="we")
        self.combo_speed_com1.config(width=13)
        self.combo_speed_com1.config(postcommand=self.on_click_combo_speed1)

        self.combo_speed_com2 = ttk.Combobox(self.frame, bootstyle="dark")
        self.combo_speed_com2.insert(0, "Select speed")
        self.combo_speed_com2['values'] = [115200, 38400]
        self.combo_speed_com2.grid(row=1, column=2, padx=5, pady=5, sticky="we")
        self.combo_speed_com2.config(width=13)
        self.combo_speed_com2.config(postcommand=self.on_click_combo_speed2)

        self.combo_speed_com3 = ttk.Combobox(self.frame, bootstyle="dark")
        self.combo_speed_com3.insert(0, "Select speed")
        self.combo_speed_com3['values'] = [115200, 38400]
        self.combo_speed_com3.grid(row=2, column=2, padx=5, pady=5, sticky="we")
        self.combo_speed_com3.config(width=13)
        self.combo_speed_com3.config(postcommand=self.on_click_combo_speed3)

        self.combo_format_com1 = ttk.Combobox(self.frame, bootstyle="dark")
        self.combo_format_com1.insert(0, "Output format")
        self.combo_format_com1['values'] = ["rtcm3.0", "rtcm3.2", "rtcm3.2.radio"]
        self.combo_format_com1.grid(row=0, column=3, padx=5, pady=5, sticky="we")
        self.combo_format_com1.config(width=13)
        self.combo_format_com1.config(postcommand=self.on_click_combo_format1)

        self.combo_format_com2 = ttk.Combobox(self.frame, bootstyle="dark")
        self.combo_format_com2.insert(0, "Output format")
        self.combo_format_com2['values'] = ["rtcm3.0", "rtcm3.2", "rtcm3.2.radio"]
        self.combo_format_com2.grid(row=1, column=3, padx=5, pady=5, sticky="we")
        self.combo_format_com2.config(width=13)
        self.combo_format_com2.config(postcommand=self.on_click_combo_format2)

        self.combo_format_com3 = ttk.Combobox(self.frame, bootstyle="dark")
        self.combo_format_com3.insert(0, "Output format")
        self.combo_format_com3['values'] = ["rtcm3.0", "rtcm3.2", "rtcm3.2.radio"]
        self.combo_format_com3.grid(row=2, column=3, padx=5, pady=5, sticky="we")
        self.combo_format_com3.config(width=13)
        self.combo_format_com3.config(postcommand=self.on_click_combo_format3)

        self.gps_var = tk.IntVar()
        self.gps_var.set(0)
        self.gps_checkbutton = ttk.Checkbutton(self.frame, text="GPS", variable=self.gps_var, bootstyle="dark")
        self.gps_checkbutton.grid(row=0, column=4, padx=5, pady=5, sticky="we")

        self.glo_var = tk.IntVar()
        self.glo_var.set(0)
        self.glo_checkbutton = ttk.Checkbutton(self.frame, text="GLO", variable=self.glo_var, bootstyle="dark")
        self.glo_checkbutton.grid(row=0, column=5, padx=5, pady=5, sticky="we")

        self.gali_var = tk.IntVar()
        self.gali_var.set(0)
        self.gali_checkbutton = ttk.Checkbutton(self.frame, text="GAL", variable=self.gali_var, bootstyle="dark")
        self.gali_checkbutton.grid(row=1, column=4, padx=5, pady=5, sticky="we")

        self.bds_var = tk.IntVar()
        self.bds_var.set(0)
        self.bds_checkbutton = ttk.Checkbutton(self.frame, text="BDS", variable=self.bds_var, bootstyle="dark")
        self.bds_checkbutton.grid(row=1, column=5, padx=5, pady=5, sticky="we")

        self.interval_combo = ttk.Combobox(self.frame, bootstyle="dark")
        self.interval_combo.insert(0, "interval")
        self.interval_combo['values'] = ["1", "2", "3", "4", "5"]
        self.interval_combo.grid(row=2, column=4, padx=5, pady=5, sticky="we", columnspan = 2)
        self.interval_combo.config(width=5)
        self.interval_combo.config(postcommand=self.on_click_combo_interval)

        # Max_files
        self.label_params = tk.Label(self.frame, text="Max files")
        self.label_params.grid(row=0, column=7, padx=5, pady=5, sticky="we")
        self.max_size_entry = ttk.Entry(self.frame, bootstyle="dark")
        self.max_size_entry.grid(row=1, column=7, padx=5, pady=5, sticky="we")
        self.max_size_entry.config(width=5)

        # PPS and Event
        self.label_params = tk.Label(self.frame, text="Settings")
        self.pps_var = tk.IntVar()
        self.pps_var.set(0)
        self.pps_status = "off"
        self.pps_checkbutton = ttk.Checkbutton(self.frame, text="PPS", variable=self.pps_var, bootstyle="dark-square-toggle")
        self.event_var = tk.IntVar()
        self.event_var.set(0)
        self.event_status = "off"
        self.event_checkbutton = ttk.Checkbutton(self.frame, text="EVENT", variable=self.event_var, bootstyle="dark-square-toggle")
        self.label_params.grid(row=0, column=6, padx=40, pady=5, sticky="we")
        self.pps_checkbutton.grid(row=1, column=6, padx=40, pady=5, sticky="we")
        self.event_checkbutton.grid(row=2, column=6, padx=40, pady=5, sticky="we")

    def on_click_combo_speed1(self):
        self.combo_speed_com1.state(["readonly"])

    def on_click_combo_speed2(self):
        self.combo_speed_com2.state(["readonly"])

    def on_click_combo_speed3(self):
        self.combo_speed_com3.state(["readonly"])

    def on_click_combo_format1(self):
        self.combo_format_com1.state(["readonly"])

    def on_click_combo_format2(self):
        self.combo_format_com2.state(["readonly"])

    def on_click_combo_format3(self):
        self.combo_format_com3.state(["readonly"])

    def on_click_combo_interval(self):
        self.combo_format_com3.state(["readonly"])

    def get_hardware1_checkbox(self):
        return False if self.hardware_com1_var.get() == 0 else True

    def get_hardware2_checkbox(self):
        return False if self.hardware_com2_var.get() == 0 else True

    def get_hardware3_checkbox(self):
        return False if self.hardware_com3_var.get() == 0 else True

    def get_combo_speed_com1(self):
        val = self.combo_speed_com1.get()
        try:
            if len(val) == 0:
                raise ValueError("Zero value")
            result = int(val)
        except Exception as e:
            result = 115200
        return result

    def get_combo_speed_com2(self):
        val = self.combo_speed_com2.get()
        try:
            if len(val) == 0:
                raise ValueError("Zero value")
            result = int(val)
        except Exception as e:
            result = 115200
        return result

    def get_combo_speed_com3(self):
        val = self.combo_speed_com3.get()
        try:
            if len(val) == 0:
                raise ValueError("Zero value")
            result = int(val)
        except Exception as e:
            result = 115200
        return result

    def get_combo_format_com1(self):
        val = self.combo_format_com1.get()
        try:
            if val == "Output format":
                return "none"
            elif val == "rtcm3.0":
                return "3.0"
            elif val == "rtcm3.2":
                return "3.2"
            elif val == "rtcm3.2.radio":
                val = "3.2.radio"
            elif len(val) == 0:
                raise ValueError("Zero value")
            result = str(val)
        except Exception as e:
            result = "none"
        return result

    def get_combo_format_com2(self):
        val = self.combo_format_com2.get()
        try:
            if val == "Output format":
                return "none"
            if val == "Output format":
                return "none"
            elif val == "rtcm3.0":
                return "3.0"
            elif val == "rtcm3.2":
                return "3.2"
            elif val == "rtcm3.2.radio":
                val = "3.2.radio"
            elif len(val) == 0:
                raise ValueError("Zero value")
            result = str(val)
        except Exception as e:
            result = "none"
        return result

    def get_combo_format_com3(self):
        val = self.combo_format_com3.get()
        try:
            if val == "Output format":
                return "none"
            if val == "Output format":
                return "none"
            elif val == "rtcm3.0":
                return "3.0"
            elif val == "rtcm3.2":
                return "3.2"
            elif val == "rtcm3.2.radio":
                val = "3.2.radio"
            elif len(val) == 0:
                raise ValueError("Zero value")
            result = str(val)
        except Exception as e:
            result = "none"
        return result

    def get_satellite_constellations(self):
        satellite_constellations = []
        if self.gps_var.get() == 1:
            satellite_constellations.append("GPS")
        if self.glo_var.get() == 1:
            satellite_constellations.append("GLO")
        if self.gali_var.get() == 1:
            satellite_constellations.append("GAL")
        if self.bds_var.get() == 1:
            satellite_constellations.append("BDS")
        return satellite_constellations

    def get_interval_list(self):
        val = self.interval_combo.get()
        try:
            if val == "interval":
                return 1
            elif len(val) == 0:
                raise ValueError("Zero value")
            result = int(val)
        except Exception as e:
            result = "none"
        return result

    def get_max_files(self):
        value = self.max_size_entry.get()
        try:
            if len(value) == 0:
                raise ValueError("Zero value")
            result = int(value)
        except Exception as e:
            result = 200
        return result

    def set_max_files(self, max_size_entry):
        self.max_size_entry.delete(0, "end")
        self.max_size_entry.insert(0, max_size_entry)

    def get_event_var(self):
        return False if self.event_var.get() == 0 else True

    def get_pps_var(self):
        return False if self.pps_var.get() == 0 else True

    def set_val_pps_output(self, val: bool):
        self.pps_var.set(0) if not val else self.pps_var.set(1)

    def set_val_event_input(self, val: bool):
        self.event_var.set(0) if not val else self.event_var.set(1)

    def set_satellite_constellations(self, list_sats):
        for sats in list_sats:
            if sats == "GPS":
                self.gps_var.set(1)
            if sats == "GLO":
                self.glo_var.set(1)
            if sats == "GAL":
                self.gali_var.set(1)
            if sats == "BDS":
                self.bds_var.set(1)

    def set_interval_list(self, interval):
        self.interval_combo.set(interval)

    def set_combo_format_com1(self, formate):
        if formate == "3.0":
            formate = "rtcm3.0"
        if formate == "3.2":
            formate = "rtcm3.2"
        if formate == "3.2.radio":
            formate = "rtcm3.2.radio"
        self.combo_format_com1.set(formate)

    def set_combo_format_com2(self, formate):
        if formate == "3.0":
            formate = "rtcm3.0"
        if formate == "3.2":
            formate = "rtcm3.2"
        if formate == "3.2.radio":
            formate = "rtcm3.2.radio"
        self.combo_format_com2.set(formate)

    def set_combo_format_com3(self, formate):
        if formate == "3.0":
            formate = "rtcm3.0"
        if formate == "3.2":
            formate = "rtcm3.2"
        if formate == "3.2.radio":
            formate = "rtcm3.2.radio"
        self.combo_format_com3.set(formate)

    def set_combo_speed_com1(self, speed):
        self.combo_speed_com1.set(speed)

    def set_combo_speed_com2(self, speed):
        self.combo_speed_com2.set(speed)

    def set_combo_speed_com3(self, speed):
        self.combo_speed_com3.set(speed)

    def set_hardware1_checkbox(self, val: bool):
        self.hardware_com1_var.set(0) if not val else self.hardware_com1_var.set(1)

    def set_hardware2_checkbox(self, val: bool):
        self.hardware_com2_var.set(0) if not val else self.hardware_com2_var.set(1)

    def set_hardware3_checkbox(self, val: bool):
        self.hardware_com3_var.set(0) if not val else self.hardware_com3_var.set(1)

    def lock_configures(self, value):
        if value:
            self.max_size_entry.configure(state="disabled")
            self.checkbox_hardware1.configure(state="disabled")
            self.checkbox_hardware2.configure(state="disabled")
            self.checkbox_hardware3.configure(state="disabled")
            self.combo_speed_com1.configure(state="disabled")
            self.combo_speed_com2.configure(state="disabled")
            self.combo_speed_com3.configure(state="disabled")
            self.combo_format_com1.configure(state="disabled")
            self.combo_format_com2.configure(state="disabled")
            self.combo_format_com3.configure(state="disabled")
            self.gps_checkbutton.configure(state="disabled")
            self.glo_checkbutton.configure(state="disabled")
            self.gali_checkbutton.configure(state="disabled")
            self.bds_checkbutton.configure(state="disabled")
            self.interval_combo.configure(state="disabled")
            self.pps_checkbutton.configure(state="disabled")
            self.event_checkbutton.configure(state="disabled")
        else:
            self.max_size_entry.configure(state="normal")
            self.checkbox_hardware1.configure(state="normal")
            self.checkbox_hardware2.configure(state="normal")
            self.checkbox_hardware3.configure(state="normal")
            self.combo_speed_com1.configure(state="normal")
            self.combo_speed_com2.configure(state="normal")
            self.combo_speed_com3.configure(state="normal")
            self.combo_format_com1.configure(state="normal")
            self.combo_format_com2.configure(state="normal")
            self.combo_format_com3.configure(state="normal")
            self.gps_checkbutton.configure(state="normal")
            self.glo_checkbutton.configure(state="normal")
            self.gali_checkbutton.configure(state="normal")
            self.bds_checkbutton.configure(state="normal")
            self.interval_combo.configure(state="normal")
            self.pps_checkbutton.configure(state="normal")
            self.event_checkbutton.configure(state="normal")

