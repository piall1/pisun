import math
import threading
import time
import tkinter as tk
import ttkbootstrap as ttk
import webbrowser
from src.services.Controller import Controller
from easygui import fileopenbox
from src.structures import ConvertDataClass
from src.utils.Console import print


class ConverterCNBView:
    con = Controller()

    def __init__(self):
        self.th = None
        self.is_running = False
        self.run_thread = False
        self.files_item_counter = 0
        self.current_page = 0
        self.MAX_FILES_TO_DISPLAY = 10
        self.count_page = None
        self.items_container = {}
        self.icon_download = tk.PhotoImage(file="resources/image/icon_download.png")
        self.icons_update = tk.PhotoImage(file="resources/image/icons_update.png")
        self.icon_right_arrow = tk.PhotoImage(file="resources/image/icon_right_arrow.png")
        self.icon_left_arrow = tk.PhotoImage(file="resources/image/icon_left_arrow.png")
        self.icon_search = tk.PhotoImage(file="resources/image/icon_search.png")
        self.icon_cancel = tk.PhotoImage(file="resources/image/icon_cancel.png")

    def start_converter(self, parent):
        try:
            if self.is_running:
                print("Окно уже открыто")
                return
            self.popup = tk.Toplevel(parent)
            self.popup.title(f"PiSun Cors {self.con.get_firmware_version()}")
            self.popup.iconbitmap("resources/image/Pi_sun_w_1_ico.ico")
            self.popup.protocol("WM_DELETE_WINDOW", self.on_click_close_window)
            self.popup.update()
            win_width = 508
            win_height = 750
            screen_width = self.popup.winfo_screenwidth()
            screen_height = self.popup.winfo_screenheight()
            x = int((screen_width / 2) - (win_width / 2))
            y = int((screen_height / 2) - (win_height / 2))
            geometry_str = f"{win_width}x{win_height}+{x}+{y}"
            self.popup.geometry(geometry_str)
            self.popup.resizable(True, True)

            self.Frame = ttk.Frame(self.popup)
            self.Frame.grid(row=0, column=0, padx=2, pady=5, sticky="we", columnspan=1)

            self.show_window()
            self.run_thread = True
            self.th = threading.Thread(target=self.update_converting)
            self.th.start()
        except Exception as e:
            print(f"Error open converter: {e}")

    def on_click_close_window(self):
        def on_window_closing_async():
            if self.run_thread:
                self.run_thread = False
                self.th.join()
            self.is_running = False
            self.items_container.clear()
            self.files_item_counter = 0
            self.popup.destroy()
            return

        closing_thread = threading.Thread(
            target=on_window_closing_async, name="on_window_closing_async"
        )
        closing_thread.start()

    def on_click_open_file(self):
        try:
            filenames = fileopenbox(multiple=True)
            if filenames is None:
                raise Exception("Файл не выбран")
            for filename in filenames:
                if ".cnb" not in filename:
                    raise Exception("Неверный формат файла")
                self.con.start_convert_cnb(filename)
            self.destroy_window()
            self.update_draw()
        except Exception as e:
            print(f"Ошибка при конвертации: {e}")

    def download_result(self, link):
        try:
            if len(link) == 0 or link is None:
                raise Exception("Ссылка не найдена")
            webbrowser.open_new(link)
        except Exception as e:
            print(f"Ошибка: {str(e)}")

    def download_all(self):
        new_data = self.con.get_convert_result_link()
        data_list = list(reversed(new_data))
        chunks = [data_list[i:i + self.MAX_FILES_TO_DISPLAY] for i in
                  range(0, len(data_list), self.MAX_FILES_TO_DISPLAY)]
        if len(chunks) == 0:
            return
        for item in chunks[self.current_page]:
            if item.status == ConvertDataClass.Status.ready:
                webbrowser.open_new(item.link)

    def on_click_searched_points(self):
        self.destroy_window()
        self.searched_file()

    def searched_file(self):
        try:
            found_file = self.entry_searche.get()
            list_data = self.con.get_convert_result_link()
            filter_list = []
            for item in list_data:
                if found_file in item.file_path:
                    filter_list.append(item)
            self.update_draw(filter_list)
            if len(filter_list) == 0:
                print("No result")
        except Exception as e:
            print(f"Search error {e}")

    def close_searche_window(self):
        self.entry_searche.config(state="normal")
        self.entry_searche.delete(0, "end")
        self.destroy_window()
        self.update_draw()

    def go_forward(self):
        if self.current_page < self.count_page - 1:
            self.current_page += 1
            self.destroy_window()
            self.update_draw()

    def go_back(self):
        if self.current_page > 0:
            self.current_page -= 1
            self.destroy_window()
            self.update_draw()

    def destroy_window(self):
        self.items_container.clear()
        self.frm2.destroy()
        self.frm2 = ttk.Frame(self.Frame, borderwidth=1)
        self.frm2.grid(row=2, column=0, padx=5, pady=1, sticky="ns", columnspan=1)

    def update_converting(self):
        while self.run_thread:
            try:
                found_file = self.entry_searche.get()
                if found_file == "":
                    self.update_draw()
                    time.sleep(1)
                else:
                    self.searched_file()
                    time.sleep(1)
            except Exception as e:
                print(f"Exсept {e}")

    def update_draw(self, val=None):
        if val is None:
            val = self.con.get_convert_result_link()
        data_list = list(reversed(val))

        self.count_page = math.ceil(len(data_list) / self.MAX_FILES_TO_DISPLAY)
        self.label_count_page.config(text=f"{(self.current_page + 1)} / {self.count_page}")

        chunks = [data_list[i:i + self.MAX_FILES_TO_DISPLAY] for i in
                  range(0, len(data_list), self.MAX_FILES_TO_DISPLAY)]

        if len(chunks) == 0:
            return

        for item in chunks[self.current_page]:
            if not item.id in self.items_container:
                if not hasattr(self, 'frm2') or not self.frm2.winfo_exists():
                    self.frm2 = ttk.Frame(self.Frame, borderwidth=1)
                    self.frm2.grid(row=2, column=0, padx=5, pady=1, sticky="ns", columnspan=1)
                self.draw_new_file(item.id, item.file_path, item.link,
                                   ConvertDataClass.Status(item.status).name)
                continue
            if self.items_container[item.id]["status"].cget("text") != ConvertDataClass.Status(
                    item.status).name:
                self.items_container[item.id]["link"].config(
                    command=lambda i=item.link: self.download_result(i))
                self.items_container[item.id]["status"].config(text=ConvertDataClass.Status(item.status).name)

    def show_window(self):
        self.frm1 = ttk.Frame(self.Frame, borderwidth=1)
        self.frm1.grid(row=0, column=0, padx=5, pady=1, sticky="ns", columnspan=1)

        self.frame_page = ttk.Frame(self.Frame, borderwidth=1)
        self.frame_page.grid(row=1, column=0, padx=5, pady=5, sticky="ns", columnspan=1)

        self.frm2 = ttk.Frame(self.Frame, borderwidth=1)
        self.frm2.grid(row=2, column=0, padx=5, pady=1, sticky="ns", columnspan=1)

        self.entry_searche = ttk.Entry(self.frm1, bootstyle="dark")
        self.entry_searche.grid(row=0, column=0, padx=2, pady=5, sticky="we", columnspan=2)
        btn_searche = ttk.Button(self.frm1, image=self.icon_search, bootstyle="light-outline", command=self.on_click_searched_points)
        btn_searche.grid(row=0, column=2, padx=2, pady=5, sticky="we", columnspan=1)
        btn_close = ttk.Button(self.frm1, image=self.icon_cancel, bootstyle="light-outline", command=self.close_searche_window)
        btn_close.grid(row=0, column=3, padx=2, pady=5, sticky="we", columnspan=1)

        label_convert = tk.Label(self.frm1, text="Select CNB files to upload", bg="#e9e9e9")
        label_convert.grid(row=1, column=0, padx=5, pady=5, sticky="we")
        browse_button = ttk.Button(self.frm1, text="Upload file", bootstyle="dark", command=self.on_click_open_file)
        browse_button.grid(row=1, column=1, padx=2, pady=5, sticky="we", columnspan=1)
        download_btn = ttk.Button(self.frm1, image=self.icon_download, bootstyle="light-outline", command=self.download_all)
        download_btn.grid(row=1, column=2, padx=2, pady=1, sticky="we", columnspan=1)

        button_1 = ttk.Button(self.frame_page, image=self.icon_right_arrow,  bootstyle="light-outline", command=self.go_forward)
        button_1.grid(row=2, column=2, padx=2, pady=1, sticky="we", columnspan=1)

        self.label_count_page = tk.Label(self.frame_page, text=f"{(self.current_page + 1)} / {self.count_page}", bg="#e9e9e9", width=15, wraplength=50)
        self.label_count_page.grid(row=2, column=1, padx=5, pady=5, sticky="e")

        button_2 = ttk.Button(self.frame_page, image=self.icon_left_arrow, bootstyle="light-outline", command=self.go_back)
        button_2.grid(row=2, column=0, padx=2, pady=1, sticky="we", columnspan=1)

        self.is_running = True

    def draw_new_file(self, id, file_name, link, status):
        if id in self.items_container:
            print("Error")

        labelfrm = ttk.Labelframe(self.frm2, text="", bootstyle="dark")

        label_file = tk.Label(labelfrm, text="File: ", bg="#e9e9e9")
        label_link = tk.Label(labelfrm, text="RINEX file:", bg="#e9e9e9")

        file_received = tk.Label(labelfrm, text=file_name, bg="#e9e9e9", width=25, anchor='w')
        status_received = tk.Label(labelfrm, text=status, bg="#e9e9e9", width=15)
        link_received = ttk.Button(labelfrm, image=self.icon_download, bootstyle="light-outline", command=lambda i=link: self.download_result(i))

        labelfrm.grid        (row=self.files_item_counter, column=0, padx=10, pady=1, sticky="we", columnspan=1)
        label_file.grid      (row=0, column=0, padx=5, pady=5, sticky="we", columnspan=1)
        file_received.grid   (row=0, column=1, padx=5, pady=5, sticky="w",  columnspan=1)
        status_received.grid (row=0, column=3, padx=5, pady=5, sticky="we", columnspan=1)
        label_link.grid      (row=0, column=4, padx=5, pady=5, sticky="we")
        link_received.grid   (row=0, column=5, padx=2, pady=5, sticky="we")

        self.items_container[id] = {"file": file_received, "link": link_received, "status": status_received}

        self.files_item_counter += 1

