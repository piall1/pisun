import tkinter as tk
import ttkbootstrap as ttk
from src.services import Controller


class FTPComponents:

    def __init__(self):
        self.cont = Controller.Controller()
        self.connect_callback = None
        self.elements = []

    def init_settings(self, frame: ttk.Frame):
        self.frame = frame

        self.label = tk.Label(self.frame, text="Send static files to your FTP server.", bg="#e9e9e9")
        self.label.grid(row=0, column=0, padx=5, pady=5, sticky="ns", columnspan=2)

        self.label_btn = tk.Label(self.frame, text="Send files", bg="#e9e9e9")
        self.label_btn.grid(row=0, column=2, padx=1, pady=5, sticky="ns", columnspan=1)

        self.status_work = tk.BooleanVar()
        self.status_work.set(False)
        self.check_btn_send = ttk.Checkbutton(self.frame, text="OFF", variable=self.status_work, command=self.update_window, bootstyle="dark-round-toggle")
        self.check_btn_send.grid(row=0, column=3, padx=5, pady=5, sticky="we")

    def update_window(self):
        current_status = self.status_work.get()
        if current_status:
            self.new_window()
        else:
            self.remove_window()

    def new_window(self):
        self.check_btn_send = ttk.Checkbutton(self.frame, text="ON", variable=self.status_work, command=self.update_window, bootstyle="dark-round-toggle")
        self.check_btn_send.grid(row=0, column=3, padx=5, pady=5, sticky="we")

        if not self.elements:
            self.host_label = tk.Label(self.frame, text="Host", bg="#e9e9e9")
            self.host_label.grid(row=1, column=0, padx=5, pady=5, sticky="e")
            self.host_name = ttk.Entry(self.frame, bootstyle="dark")
            self.host_name.grid(row=1, column=1, padx=5, pady=5, sticky="we")

            self.port_label = tk.Label(self.frame, text="Port", bg="#e9e9e9")
            self.port_label.grid(row=1, column=2, padx=5, pady=5, sticky="e")
            self.port_name = ttk.Entry(self.frame, bootstyle="dark")
            self.port_name.grid(row=1, column=3, padx=5, pady=5, sticky="we")

            self.login_label = tk.Label(self.frame, text="Login", bg="#e9e9e9")
            self.login_label.grid(row=2, column=0, padx=5, pady=5, sticky="e")
            self.login = ttk.Entry(self.frame, bootstyle="dark")
            self.login.grid(row=2, column=1, padx=5, pady=5, sticky="we")

            self.password_label = tk.Label(self.frame, text="PWD", bg="#e9e9e9")
            self.password_label.grid(row=2, column=2, padx=5, pady=5, sticky="e")
            self.password = ttk.Entry(self.frame, bootstyle="dark", show="●")
            self.password.grid(row=2, column=3, padx=5, pady=5, sticky="we")

            # self.btn_connect = ttk.Button(self.frame, text="Connect", width=15, command=self.on_click_connect, bootstyle="dark")
            # self.btn_connect.grid(row=3, column=1, padx=5, pady=5, sticky="we")

            self.elements = [
                self.host_label,
                self.host_name,
                self.port_label,
                self.port_name,
                self.login_label,
                self.login,
                self.password_label,
                self.password,
            ]

    def remove_window(self):
        self.check_btn_send = ttk.Checkbutton(self.frame, text="OFF", variable=self.status_work, command=self.update_window, bootstyle="dark-round-toggle")
        self.check_btn_send.grid(row=0, column=3, padx=5, pady=5, sticky="we")

        if self.elements:
            for element in self.elements:
                element.grid_remove()  # Прячем элементы
            self.elements.clear()  # Очищаем список

    def get_status_work_ftp(self):
        return False if self.status_work.get() == False else True

    def set_status_work_ftp(self, val: bool):
        self.status_work.set(False) if not val else self.status_work.set(True)
        self.update_window()

    def get_host(self):
        try:
            if hasattr(self, 'host_name'):
                value = self.host_name.get()
                if len(value) == 0:
                    raise ValueError("Zero value")
                result = str(value)
            else:
                result = ""
        except Exception:
            result = ""
        return result

    def get_port(self):
        try:
            if hasattr(self, 'port_name'):
                value = self.port_name.get()
                if len(value) == 0:
                    raise ValueError("Zero value")
                result = int(value)
            else:
                result = 0
        except Exception:
            result = 0
        return result

    def get_login(self):
        try:
            if hasattr(self, 'login'):
                value = self.login.get()
                if len(value) == 0:
                    raise ValueError("Zero value")
                result = str(value)
            else:
                result = ""
        except Exception:
            result = ""
        return result

    def get_password(self):
        try:
            if hasattr(self, 'password'):
                value = self.password.get()
                if len(value) == 0:
                    raise ValueError("Zero value")
                result = str(value)
            else:
                result = ""
        except Exception:
            result = ""
        return result

    def set_host(self, host_name):
        if hasattr(self, 'host_name'):
            self.host_name.delete(0, "end")
            self.host_name.insert(0, host_name)
        else:
            return

    def set_port(self, port_name):
        if hasattr(self, 'port_name'):
            self.port_name.delete(0, "end")
            self.port_name.insert(0, port_name)
        else:
            return

    def set_login(self, login):
        if hasattr(self, 'login'):
            self.login.delete(0, "end")
            self.login.insert(0, login)
        else:
            return

    def set_pwd(self, pwd):
        if hasattr(self, 'password'):
            self.password.delete(0, "end")
            self.password.insert(0, pwd)
        else:
            return