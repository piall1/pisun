import threading
import tkinter as tk
from tkinter import ttk
import numpy as np
import matplotlib as plt
from matplotlib.figure import Figure
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from src.structures.ForPrint import for_print


class Plotter:
    _get_data_for_print = None
    _update_thread = None
    is_running = False
    event = threading.Event()
    _local_data = for_print()
    _previos_fig = "bar"

    _size_bar = 8, 5
    _size_polar = 5, 5

    class Colors:
        default_freq_color = "#DC143C"
        default_sat_name_color = "gray"
        _freqs_color = {
            0:"#DC143C",
            1:"#ADFF2F",
            2:"#32CD32",
            3:"#2E8B57",
            4:"#006400",
            5:"#20B2AA",
            6:"#008080"
        }
        _sat_name_color = {
            "G": "#228B22",
            "R": "#0000FF",
            "S": "#6A5ACD",
            "E": "#8B4513",
            "C": "#FF0000",
            "N": "#e9e9e9"
        }

        def get_color_freq(self, freq_n):
            return self._freqs_color.get(freq_n, self.default_freq_color)

        def get_color_sat(self, sat_sys):
            return self._sat_name_color.get(sat_sys, self.default_sat_name_color)

    def __init__(self):
        pass

    def init_plot(self):
        self.fig_frame = ttk.Frame(self.root)
        self.fig_frame.grid(row=0, column=1, padx=5, pady=5, columnspan=1, sticky="w")

        self.figure_bar = Figure(figsize=(self._size_bar), dpi=100)
        self.plot_bar = self.figure_bar.add_subplot(1, 1, 1)
        self.figure_bar.tight_layout(pad=2)
        self.canvas_bar = FigureCanvasTkAgg(self.figure_bar, self.fig_frame)
        self.canvas_bar.get_tk_widget().grid(row=0, column=0)
        #self.figure_bar.patch.set_facecolor('red')
        self.plot_bar.bar([], [])
        self.plot_bar.set_ylim(0, 60)

        self.figure_bar = Figure(figsize=(self._size_polar), dpi=100)
        self.plot_pol = self.figure_bar.add_subplot(1, 1, 1, polar=True)
        self.canvas_polar = FigureCanvasTkAgg(self.figure_bar, self.fig_frame)
        # self.canvas_polar.get_tk_widget().grid(row=1, column=1)
        self.plot_pol.set_ylim([90,0])
        self.plot_pol.set_theta_zero_location('N')
        self.plot_pol.set_theta_direction(-1)

        frame = ttk.Frame(self.root)
        self.init_gnss_icon(frame)
        frame.grid(row=0, column=0, columnspan=1, padx=10, pady=10, sticky="en")

        self.root.grid_rowconfigure(1, weight=1)
        self.root.grid_columnconfigure(1, weight=1)

    def init_gnss_icon(self, frame):
        width_value = 20
        height_value = 20

        label1 = tk.Label(frame, text="GPS")
        label1.grid(row=2, column=0, sticky='w')
        canvas1 = tk.Canvas(frame, width=width_value, height=height_value)
        canvas1.grid(row=2, column=1, sticky='e')
        circle1 = canvas1.create_oval(5, 5, 15, 15, fill=self.colors.get_color_sat("G"))

        label2 = tk.Label(frame, text="GLONASS")
        label2.grid(row=3, column=0, sticky='w')
        canvas2 = tk.Canvas(frame, width=width_value, height=height_value)
        canvas2.grid(row=3, column=1, sticky='e')
        circle2 = canvas2.create_oval(5, 5, 15, 15, fill=self.colors.get_color_sat("R"))

        label3 = tk.Label(frame, text="SBAS")
        label3.grid(row=4, column=0, sticky='w')
        canvas3 = tk.Canvas(frame, width=width_value, height=height_value)
        canvas3.grid(row=4, column=1, sticky='e')
        circle3 = canvas3.create_oval(5, 5, 15, 15, fill=self.colors.get_color_sat("S"))

        label4 = tk.Label(frame, text="GALILEO")
        label4.grid(row=5, column=0, sticky='w')
        canvas4 = tk.Canvas(frame, width=width_value, height=height_value)
        canvas4.grid(row=5, column=1, sticky='e')
        circle4 = canvas4.create_oval(5, 5, 15, 15, fill=self.colors.get_color_sat("E"))

        label5 = tk.Label(frame, text="BEIDOU")
        label5.grid(row=6, column=0, sticky='w')
        canvas5 = tk.Canvas(frame, width=width_value, height=height_value)
        canvas5.grid(row=6, column=1, sticky='e')
        circle5 = canvas5.create_oval(5, 5, 15, 15, fill=self.colors.get_color_sat("C"))

        label6 = tk.Label(frame, text="NOT RECOGNISED")
        label6.grid(row=7, column=0, sticky='w')
        canvas6 = tk.Canvas(frame, width=width_value, height=height_value)
        canvas6.grid(row=7, column=1, sticky='e')
        circle6 = canvas6.create_oval(5, 5, 15, 15, fill=self.colors.get_color_sat("N"))

        self.selected_format = tk.StringVar()
        self.selected_format.set("bar")

        self.switch_fig_frame = ttk.Frame(frame)
        self.plot_format_bar   = ttk.Radiobutton(self.switch_fig_frame, text="bar",   variable = self.selected_format, value="bar", command=self.update_bar_for_change,  bootstyle="dark-outline-toolbutton", width=8)
        self.plot_format_polar = ttk.Radiobutton(self.switch_fig_frame, text="polar", variable = self.selected_format, value="polar",command=self.update_bar_for_change,  bootstyle="dark-outline-toolbutton", width=8)
        self.plot_format_bar.grid(row=0, column=0, sticky="ew")
        self.plot_format_polar.grid(row=0, column=1, sticky="ew")
        self.switch_fig_frame.grid(row=0, column=0, pady=5, columnspan=2, sticky="ew")

        self.combo_sats = ttk.Combobox(frame, bootstyle="dark")
        self.combo_sats['values'] = ["ALL" , "GPS", "GLONASS", "SBAS", "GALILEO", "BEIDOU", "NOT RECOGNISED"]
        self.combo_sats.current(0)
        self.combo_sats.config (width=16)
        self.combo_sats.grid(row=1, column=0, padx=0, pady=0, columnspan=2,sticky="ew")

    def set_printdata_callback(self, get_data_callback):
        self._get_data_for_print = get_data_callback

    def _update_data_thread(self):
        try:
            while self.is_running:
                data = self._get_data_for_print()
                self.update_bar(data)
                # self.update_polar(structures)
                self.event.wait(timeout=1)
                self.event.clear()
        except Exception as e:
            print("ERROR Plotter thread:", e)
            self.is_running = False
            self.root.destroy()

    def start_plotter(self, parrent, get_satdata):
        try:
            if self.is_running:
                print("Plotter уже запущен.")
                return
            self.colors = self.Colors()
            self.root = tk.Toplevel(parrent)
            self.root.iconbitmap("resources/image/Pi_sun_w_1_ico.ico")
            self.root.resizable(False, False)
            self.root.protocol("WM_DELETE_WINDOW", self.on_close)
            self.init_plot()
            self.set_printdata_callback(get_satdata)
            self.is_running = True
            self._update_thread = threading.Thread(target=self._update_data_thread, name="Plotter")
            self._update_thread.start()
        except Exception as e:
            print("ERROR Start plotter thread:", e)
            self.is_running = False

    def on_close(self):
        self.stop_plotter()

    def stop_plotter(self):
        def stop_plotter_async():
            if self.is_running:
                self.is_running = False
                self.event.set()
                self._update_thread.join()
                self.root.destroy()
        stop_thread = threading.Thread(target=stop_plotter_async, name="stop_plotter")
        stop_thread.start()

    def update_bar_for_change(self):
        self.update_bar(self._local_data)

    def update_bar(self, data: for_print):
        temp_data = for_print()
        if self.combo_sats.get()   == "ALL":                search_obj = ""
        elif self.combo_sats.get() == "GPS":                search_obj = "G"
        elif self.combo_sats.get() == "GLONASS":            search_obj = "R"
        elif self.combo_sats.get() == "SBAS":               search_obj = "S"
        elif self.combo_sats.get() == "GALILEO":            search_obj = "E"
        elif self.combo_sats.get() == "BEIDOU":             search_obj = "C"
        elif self.combo_sats.get() == "NOT RECOGNISED":     search_obj = "N"
        else:
            search_obj = ""
        if search_obj == "":
            temp_data = data
        else:
            for i in range(len(data.name_color)):
                if data.name_color[i] == search_obj:
                    temp_data.name.append(data.name[i])
                    temp_data.azimut.append(data.azimut[i])
                    temp_data.elevation.append(data.elevation[i])
                    temp_data.snr.append(data.snr[i])
                    temp_data.freq_num.append(data.freq_num[i])
                    temp_data.name_color.append(data.name_color[i])

        self._local_data = data
        self.plot_bar.clear()
        colors_bar = []
        colors_name = []
        new_list = []

        if self._previos_fig != self.selected_format.get():
            self.canvas_bar.get_tk_widget().grid_forget()
            if self.selected_format.get() == "bar":
                self.figure_bar = Figure(figsize=(self._size_bar), dpi=100)
                self.plot_bar = self.figure_bar.add_subplot(1, 1, 1, polar=False)
                self.canvas_bar = FigureCanvasTkAgg(self.figure_bar, self.fig_frame)
                self.canvas_bar.get_tk_widget().grid(row=1, column=0)
            else:
                self.figure_bar = Figure(figsize=(self._size_polar), dpi=100)
                self.plot_bar = self.figure_bar.add_subplot(1, 1, 1, polar=True)
                self.canvas_bar = FigureCanvasTkAgg(self.figure_bar, self.fig_frame)
                self.canvas_bar.get_tk_widget().grid(row=1, column=0)
        self._previos_fig = self.selected_format.get()

        if self.selected_format.get() == "bar":
            for freq_n in temp_data.freq_num:
                colors_bar.append(self.colors.get_color_freq(freq_n))

            for name_color in temp_data.name_color:
                colors_name.append(self.colors.get_color_sat(name_color))

            calc_width = len(temp_data.name) / 60
            if calc_width >= 0.9:
                calc_width = 0.9

            self.plot_bar.bar(temp_data.name, temp_data.snr, color=colors_bar, width=calc_width, zorder=0)
            self.plot_bar.margins(x=0.01)
            self.plot_bar.grid(axis='y', linestyle='--', zorder=1)
            self.plot_bar.set_xticks(temp_data.name)

            self.plot_bar.set_xticklabels(temp_data.name, rotation=90)
            self.plot_bar.set_ylim(0, 60)
            for color, tick in zip(colors_name, self.plot_bar.get_xticklabels()):
                tick.set_color(color)
            plt.rcParams.update({'font.size': 9})
            self.figure_bar.tight_layout(pad=1)
            self.canvas_bar.draw()

        else:
            colors_name = []
            for name_color in temp_data.name_color:
                colors_name.append(self.colors.get_color_sat(name_color))
            self.plot_bar.scatter(np.deg2rad(temp_data.azimut), temp_data.elevation, color=colors_name)
            self.plot_bar.set_ylim([90, 0])
            self.plot_bar.set_theta_zero_location('N')
            self.plot_bar.set_theta_direction(-1)
            self.plot_bar.set_yticklabels([])
            self.figure_bar.tight_layout(pad=1)
            self.canvas_bar.draw()