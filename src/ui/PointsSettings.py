import math
import threading
import tkinter as tk
import ttkbootstrap as ttk
from src.services import Controller
from src.utils.Console import print


class PointsSettings:
    get_point_callback = None

    def __init__(self):
        self.cont = Controller.Controller()
        self.items_container = {}
        self.is_running = False
        self.run_second_window = False
        self.select_point = None
        self.count_page = None
        self.current_page = 0
        self.files_item_counter = 0
        self.MAX_FILES_TO_DISPLAY = 3
        self.icon_delete = tk.PhotoImage(file="resources/image/icons8.png")
        self.icon_select = tk.PhotoImage(file="resources/image/icon_select.png")
        self.icon_right_arrow = tk.PhotoImage(file="resources/image/icon_right_arrow.png")
        self.icon_left_arrow = tk.PhotoImage(file="resources/image/icon_left_arrow.png")
        self.icons_edit = tk.PhotoImage(file="resources/image/icons_edit.png")
        self.icon_cloud = tk.PhotoImage(file="resources/image/icon_cloud.png")
        self.icon_search = tk.PhotoImage(file="resources/image/icon_search.png")
        self.icon_cancel = tk.PhotoImage(file="resources/image/icon_cancel.png")

    def update_save_point(self):
        try:
            self.destroy_window()
            self.update_window()
        except Exception:
            print(f"open the window with points")

    def get_select_points(self):
        list_coords = []
        list_point = self.cont.get_list_points()
        for item in list_point:
            if item["uuid"] == self.select_point:
                try:
                    name = item["name"]
                    list_coords.append(item["data"]["lat"])
                    list_coords.append(item["data"]["lon"])
                    list_coords.append(item["data"]["height"])
                except Exception as e:
                    print(f"Error input point: {e}")
                return list_coords, name

    def set_input_point(self, select_point):
        self.select_point = select_point
        point_coords, name = self.get_select_points()
        self.label1.delete(0, "end")
        self.label1.insert(0, point_coords[0])
        self.label2.delete(0, "end")
        self.label2.insert(0, point_coords[1])
        self.label3.delete(0, "end")
        self.label3.insert(0, point_coords[2])
        self.point.delete(0, 'end')
        self.point.insert(0, name)

    def get_edit_point(self):
        coord1 = self.label1.get()
        coord2 = self.label2.get()
        coord3 = self.label3.get()
        if coord1 == "":
            coord1 = "0.0"
        if coord2 == "":
            coord2 = "0.0"
        if coord3 == "":
            coord3 = "0.0"
        try:
            coord1 = float(coord1)
        except ValueError:
            print("Ошибка: Невозможно преобразовать Lat в число")
            coord1 = 0.0
        try:
            coord2 = float(coord2)
        except ValueError:
            print("Ошибка: Невозможно преобразовать Lon в число")
            coord2 = 0.0
        try:
            coord3 = float(coord3)
        except ValueError:
            print("Ошибка: Невозможно преобразовать Heigh в число")
            coord3 = 0.0

        coords = [float(coord1), float(coord2), float(coord3)]
        point_name = self.point.get()
        return point_name, coords

    def on_click_open_window(self):
        if self.is_running:
            print("Окно уже открыто")
            return
        self.show_window()

    def on_click_close_window(self):
        def on_window_closing_async():
            if self.is_running:
                self.is_running = False
            self.items_container.clear()
            self.files_item_counter = 0
            self.popup.destroy()
            return

        closing_thread = threading.Thread(
            target=on_window_closing_async, name="on_window_closing_async"
        )
        closing_thread.start()

    def on_click_open_second_window(self, id_point):
        try:
            if self.run_second_window:
                print("Окно уже открыто")
                return
            self.create_second_window()
            self.set_input_point(id_point)
        except Exception as e:
            self.run_second_window = True
            print(f"Error open points window: {e}")

    def on_click_close_second_window(self):
        def on_window_close():
            if self.run_second_window:
                self.run_second_window = False
            self.window.destroy()
            return

        closing_thread = threading.Thread(
            target=on_window_close, name="on_window_close"
        )
        closing_thread.start()

    def on_click_save_edit_point(self):
        try:
            point_name, coords = self.get_edit_point()
            coords_dict = {
                "lat": coords[0],
                "lon": coords[1],
                "height": coords[2]
            }
            self.cont.update_point(self.select_point, point_name, coords_dict)
            self.run_second_window = False
            self.window.destroy()
            self.destroy_window()
            self.update_window()
        except Exception as e:
            print(f"Error edit point: {e}")

    def on_click_delete_point(self, id_point):
        try:
            self.cont.delete_point(id_point)
            self.destroy_window()
            self.update_window()
        except Exception as e:
            print(f"Error delete point: {e}")

    def on_click_input_points(self, point):
        self.select_point = point
        self.get_point_callback()

    def on_click_sync(self):
        try:
            sunc_point = self.cont.sync_points()
            self.destroy_window()
            self.update_window(sunc_point)
        except Exception as e:
            print(f"Error sync point: {e}")

    def on_click_searche_points(self):
        try:
            found_point = self.entry_searche.get()
            list_points = self.cont.get_list_points()
            new_list = []
            for item in list_points:
                if found_point in item["name"]:
                    new_list.append(item)
            self.destroy_window()
            self.update_window(new_list)
            if len(new_list) == 0:
                print("No result")
        except Exception as e:
            print(f"Search error {e}")

    def close_searche_window(self):
        self.destroy_window()
        self.update_window()

    def destroy_window(self):
        self.items_container.clear()
        self.frm2.destroy()
        self.frm2 = ttk.Frame(self.frm, borderwidth=1)
        self.frm2.grid(row=1, column=0, padx=5, pady=1, sticky="ns", columnspan=1)

    def create_second_window(self):
        self.window = tk.Toplevel()
        self.window.title(f"PiSun Cors {self.cont.get_firmware_version()}")
        self.window.iconbitmap("resources/image/Pi_sun_w_1_ico.ico")
        self.window.protocol("WM_DELETE_WINDOW", self.on_click_close_second_window)
        window_width = 360
        window_height = 450
        screen_width = self.window.winfo_screenwidth()
        screen_height = self.window.winfo_screenheight()

        x_cordinate = int((screen_width / 2) - (window_width / 2))
        y_cordinate = int((screen_height / 2) - (window_height / 2))

        self.window.geometry("{}x{}+{}+{}".format(window_width, window_height, x_cordinate, y_cordinate))

        self.point_name = tk.Label(self.window, text="Name point")
        self.point_name.pack(padx=5, pady=5, anchor="n")
        self.point = ttk.Entry(self.window, bootstyle="dark")
        self.point.pack(padx=5, pady=5, fill="x")

        self.coord1 = tk.Label(self.window, text="Latitude", bg="#e9e9e9")
        self.coord1.pack(padx=5, pady=5, anchor="n")
        self.label1 = ttk.Entry(self.window, bootstyle="dark")
        self.label1.pack(padx=5, pady=5, fill="x")

        self.coord2 = tk.Label(self.window, text="Longitude", bg="#e9e9e9")
        self.coord2.pack(padx=5, pady=5, anchor="n")
        self.label2 = ttk.Entry(self.window, bootstyle="dark")
        self.label2.pack(padx=5, pady=5, fill="x")

        self.coord3 = tk.Label(self.window, text="Height", bg="#e9e9e9")
        self.coord3.pack(padx=5, pady=5, anchor="n")
        self.label3 = ttk.Entry(self.window, bootstyle="dark")
        self.label3.pack(padx=5, pady=5, fill="x")

        self.btn_save_point = ttk.Button(self.window, text="Save change", width=5, command=self.on_click_save_edit_point,
                                         bootstyle="dark-outline")
        self.btn_save_point.pack(padx=5, pady=10, fill="x")

        self.run_second_window = True

    def show_window(self):
        self.popup = tk.Toplevel()
        self.popup.title(f"PiSun Cors {self.cont.get_firmware_version()}")
        self.popup.iconbitmap("resources/image/Pi_sun_w_1_ico.ico")
        self.popup.protocol("WM_DELETE_WINDOW", self.on_click_close_window)
        self.popup.update()

        win_width = 433
        win_height = 530
        screen_width = self.popup.winfo_screenwidth()
        screen_height = self.popup.winfo_screenheight()
        x = int((screen_width / 2) - (win_width / 2))
        y = int((screen_height / 2) - (win_height / 2))
        geometry_str = f"{win_width}x{win_height}+{x}+{y}"
        self.popup.geometry(geometry_str)
        self.popup.resizable(True, True)

        self.frm = ttk.Frame(self.popup, borderwidth=1)
        self.frm.grid(row=0, column=0, padx=5, pady=5, sticky="ns", columnspan=1)

        self.frm1 = ttk.Frame(self.frm, borderwidth=1)
        self.frm1.grid(row=0, column=0, padx=5, pady=1, sticky="ns", columnspan=1)

        self.frm2 = ttk.Frame(self.frm, borderwidth=1)
        self.frm2.grid(row=1, column=0, padx=5, pady=1, sticky="ns", columnspan=1)

        self.entry_searche = ttk.Entry(self.frm1, bootstyle="dark")
        self.entry_searche.grid(row=0, column=0, padx=2, pady=5, sticky="we", columnspan=2)
        btn_searche = ttk.Button(self.frm1, image=self.icon_search, bootstyle="light-outline", command=self.on_click_searche_points)
        btn_searche.grid(row=0, column=2, padx=2, pady=5, sticky="we", columnspan=1)
        btn_close = ttk.Button(self.frm1, image=self.icon_cancel, bootstyle="light-outline", command=self.close_searche_window)
        btn_close.grid(row=0, column=3, padx=2, pady=5, sticky="we", columnspan=1)

        button_2 = ttk.Button(self.frm1, image=self.icon_left_arrow, bootstyle="light-outline", command=self.go_back)
        button_2.grid(row=1, column=0, padx=2, pady=1, sticky="we", columnspan=1)
        self.label_count_page = tk.Label(self.frm1, text=f"{(self.current_page + 1)} / {self.count_page}", bg="#e9e9e9", width=15, wraplength=50)
        self.label_count_page.grid(row=1, column=1, padx=5, pady=5, sticky="e")
        button_1 = ttk.Button(self.frm1, image=self.icon_right_arrow, bootstyle="light-outline", command=self.go_forward)
        button_1.grid(row=1, column=2, padx=2, pady=1, sticky="we", columnspan=1)

        sync_btn = ttk.Button(self.frm1, image=self.icon_cloud, bootstyle="light-outline", command=self.on_click_sync)
        sync_btn.grid(row=1, column=3, padx=2, pady=1, sticky="we", columnspan=1)

        self.update_window()
        self.is_running = True

    def update_window(self, val=None):
        if val is None:
            val = self.cont.get_list_points()
        data_list = list(reversed(val))

        self.count_page = math.ceil(len(data_list) / self.MAX_FILES_TO_DISPLAY)
        self.label_count_page.config(text=f"{(self.current_page + 1)} / {self.count_page}")

        chunks = [data_list[i:i + self.MAX_FILES_TO_DISPLAY] for i in
                  range(0, len(data_list), self.MAX_FILES_TO_DISPLAY)]

        if len(chunks) == 0:
            return

        for item in chunks[self.current_page]:
            if item["deleted_at"] == 1:
                continue
            if not item["uuid"] in self.items_container:
                if not hasattr(self, 'frm2') or not self.frm2.winfo_exists():
                    self.frm2 = ttk.Frame(self.frm2, borderwidth=1)
                    self.frm2.grid(row=1, column=0, padx=5, pady=1, sticky="ns", columnspan=1)
                self.draw_new_point(item["uuid"], item["name"], item["data"]["lat"], item["data"]["lon"],
                                    item["data"]["height"])
                continue

    def draw_new_point(self, uuid, point_name, lat, lon, height):
        if uuid in self.items_container:
            print("Error")

        labelfrm = ttk.Labelframe(self.frm2, text="", bootstyle="dark")
        labelfrm.columnconfigure([2, 3], minsize=150, weight=1)
        labelfrm.grid(row=self.files_item_counter, column=0, padx=10, pady=10, sticky="we", columnspan=1)

        name_lb = ttk.Label(labelfrm, text=f"Name point: {point_name}", anchor="w", wraplength=160, bootstyle="dark")
        name_lb.grid(row=0, column=0, padx=5, pady=5, sticky="we", columnspan=3)

        input_lat = ttk.Label(labelfrm, text=f"Lat: {lat}", bootstyle="dark", anchor="w", wraplength=160)
        input_lat.grid(row=0, column=3, padx=5, pady=5, sticky="we")

        input_lon = ttk.Label(labelfrm, text=f"Lon: {lon}", bootstyle="dark", anchor="w", wraplength=160)
        input_lon.grid(row=1, column=3, padx=5, pady=5, sticky="we")

        input_height = ttk.Label(labelfrm, text=f"H: {height}", bootstyle="dark", anchor="w", wraplength=160)
        input_height.grid(row=2, column=3, padx=5, pady=5, sticky="we")

        self.btn_select = ttk.Button(labelfrm, image=self.icon_select,
                                     command=lambda i=uuid: self.on_click_input_points(i), bootstyle="light-outline")
        self.btn_select.grid(row=1, column=1, padx=0, pady=1, sticky="w")
        self.btn_select.configure(width=1)

        self.btn_delete = ttk.Button(labelfrm, image=self.icon_delete,
                                     command=lambda i=uuid: self.on_click_delete_point(i), bootstyle="light-outline")
        self.btn_delete.grid(row=1, column=0, padx=0, pady=1, sticky="w")
        self.btn_delete.configure(width=1)

        self.btn_edit = ttk.Button(labelfrm, image=self.icons_edit,
                                   command=lambda i=uuid: self.on_click_open_second_window(i),
                                   bootstyle="light-outline")
        self.btn_edit.grid(row=1, column=2, padx=0, pady=1, sticky="w")
        self.btn_edit.configure(width=1)

        self.items_container[uuid] = {"point": point_name, "lat": lat, "lon": lon, "height": height}
        self.files_item_counter += 1

    def go_forward(self):
        if self.current_page < self.count_page - 1:
            self.current_page += 1
            self.destroy_window()
            self.update_window()

    def go_back(self):
        if self.current_page > 0:
            self.current_page -= 1
            self.destroy_window()
            self.update_window()
