import tkinter as tk
import ttkbootstrap as ttk
from src.services import Controller


class RoverSettings:
    is_running = False
    get_list_base =None

    def __init__(self):
        pass

    def init_settings(self, frame: ttk.Frame):
        self.cont = Controller.Controller()
        self.frame = frame
        self.is_running = True

        self.host_label = tk.Label(self.frame, text="Host", bg="#e9e9e9")
        self.host_name = ttk.Entry(self.frame, bootstyle="dark")
        self.host_label.grid(row=0, column=0, padx=5, pady=5, sticky="e")
        self.host_name.grid(row=0, column=1, padx=5, pady=5, sticky="we")

        self.port_label = tk.Label(self.frame, text="Port", bg="#e9e9e9")
        self.port_name = ttk.Entry(self.frame, bootstyle="dark")
        self.port_label.grid(row=0, column=2, padx=5, pady=5, sticky="e")
        self.port_name.grid(row=0, column=3, padx=5, pady=5, sticky="we")

        self.login_label = tk.Label(self.frame, text="Login", bg="#e9e9e9")
        self.login = ttk.Entry(self.frame, bootstyle="dark")
        self.login_label.grid(row=1, column=0, padx=5, pady=5, sticky="e")
        self.login.grid(row=1, column=1, padx=5, pady=5, sticky="we")

        self.password_label = tk.Label(self.frame, text="Password", bg="#e9e9e9")
        self.password = ttk.Entry(self.frame, bootstyle="dark")
        self.password_label.grid(row=1, column=2, padx=5, pady=5, sticky="e")
        self.password.grid(row=1, column=3, padx=5, pady=5, sticky="we")

        self.list_base = ttk.Combobox(self.frame, bootstyle="dark")
        self.list_base.config(postcommand=self.on_click_select_base)
        self.list_base.insert(0, "Select basestation")
        self.list_base.grid(row=0, column=4, padx=5, pady=5, sticky="we")

        self.btn_connect = ttk.Button(self.frame, text="Connect", width=15, command=self.on_click_unload_base, bootstyle="dark")
        self.btn_connect.grid(row=1, column=4, padx=5, pady=5, sticky="we")

        self.status_work_rover = tk.IntVar()
        self.status_work_rover.set(0)
        self.check_btn_start_rover = ttk.Checkbutton(self.frame, text="Start RTK", variable=self.status_work_rover, bootstyle="dark-outline-toolbutton")
        self.check_btn_start_rover.grid(row=2, column=4, padx=5, pady=5, sticky="we")

    def on_click_select_base(self):
        if self.get_list_base is not None:
            base = self.get_list_base()
            self.list_base.config(values=base)
            self.list_base.state(["readonly"])
        else:
            print("ERROR callback is none")

    def on_click_unload_base(self):
        host = self.get_host()
        port = self.get_port()
        login = self.get_login()
        pwd = self.get_password()
        # self.cont.connect_to_table(host, port, login, pwd)

    def get_select_basestation(self):
        val = self.list_base.get()
        try:
            if val == "Select basestation":
                return ""
            elif len(val) == 0:
                raise ValueError("Zero value")
            result = str(val)
        except Exception as e:
            result = "none"
        return result

    def get_status_work_rover(self):
        return False if self.status_work_rover.get() == 0 else True

    def set_status_work_rover(self, val: bool):
        self.status_work_rover.set(0) if not val else self.status_work_rover.set(1)

    def get_host(self):
        value = self.host_name.get()
        try:
            if len(value) == 0:
                raise ValueError("Zero value")
            result = str(value)
        except Exception as e:
            result = ""
        return result

    def get_port(self):
        value = self.port_name.get()
        try:
            if len(value) == 0:
                raise ValueError("Zero value")
            result = int(value)
        except Exception as e:
            result = 0
        return result

    def get_login(self):
        value = self.login.get()
        try:
            if len(value) == 0:
                raise ValueError("Zero value")
            result = str(value)
        except Exception as e:
            result = ""
        return result

    def get_password(self):
        value = self.password.get()
        try:
            if len(value) == 0:
                raise ValueError("Zero value")
            result = str(value)
        except Exception as e:
            result = ""
        return result

    def set_host(self, host_name):
        self.host_name.delete(0, "end")
        self.host_name.insert(0, host_name)

    def set_port(self, port_name):
        self.port_name.delete(0, "end")
        self.port_name.insert(0, port_name)

    def set_login(self, login):
        self.login.delete(0, "end")
        self.login.insert(0, login)
