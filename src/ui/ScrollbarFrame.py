from tkinter import *


class ScrollableFrame:

    def __init__(self, master, width, height, mousescroll=True):
        self.mousescroll = mousescroll
        self.master = master
        self.height = height
        self.width = width
        self.main_frame = Frame(self.master)
        self.main_frame.pack(fill=BOTH, expand=1)

        self.scrollbar = Scrollbar(self.main_frame, orient=VERTICAL)
        self.scrollbar.pack(side=RIGHT, fill=Y)

        self.canvas = Canvas(self.main_frame, yscrollcommand=self.scrollbar.set)
        self.canvas.pack(expand=True, fill=BOTH)
        self.canvas.bind_all("<MouseWheel>", self._on_mouse_wheel)

        self.scrollbar.config(command=self.canvas.yview)

        self.canvas.bind('<Configure>', lambda e: self.canvas.configure(scrollregion=self.canvas.bbox("all")))

        self.frame = Frame(self.canvas, width=self.width, height=self.height)
        self.frame.pack(expand=True, fill=BOTH)
        self.canvas.create_window((0, 0), window=self.frame, anchor="nw")

    def _on_mouse_wheel(self, event):
        if self.mousescroll and self.master.winfo_height() != 930:
            self.canvas.yview_scroll(-1 * int((event.delta / 120)), "units")