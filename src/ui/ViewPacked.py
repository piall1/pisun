import math
import threading
import tkinter as tk
import ttkbootstrap as ttk
from src.services import Controller
from src.services import CSController
from src.utils.Console import print


class CSCardView:

    def __init__(self):
        self.cont = Controller.Controller()
        self.cont_cs = CSController.ControllerCS()

        self.items_container = {}
        self.labels_entries_1025 = []
        self.select_id = None
        self.is_open_window = False
        self.count_page = None
        self.current_page = 0
        self.files_item_counter = 0
        self.MAX_FILES_TO_DISPLAY = 4

        self.callback_func_set_params = None

        self.icon_right_arrow = tk.PhotoImage(file="resources/image/icon_right_arrow.png")
        self.icon_left_arrow = tk.PhotoImage(file="resources/image/icon_left_arrow.png")
        self.icon_delete = tk.PhotoImage(file="resources/image/icons8.png")
        self.icon_select = tk.PhotoImage(file="resources/image/icon_select.png")
        self.icons_edit = tk.PhotoImage(file="resources/image/icons_edit.png")

    def on_click_update_window(self):
        try:
            self.destroy_window()
            self.update_window()
        except Exception:
            print(f"open the window with SC")

    def on_click_close_window(self):
        def on_window_closing_async():
            if self.is_open_window:
                self.is_open_window = False
            self.root.destroy()
            return

        closing_thread = threading.Thread(
            target=on_window_closing_async, name="on_window_closing_async"
        )
        closing_thread.start()

    def on_click_delete_packed(self, id):
        self.cont_cs.delete_coords_system(id)
        self.on_click_update_window()

    def on_click_select_packed(self, id):
        self.select_id = id
        self.callback_func_set_params()

    def get_selected_sc(self):
        list_params = self.cont_cs.get_list_coords_system()
        for item in list_params:
            if item["uuid"] == self.select_id:
                return item

    def go_forward(self):
        if self.current_page < self.count_page - 1:
            self.current_page += 1
            self.destroy_window()
            self.update_window()

    def go_back(self):
        if self.current_page > 0:
            self.current_page -= 1
            self.destroy_window()
            self.update_window()

    def destroy_window(self):
        self.items_container.clear()
        self.frm2.destroy()
        self.frm2 = ttk.Frame(self.frm, borderwidth=1)
        self.frm2.grid(row=1, column=0, padx=5, pady=1, sticky="ns", columnspan=1)

    def show_window(self):
        if self.is_open_window:
            print("Окно уже открыто")
            return
        self.root = tk.Toplevel()
        self.root.title(f"PiSun Cors {self.cont.get_firmware_version()}")
        self.root.iconbitmap("resources/image/Pi_sun_w_1_ico.ico")
        self.root.protocol("WM_DELETE_WINDOW", self.on_click_close_window)
        self.root.update()

        win_width = 400
        win_height = 550
        screen_width = self.root.winfo_screenwidth()
        screen_height = self.root.winfo_screenheight()
        x = int((screen_width / 2) - (win_width / 2))
        y = int((screen_height / 2) - (win_height / 2))
        geometry_str = f"{win_width}x{win_height}+{x}+{y}"
        self.root.geometry(geometry_str)
        self.root.resizable(True, True)

        self.frm = ttk.Frame(self.root, borderwidth=1)
        self.frm.grid(row=0, column=0, padx=5, pady=5, sticky="ns", columnspan=1)

        self.frm1 = ttk.Frame(self.frm, borderwidth=1)
        self.frm1.grid(row=0, column=0, padx=5, pady=1, sticky="ns", columnspan=1)

        self.frm2 = ttk.Frame(self.frm, borderwidth=1)
        self.frm2.grid(row=1, column=0, padx=5, pady=1, sticky="ns", columnspan=1)

        button_2 = ttk.Button(self.frm1, image=self.icon_left_arrow, bootstyle="light-outline", command=self.go_back)
        button_2.grid(row=1, column=0, padx=2, pady=1, sticky="we", columnspan=1)
        self.label_count_page = tk.Label(self.frm1, text=f"{(self.current_page + 1)} / {self.count_page}", bg="#e9e9e9", width=15, wraplength=50)
        self.label_count_page.grid(row=1, column=1, padx=5, pady=5, sticky="e")
        button_1 = ttk.Button(self.frm1, image=self.icon_right_arrow, bootstyle="light-outline", command=self.go_forward)
        button_1.grid(row=1, column=2, padx=2, pady=1, sticky="we", columnspan=1)

        self.update_window()
        self.is_open_window = True

    def update_window(self, val=None):
        if val is None:
            val = self.cont_cs.get_list_coords_system()
        data_list = list(reversed(val))

        self.count_page = math.ceil(len(data_list) / self.MAX_FILES_TO_DISPLAY)
        self.label_count_page.config(text=f"{(self.current_page + 1)} / {self.count_page}")

        chunks = [data_list[i:i + self.MAX_FILES_TO_DISPLAY] for i in
                  range(0, len(data_list), self.MAX_FILES_TO_DISPLAY)]

        if len(chunks) == 0:
            return

        for item in chunks[self.current_page]:
            if not item["uuid"] in self.items_container:
                if not hasattr(self, 'frm2') or not self.frm2.winfo_exists():
                    self.frm2 = ttk.Frame(self.frm2, borderwidth=1)
                    self.frm2.grid(row=1, column=0, padx=5, pady=1, sticky="ns", columnspan=1)

                self.draw_new_packed(item["uuid"], item["Datum"], item["SourceName"], item["TargetName"])
                continue

    def draw_new_packed(self, uuid, datum, source_name, target_name):
        if uuid in self.items_container:
            print("Error")

        labelfrm = ttk.Labelframe(self.frm2, text="", bootstyle="dark")
        labelfrm.columnconfigure([2, 3], minsize=100, weight=1)
        labelfrm.grid(row=self.files_item_counter, column=0, padx=10, pady=10, sticky="we", columnspan=1)

        label_name = ttk.Label(labelfrm, text=f"Datum: {datum}", anchor="w", wraplength=160, bootstyle="dark")
        label_name.grid(row=0, column=0, padx=5, pady=5, sticky="we", columnspan=3)

        label_source = ttk.Label(labelfrm, text=f"SourceName: {source_name}", bootstyle="dark", anchor="w", wraplength=160)
        label_source.grid(row=0, column=3, padx=5, pady=5, sticky="we")

        label_target = ttk.Label(labelfrm, text=f"TargetName: {target_name}", bootstyle="dark", anchor="w", wraplength=160)
        label_target.grid(row=1, column=3, padx=5, pady=5, sticky="we")

        self.btn_select = ttk.Button(labelfrm, image=self.icon_select, command=lambda i=uuid: self.on_click_select_packed(i), bootstyle="light-outline")
        self.btn_select.grid(row=1, column=1, padx=0, pady=1, sticky="w")
        self.btn_select.configure(width=1)

        self.btn_delete = ttk.Button(labelfrm, image=self.icon_delete, command=lambda i=uuid: self.on_click_delete_packed(i), bootstyle="light-outline")
        self.btn_delete.grid(row=1, column=0, padx=0, pady=1, sticky="w")
        self.btn_delete.configure(width=1)

        self.items_container[uuid] = {"Datum": datum, "SourceName": source_name, "TargetName":target_name}
        self.files_item_counter += 1