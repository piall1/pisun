import os
import platform
import time
from src.utils.Console import print


class Buzzer:
    is_connect = False
    pi = None
    pigpio = None

    def __init__(self) -> None:
        self.pin = 12

        self.mode_error = [(300, 50), (300, 50), (300, 50)]

        self.mode_start = [(2000, 25), (0, 200), (2000, 50), (2500, 50), (0, 200),
                           (2000, 50), (2500, 50), (0, 200), (2000, 50), (2500, 50)]

        self.mode_stop = [(2500, 25), (0, 200), (2500, 50), (2000, 50), (0, 200),
                          (2500, 50), (2000, 50), (0, 200), (2500, 50), (2000, 50)]

    def connect(self):
        if self.is_connect:
            return self.is_connect
        try:
            self.start_demon_pigpio()
            time.sleep(3)
            import pigpio
            self.pigpio = pigpio
        except ModuleNotFoundError:
            print(f"Модуль для проигрывания сигнала не поддерживается на вашем устройстве.")
            self.is_connect = False
            return self.is_connect

        if self.pigpio is None:
            print("Не удалось подключиться: модуль pigpio не доступен.")
            self.is_connect = False
            return self.is_connect

        self.pi = self.pigpio.pi()
        self.is_connect = True

        if not self.pi.connected:
            print("Не удалось подключиться к демону pigpio.")
            self.is_connect = False
            return self.is_connect

        return self.is_connect

    def start_buzzer(self):
        # звук при старте
        if self.pi is None or not self.pi.connected:
            print("Buzzer не может быть запущен, так как не установлено соединение с pigpio.")
            return

        try:
            self.pi.set_mode(self.pin, self.pigpio.OUTPUT)
            self.play_buzzer(self.mode_start)
            self.pi.write(self.pin, 0)
            self.is_connect = False
        except Exception as e:
            print(f"Ошибка во время запуска buzzer: {e}")
            self.stop_demon_pigpio()
            self.pi.stop()

    def stop_buzzer(self):
        # звук при остановки
        if self.pi is None or not self.pi.connected:
            print("Buzzer не может быть запущен, так как не установлено соединение с pigpio.")
            return

        try:
            self.pi.set_mode(self.pin, self.pigpio.OUTPUT)
            self.play_buzzer(self.mode_stop)
            self.pi.write(self.pin, 0)
            self.stop_demon_pigpio()
            self.is_connect = False
        except Exception as e:
            print(f"Ошибка во время запуска buzzer: {e}")
            self.stop_demon_pigpio()
        finally:
            self.pi.stop()

    def error_buzzer(self):
        # звук при ошибки
        if self.pi is None or not self.pi.connected:
            print("Buzzer не может быть запущен, так как не установлено соединение с pigpio.")
            return

        try:
            self.pi.set_mode(self.pin, self.pigpio.OUTPUT)
            self.play_buzzer(self.mode_error)
            self.pi.write(self.pin, 0)
            self.stop_demon_pigpio()
        except Exception as e:
            print(f"Ошибка во время запуска buzzer: {e}")
            self.stop_demon_pigpio()
        finally:
            self.pi.stop()

    def play_buzzer(self, mode):
        for freq, duration in mode:
            self.pi.hardware_PWM(self.pin, freq, 500000)
            time.sleep(duration / 1000.0)
            self.pi.hardware_PWM(self.pin, 0, 0)
            time.sleep(0.04)

    def start_demon_pigpio(self):
        if platform.system() == "Linux":
            os.system("sudo pigpiod")
        else:
            raise SystemError("Buzzer не запущен")

    def stop_demon_pigpio(self):
        if platform.system() == "Linux":
            os.system("sudo killall pigpiod")
        else:
            raise SystemError("Buzzer не остановлен")