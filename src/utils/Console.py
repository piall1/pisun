
original_print = print


class Console:
    # Callback - функция для вывода в консоль
    _callback = None
    _instance = None

    def __new__(cls):
        if cls._instance is None:
            cls._instance = super(Console, cls).__new__(cls)
        return cls._instance

    def configure(self, callback):
        self._callback = callback

    def write_console(self, text):
        if self._callback is None:
            original_print("Ошибка! Обратный вызов отсутствует", text)
            return
        self._callback(text)


cons = Console()


def print(*args):
    for arg in args:
        original_print(arg)
        cons.write_console(arg)
    cons.write_console("\n")
