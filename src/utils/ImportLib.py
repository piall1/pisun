import os
import platform
import time


class ImportLib:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(ImportLib, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def check_network_manager(self):
        if platform.system() == "Linux":
            result = os.system("systemctl is-active NetworkManager")
            if result == 0:
                print("NetworkManager уже запущен.")
            else:
                install_check = os.system("dpkg -l | grep network-manager")
                if install_check == 0:
                    print("NetworkManager установлен, но не запущен. Попробуем его запустить...")
                    start_result = os.system("sudo systemctl start NetworkManager")
                    if start_result == 0:
                        print("NetworkManager успешно запущен.")
                    else:
                        print("Не удалось запустить NetworkManager. Проверьте права доступа и наличие ошибок.")
                else:
                    print("NetworkManager не установлен. Установка...")
                    install_result = os.system("sudo apt update && sudo apt install -y network-manager")
                    if install_result == 0:
                        print("NetworkManager успешно установлен.")
                    else:
                        print("Ошибка установки NetworkManager. Проверьте права доступа и наличие ошибок.")
        else:
            raise SystemError("Подключение к WiFI сети недоступно на вашей операционной системе.")

    def check_qr_lib(self):
        try:
            from pyzbar.pyzbar import decode
            print("Библиотека pyzbar установлена")
        except Exception:
            print("Библиотека pyzbar не установлена")
            if platform.system() == "Linux":
                os.system("sudo apt-get update")
                time.sleep(5)
                os.system("sudo apt-get -qq -y install libzbar0")
                time.sleep(5)
                os.system("venv/bin/pip install pyzbar")
                os.system("systemctl restart pisun.service")
                exit(0)
            else:
                raise SystemError("Error")

    def check_image_lib(self):
        try:
            from PIL import Image
            print("Библиотека PIL установлена")
        except Exception:
            print("Библиотека PIL не установлена")
            if platform.system() == "Linux":
                os.system("sudo apt-get install libopenjp2-7")
