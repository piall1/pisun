import json
import os
from src.structures import ConvertDataClass


class WebJson:
   
    def create_status(self, status_params):
        status = {
            "firmware_version": status_params.get("firmware_version", 1.2),
            "work_status":  status_params.get("work_status", ""),
            "coords":       status_params.get("coords", [0.0, 0.0, 0.0]),
            "ntrip_status": status_params.get("ntrip_status", ""),
            "file_status":  status_params.get("file_status", ""),
            "ports":        status_params.get("ports", []),
            "serial_number": status_params.get("serial_number", ""),
            "sats_data":    status_params.get("sats_data", {}),
            "fix_status":   status_params.get("fix_status", False),
            "console":      status_params.get("console", ""),
            "status_authorize": status_params.get("status_authorize", {})
        }
        return json.dumps(status, ensure_ascii=False, indent=4)

    def create_points(self, points_input):
        points = {"points": points_input}
        return json.dumps(points, ensure_ascii=False, indent=4)

    def create_system_coord(self, coords):
        system_coord = {"system_coord": coords}
        return json.dumps(system_coord, ensure_ascii=False, indent=4)

    def create_sync_points(self, points_input, status, message):
        status_string = "ok" if status else "error"
        points = {"points": points_input,
                  "status": status_string,
                  "message": message
                  }
        return json.dumps(points, ensure_ascii=False, indent=4)

    def create_data_converting(self, data_list):
        data_convert = {"responce": [],
                        "status_response": "ok",
                        "message": "Конвертация прошла успешно"
                        }
        for item in data_list:
            data_convert["responce"].append({
                "file_name": item.file_path,
                "link": item.link,
                "status": ConvertDataClass.Status(item.status).name,
            })
        return json.dumps(data_convert, ensure_ascii=False, indent=4)

    def create_post_responce(self, status: bool, message: str):
        status_string = "ok" if status else "error"
        out_string = {
            "status": status_string,
            "message": message
        }
        return json.dumps(out_string, ensure_ascii=False, indent=4)

    def create_params(self, params_dict):
        return json.dumps(params_dict, ensure_ascii=False, indent=4)

    def create_filelist(self, path):
        file_list = []
        try:
            for filename in os.listdir(path):
                file_path = os.path.join(path, filename)
                if os.path.isfile(file_path):
                    size = os.path.getsize(file_path)
                    time_mark = int(os.path.getmtime(file_path))

                    file_info = {
                        "filename": filename,
                        "size": size,
                        "time_mark": time_mark
                    }
                    file_list.append(file_info)
        
        except Exception as e:
            print(f"Ошибка при создании списка файлов: {e}")
        
        sorted_file_list = sorted(file_list, key=lambda x: x["time_mark"], reverse=True)
        json_data = {"files": sorted_file_list}
        return json.dumps(json_data, indent=4)

    def parce_params(self, string) -> dict:
        decoded_data = json.loads(string)
        return decoded_data

    def create_files(self, path):
        decoded_data = json.loads(path)
        file = decoded_data["filename"]
        file_path = "files/"
        file_name = file_path + file
        return str(file_name)

    def parce_delete_file(self, string):
        decoded_data = json.loads(string)
        file = decoded_data["filename"]
        return str(file)

    def parce_delete_point(self, string):
        decoded_data = json.loads(string)
        uuid_point = decoded_data["uuid"]
        return str(uuid_point)

    def parce_get_uuid(self, string):
        decoded_data = json.loads(string)
        uuid = decoded_data["uuid"]
        if uuid is None:
            return None
        return str(uuid)

    def create_ssid(self, name, ip):
        string = {
            "network": name,
            "ip": ip
        }
        return json.dumps(string, ensure_ascii=False, indent=4)

    def load_params_from_file(self):
        with open("resources/settings.json", 'r', encoding='utf-8') as f:
            data = json.load(f)
        return data
