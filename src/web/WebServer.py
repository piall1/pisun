import cgi
import http.server
import json
import re
import socketserver
import os
from src.structures import ParamsCS
from src.web import WebJson, WifiManager
from src.services import Controller, CSController
from src.request import AuthorizationClass
from src.converters import ParceQRCode

base_path = "public/"
server_return_types = {
    '/': 'text/html',
    '.html': 'text/html',
    '.js': 'application/javascript',
    '.css': 'text/css',
    '.ico': 'image/ico',
    '.png': 'image/png',
    '.map': 'file/map',
    '.rtcm3': 'file/rtcm3',
    '.cnb': 'file/cnb'
}

console_data = []
console_data_max = 30
file_qr = ""


def console_handler(message):
    try:
        message = str(message)
    except Exception:
        return
    message = message.replace("\r", "")
    message = message.replace("\n", "")
    if len(message) <= 0:
        return
    while len(console_data) > console_data_max:
        console_data.pop(0)
    console_data.append(message)


class MyRequestHandler(http.server.BaseHTTPRequestHandler):

    def header_send(self):
        self.send_response(200)
        self.send_header('Content-type', 'application/json')
        self.send_header('Access-Control-Allow-Origin', '*')
        self.end_headers()

    def sendfile(self, file_path, content_type):
        print("file for responce: ", file_path)
        if os.path.exists(file_path):
            # Файл существует, читаем его содержимое и отправляем
            with open(file_path, 'rb') as file:
                self.send_response(200)
                self.send_header('Content-type', content_type)
                self.end_headers()
                self.wfile.write(file.read())
        else:
            # Файл не найден, отправляем ошибку 404
            self.send_error(404, 'File Not Found')

    def do_GET(self):
        print("Recive GET request", self.path)
        cont = Controller.Controller()
        auth = AuthorizationClass.AuthorizationClass()
        cont_cs = CSController.ControllerCS()
        parce_qr = ParceQRCode.ParceQRCode()
        wifi_cont = WifiManager.WifiManager()
        web_json = WebJson.WebJson()

        if '?' in self.path:
            self.path = self.path.split('?')[0]

        if self.path == '/api/status/get':
            self.header_send()
            status = {
                "firmware_version": cont.get_firmware_version(),
                "work_status": cont.params.get_param("work_mode"),
                "coords": cont.get_current_coords(),
                "ntrip_status": cont.get_ntrip_status().replace("\n", "<br>"),
                "file_status": cont.get_file_status(),
                "ports": cont.get_ports(),
                "serial_number": cont.get_serial(),
                "sats_data": cont.get_sats_num(),
                "fix_status": cont.get_fix_status(),
                "console": console_data,
                "status_authorize": auth.get_status_authorize()
            }
            string = web_json.create_status(status)
            self.wfile.write(string.encode('utf-8'))

        elif self.path == '/api/files/list':
            self.header_send()
            string = web_json.create_filelist(cont.file_recorder.file_path)
            self.wfile.write(string.encode('utf-8'))

        elif '/api/files/get/' in self.path:
            elements = self.path.split("/")
            file_name, file_extension = os.path.splitext(self.path)
            last_element = elements[-1]
            file_path = cont.file_recorder.file_path + last_element
            content_type = server_return_types[file_extension]
            self.sendfile(file_path, content_type)

        elif self.path == '/api/params/get':
            self.header_send()
            params = cont.load_params()
            if params["hardware_comport1"] == "3.0":
                params["hardware_comport1"] = "rtcm3.0"
            if params["hardware_comport1"] == "3.2":
                params["hardware_comport1"] = "rtcm3.2"

            if params["hardware_comport2"] == "3.0":
                params["hardware_comport2"] = "rtcm3.0"
            if params["hardware_comport2"] == "3.2":
                params["hardware_comport2"] = "rtcm3.2"

            if params["hardware_comport3"] == "3.0":
                params["hardware_comport3"] = "rtcm3.0"
            if params["hardware_comport3"] == "3.2":
                params["hardware_comport3"] = "rtcm3.2"

            if "ftp_password" in params:
                params["ftp_password"] = ""

            string = web_json.create_params(params)
            self.wfile.write(string.encode('utf-8'))

        elif self.path == '/api/point/list':
            try:
                self.header_send()
                points_list = cont.get_list_points()
                points = web_json.create_points(points_list)
                self.wfile.write(points.encode('utf-8'))
            except Exception as e:
                print(f"Error get points: {e}")

        elif self.path == '/api/point/sync':
            try:
                self.header_send()
                sync_points = cont.sync_points()
                points = web_json.create_sync_points(sync_points, True, "Точки синхронизированы")
                self.wfile.write(points.encode('utf-8'))
            except Exception as e:
                print("Ошибка при синхронизации точки!", e)
                var = web_json.create_post_responce(False, f"Ошибка при синхронизации точки! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/converter/result':
            try:
                self.header_send()
                data_list = cont.get_convert_result_link()
                data_converting = list(reversed(data_list))
                convert_file = web_json.create_data_converting(data_converting)
                self.wfile.write(convert_file.encode('utf-8'))
            except Exception as e:
                print("Ошибка при получении ссылки!", e)

        elif self.path == '/api/authorization/log_out':
            try:
                self.header_send()
                auth.log_out_of_account()
                var = web_json.create_post_responce(True, "Вы вышли из аккаунта")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при выходе из аккаунта!", e)
                var = web_json.create_post_responce(False, f"Ошибка при выходе из аккаунта! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/authorization/get_profile':
            try:
                self.header_send()
                profile = auth.get_name_profile()
                profile_name = {"profile": profile}
                profile_js = json.dumps(profile_name, ensure_ascii=False, indent=4)
                self.wfile.write(profile_js.encode('utf-8'))
            except Exception as e:
                print("Имя профиля не найдено!", e)

        elif self.path == '/api/plotdata/get':
            plotdata = cont.get_plot_data_dict()
            self.header_send()
            self.wfile.write(json.dumps(plotdata, ensure_ascii=False, indent=4).encode())

        elif self.path == '/api/coords_system/list':
            try:
                self.header_send()
                cs_list = cont_cs.get_list_coords_system()
                system_coord = web_json.create_system_coord(cs_list)
                self.wfile.write(system_coord.encode('utf-8'))
            except Exception as e:
                print(f"Ошибка при получении списка СК: {e}")

        elif self.path == '/api/coords_system/params_qr':
            try:
                self.header_send()
                params_cs = parce_qr.get_params_from_qr()
                system_coord = web_json.create_params(params_cs)
                self.wfile.write(system_coord.encode('utf-8'))
            except Exception as e:
                print(f"Ошибка при получении списка СК: {e}")

        elif self.path == '/api/wifi/get_available_net':
            try:
                self.header_send()
                networks = wifi_cont.get_available_networks()
                networks_js = web_json.create_params(networks)
                self.wfile.write(networks_js.encode('utf-8'))
            except Exception as e:
                print(f"Ошибка при получении списка доступных сетей: {e}")
                net_js = web_json.create_post_responce(False, f"Ошибка при получении списка доступных сетей: {e}")
                self.wfile.write(net_js.encode('utf-8'))

        elif self.path == '/api/wifi/get_saved_net':
            try:
                self.header_send()
                saved_net = wifi_cont.get_saved_networks()
                net_js = web_json.create_params(saved_net)
                self.wfile.write(net_js.encode('utf-8'))
            except Exception as e:
                print(f"Ошибка при получении списка сохранённых сетей: {e}")
                net_js = web_json.create_post_responce(False, f"Ошибка при получении списка сохранённых сетей: {e}")
                self.wfile.write(net_js.encode('utf-8'))

        elif self.path == '/api/wifi/get_previous_network':
            try:
                self.header_send()
                network, ip = wifi_cont.get_previous_network()
                network_js = web_json.create_ssid(network, ip)
                self.wfile.write(network_js.encode('utf-8'))
            except Exception as e:
                print(f"Ошибка при получении SSID: {e}")
                network_name_js = web_json.create_ssid("", "")
                self.wfile.write(network_name_js.encode('utf-8'))

        else:  # files
            try:
                file_name, file_extension = os.path.splitext(self.path)
                if file_name == '/':
                    file_path = os.path.join(base_path, 'index.html')
                    file_extension = file_name
                else:
                    file_path = os.path.join(base_path, self.path.lstrip('/'))
                content_type = server_return_types[file_extension]
                self.sendfile(file_path, content_type)
            except KeyError as e:
                print(f"Unknow type {e}")
                self.send_error(404, 'File Not Found')

    def do_POST(self):
        global post_data
        cont = Controller.Controller()
        auth = AuthorizationClass.AuthorizationClass()
        type = ParamsCS.TypeMapping()
        cont_cs = CSController.ControllerCS()
        web_json = WebJson.WebJson()
        parce_qr = ParceQRCode.ParceQRCode()
        wifi_cont = WifiManager.WifiManager()

        content_type = self.headers.get('Content-Type')
        content_length = self.headers.get('Content-Length')
        environ = {
            'REQUEST_METHOD': 'POST',
            'CONTENT_TYPE': content_type,
            'CONTENT_LENGTH': content_length,
        }

        if content_type and content_type.startswith('multipart/form-data'):
            form = cgi.FieldStorage(fp=self.rfile, headers=self.headers, environ=environ)
            file_item = form['file']

            if file_item.filename:
                file_path = os.path.join('resources', file_item.filename)
                with open(file_path, 'wb') as f:
                    f.write(file_item.file.read())
                self.file_qr = file_item.filename
                print(f"File {file_item.filename} has been saved.")
            else:
                print("No file was uploaded.")
            self.header_send()
        else:
            post_data = self.rfile.read(int(content_length)).decode('utf-8')

        # print(f"Path: {self.path} Recive POST request: {post_data}")

        if self.path == '/api/params/set':
            try:
                self.header_send()
                params_dict = web_json.parce_params(post_data)

                if params_dict["hardware_comport1"] == "rtcm3.0":
                    params_dict["hardware_comport1"] = "3.0"
                if params_dict["hardware_comport1"] == "rtcm3.2":
                    params_dict["hardware_comport1"] = "3.2"

                if params_dict["hardware_comport2"] == "rtcm3.0":
                    params_dict["hardware_comport2"] = "3.0"
                if params_dict["hardware_comport2"] == "rtcm3.2":
                    params_dict["hardware_comport2"] = "3.2"

                if params_dict["hardware_comport3"] == "rtcm3.0":
                    params_dict["hardware_comport3"] = "3.0"
                if params_dict["hardware_comport3"] == "rtcm3.2":
                    params_dict["hardware_comport3"] = "3.2"

                if params_dict["tcp_remote_port"] == "":
                    del params_dict["tcp_remote_port"]
                else:
                    params_dict["tcp_remote_port"] = int(params_dict["tcp_remote_port"])

                if params_dict["periodCS_sending"] == "":
                    del params_dict["periodCS_sending"]
                else:
                    params_dict["periodCS_sending"] = int(params_dict["periodCS_sending"])

                if params_dict["tcp_local_port"] == "":
                    del params_dict["tcp_local_port"]
                else:
                    params_dict["tcp_local_port"] = int(params_dict["tcp_local_port"])

                if params_dict["coords_input"][0] is None:
                    params_dict["coords_input"][0] = 0
                if params_dict["coords_input"][1] is None:
                    params_dict["coords_input"][1] = 0
                if params_dict["coords_input"][2] is None:
                    params_dict["coords_input"][2] = 0

                params_dict["coords_input"] = [float(params_dict["coords_input"][0]),
                                               float(params_dict["coords_input"][1]),
                                               float(params_dict["coords_input"][2])]
                cont.save_params(params_dict)
                var = web_json.create_post_responce(True, "Параметры сохранены")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при сохранении параметров", e)
                var = web_json.create_post_responce(False, f"Ошибка при сохранении параметров! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/authorization/authorize':
            try:
                self.header_send()
                params_dict = web_json.parce_params(post_data)
                auth.get_authorized(params_dict["telegram_id"], params_dict["pwd"])
                var = web_json.create_post_responce(True, "Введите пин код")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при получении пин кода!", e)
                var = web_json.create_post_responce(False, f"Ошибка: проверьте логин или пароль! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/authorization/pin':
            try:
                self.header_send()
                params_dict = web_json.parce_params(post_data)
                auth.entry_user_pin(params_dict["pin"])
                var = web_json.create_post_responce(True, "Авторизация прошла успешно")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при авторизации!!!", e)
                var = web_json.create_post_responce(False, f"Ошибка: проверьте код подтверждения! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/converter/convert':
            try:
                self.header_send()
                profile = auth.get_name_profile()
                if profile == None:
                    raise Exception("Авторизируйтесь!")
                file = web_json.create_files(post_data)
                cont.start_convert_cnb(file)
                var = web_json.create_post_responce(True, "Файл конвертируется")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при конвертации!", e)
                var = web_json.create_post_responce(False, f"Ошибка при конвертации! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/point/save':
            try:
                self.header_send()
                params_dict = web_json.parce_params(post_data)

                def is_number(s):
                    # Регулярное выражение для проверки является ли строка числом с плавающей запятой или целым числом
                    pattern = re.compile(r'^-?\d+(\.\d+)?$')
                    return bool(pattern.match(s))

                params = params_dict["data"]

                if not is_number(params["lat"]):
                    raise TypeError("Значение 'Lat' должно быть числом.")
                if not is_number(params["lon"]):
                    raise TypeError("Значение 'Lon' должно быть числом.")
                if not is_number(params["height"]):
                    raise TypeError("Значение 'Height' должно быть числом.")

                cont.save_input_points(params_dict["name"], params_dict["data"])
                var = web_json.create_post_responce(True, "Точка сохранена")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка сохранения точек!", e)
                var = web_json.create_post_responce(False, f"Ошибка при сохранении точки! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/point/edit':
            try:
                self.header_send()
                params_dict = web_json.parce_params(post_data)

                def is_number(s):
                    # Регулярное выражение для проверки является ли строка числом с плавающей запятой или целым числом
                    pattern = re.compile(r'^-?\d+(\.\d+)?$')
                    return bool(pattern.match(s))

                params = params_dict["data"]

                if not is_number(params["lat"]):
                    raise TypeError("Значение 'Lat' должно быть числом.")
                if not is_number(params["lon"]):
                    raise TypeError("Значение 'Lon' должно быть числом.")
                if not is_number(params["height"]):
                    raise TypeError("Значение 'Height' должно быть числом.")

                cont.update_point(params_dict["uuid"], params_dict["name"], params_dict["data"])
                var = web_json.create_post_responce(True, "Точка изменена")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при изменении точки!", e)
                var = web_json.create_post_responce(False, f"Ошибка при изменении точки! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/point/delete':
            try:
                self.header_send()
                uuid = web_json.parce_delete_point(post_data)
                cont.delete_point(uuid)
                var = web_json.create_post_responce(True, "Точка удалена")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при удалении точки!", e)
                var = web_json.create_post_responce(False, f"Ошибка при удалении точки! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/coords_system/save':
            try:
                self.header_send()
                params_dict = web_json.parce_params(post_data)
                type.check_types(params_dict)
                cont_cs.add_coords_system(params_dict)
                var = web_json.create_post_responce(True, "Система координат сохранена")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при сохраненим системы координат!", e)
                var = web_json.create_post_responce(False, f"Ошибка при сохранении системы координат! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/coords_system/edit':
            try:
                self.header_send()
                params_dict = web_json.parce_params(post_data)
                uuid = web_json.parce_get_uuid(post_data)
                cont_cs.update_coords_system(uuid, params_dict)
                var = web_json.create_post_responce(True, "Система координат изменена")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при изменении системы координат!", e)
                var = web_json.create_post_responce(False, f"Ошибка при изменении системы координат! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/coords_system/delete':
            try:
                self.header_send()
                uuid = web_json.parce_get_uuid(post_data)
                cont_cs.delete_coords_system(uuid)
                var = web_json.create_post_responce(True, "Система координат удалена")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при удалении точки!", e)
                var = web_json.create_post_responce(False, f"Ошибка при удалении системы координат! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/coords_system/select_cs':
            try:
                self.header_send()
                params = web_json.parce_params(post_data)
                cont_cs.get_cs_from_view(params)
                var = web_json.create_post_responce(True, "Система координат установлена")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при удалении точки!", e)
                var = web_json.create_post_responce(False, f"Ошибка при установки системы координат! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/coords_system/upload_gr_code':
            try:
                relative_path = f"resources/{self.file_qr}"
                absolute_path = os.path.abspath(relative_path)
                parce_qr.read_qr_code(absolute_path)
                os.remove(absolute_path)
                var = web_json.create_post_responce(True, "QR код получен")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка при получении QR кода!", e)
                var = web_json.create_post_responce(False, f"Ошибка при получении QR кода! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/wifi/new_network':
            try:
                self.header_send()
                params = web_json.parce_params(post_data)
                wifi_cont.add_new_network(params["ssid"], params["password"])
                var = web_json.create_post_responce(True, f"Успешное подключение к сети '{params['ssid']}'.")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                var = web_json.create_post_responce(False, f"Ошибка при подключении к сети '{params['ssid']}': {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/wifi/connect_saved_network':
            try:
                self.header_send()
                params = web_json.parce_params(post_data)
                wifi_cont.connect_to_saved_net(params["ssid"])
                var = web_json.create_post_responce(True, f"Успешное подключение к сети '{params['ssid']}'.")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                var = web_json.create_post_responce(False, f"Ошибка при подключении к сети '{params['ssid']}': {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/wifi/check_saved_network':
            try:
                self.header_send()
                params = web_json.parce_params(post_data)
                response = wifi_cont.check_saved_network(params["ssid"])
                var = web_json.create_post_responce(True, response)
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                var = web_json.create_post_responce(False, f"Ошибка команды: {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/wifi/delete_network':
            try:
                self.header_send()
                params = web_json.parce_params(post_data)
                wifi_cont.delete_network(params["uuid"])
                var = web_json.create_post_responce(True, f"Сеть '{params['uuid']}' удалена.")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                var = web_json.create_post_responce(False, f"Ошибка при удалении сети '{params['uuid']}': {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/pihatel/start':
            try:
                self.header_send()
                params_dict = web_json.parce_params(post_data)

                if params_dict["hardware_comport1"] == "rtcm3.0":
                    params_dict["hardware_comport1"] = "3.0"
                if params_dict["hardware_comport1"] == "rtcm3.2":
                    params_dict["hardware_comport1"] = "3.2"

                if params_dict["hardware_comport2"] == "rtcm3.0":
                    params_dict["hardware_comport2"] = "3.0"
                if params_dict["hardware_comport2"] == "rtcm3.2":
                    params_dict["hardware_comport2"] = "3.2"

                if params_dict["hardware_comport3"] == "rtcm3.0":
                    params_dict["hardware_comport3"] = "3.0"
                if params_dict["hardware_comport3"] == "rtcm3.2":
                    params_dict["hardware_comport3"] = "3.2"

                if params_dict["tcp_local_port"] == "":
                    params_dict["tcp_local_port"] = 5200

                if not "rtcm_type" in params_dict:
                    params_dict["rtcm_type"] = "3.2"
                else:
                    params_dict["tcp_local_port"] = int(params_dict["tcp_local_port"])
                params_dict["coords_input"] = [float(params_dict["coords_input"][0]),
                                               float(params_dict["coords_input"][1]),
                                               float(params_dict["coords_input"][2])]

                if params_dict["ftp_password"] == "":
                    params_pwd = web_json.load_params_from_file()
                    params_dict["ftp_password"] = params_pwd["ftp_password"]

                cont.start_controller(params_dict)
                var = web_json.create_post_responce(True, "Начало работы.")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print("Ошибка начала работы", e)
                var = web_json.create_post_responce(False, f"Ошибка начала работы! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/pihatel/stop':
            try:
                self.header_send()
                cont.stop_controller()
                var = web_json.create_post_responce(True, "Работа остановлена")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                var = web_json.create_post_responce(False, f"Ошибка остановки работы! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/files/delete':
            try:
                self.header_send()
                filename = web_json.parce_delete_file(post_data)
                cont.device_delete_file(filename)
                var = web_json.create_post_responce(True, f" Файл {filename} удален")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                var = web_json.create_post_responce(False, f"Ошибка удаления файла! {e}")
                self.wfile.write(var.encode('utf-8'))

        elif self.path == '/api/getupdate':
            try:
                self.header_send()
                cont.device_update()
                var = web_json.create_post_responce(True, f"Обновление завершено")
                self.wfile.write(var.encode('utf-8'))
            except Exception as e:
                print(f"Ошибка! {e} Скачайте последнюю версию с нашего репозиория.")
                var = web_json.create_post_responce(False, f"{e} Скачайте последнюю версию с нашего репозиория.")
                self.wfile.write(var.encode('utf-8'))


class WebServer:
    def __init__(self, port):
        self.port = port
        self.handler = None
        self.server = None

    def __del__(self):
        self.stop()

    def start(self):
        self.handler = MyRequestHandler
        self.server = socketserver.TCPServer(('', self.port), self.handler)
        print(f"Server started on port {self.port}")
        self.server.serve_forever()

    def stop(self):
        if self.server:
            print("Server stopping...")
            self.server.shutdown()
            self.server.server_close()
            print("Server stopped.")
