from src.structures import SettingsWifi


class WifiManager:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(WifiManager, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    def __init__(self):
        self.settings = SettingsWifi.SettingsWifi()

    def get_available_networks(self):
        return self.settings.get_available_networks()

    def get_previous_network(self):
        return self.settings.get_previous_network()

    def get_saved_networks(self):
        return self.settings.get_save_networks()

    def connect_to_saved_net(self, ssid):
        return self.settings.connect_to_saved_net(ssid)

    def check_saved_network(self, ssid):
        return self.settings.check_saved_network(ssid)

    def add_new_network(self, ssid, password):
        self.settings.add_new_network(ssid, password)

    def delete_network(self, ssid):
        self.settings.delete_network(ssid)